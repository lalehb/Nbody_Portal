
#ifndef _LL_RULES_H_
#define _LL_RULES_H_

#define DIM 3
#define GUASSIAN 2

#include <cfloat>
#include <Eigen/Dense>       // for using the determinant and inverse of matrix in Gaussian
#include <Eigen/Eigenvalues> // for eigen values and eigen vectors
#include <math.h>
#include <sys/time.h>
#include <ctime>
#include <vector>

// #include "Binary_tree.h"
// #include "Single_tree_traversal.h"
#include "Metric.h"
#include "Clock.hpp"


template <typename Tree>
class LL_rules {
  private:
    /* dimension of  input data */
    int Dim; 
    /* Number of clusers for GMM*/
    unsigned int NumGuassian;
    int data_size;
    /* Pointer to the source tree */
    Tree& src_tree;
    /* Points to permutated source array */
    Points_t& src_data;
    /* For  measuring the guassian result */
    real_t Gaussian(real_t* datai, real_t* mui, int i);
    real_t Gaussian(Point datai, real_t* mui, int i);
   // real_t Gaussian(real_t* datai, real_t* mui, real_t** cholesky, real_t determiant);
    /*For measuring  the weight in Moore's paper (or responsibilities in wiki)*/
    real_t Weight(real_t* datai, int t);
    /* EM Information measured during tree traversal */
    real_t** mean;    // NumGuassian x Dim -> matrix of guassian means
    real_t*** sigma;  // NumGussian x Dim x Dim -> matrix of gussian covariance (dxd)
    real_t* pi;       // NumGuassian -> vector of probabilities for each guassian class
    real_t** resp;    // NumData x NumGaussian -> matrix of responsibilities
  
  public:
    /* Varibale for debuging purpose */
    real_t tolerance;
    real_t* weight_sofar;
    real_t num_of_prune = 0;
    real_t size_of_prune = 0;
    real_t power = 1; // measuringt this power once and use it for all the computation of Guassian ** Optimization **
    real_t*** Cholesky_all;
    real_t* determinant_all;

    real_t LL;
    stats statistics;
    typedef typename Tree::NodeTree Box;
    /* Initilization */
    LL_rules(int size, int d, unsigned int k, real_t tr, Tree& st, Points_t& s, real_t** mean1, 
       real_t*** sigma1, real_t* pi1, real_t** resp1, real_t*** Cholesky, real_t* determinant)
        :  Dim(d), NumGuassian(k), data_size(size), src_tree(st), src_data(s), mean(mean1), sigma(sigma1), pi(pi1), resp(resp1), tolerance(tr), Cholesky_all(Cholesky), determinant_all(determinant) {
         power = pow((2* M_PI),Dim);
          weight_sofar = new real_t[NumGuassian];
          for (unsigned int i = 0; i < NumGuassian; ++i) {
            weight_sofar[i] = 0;
          }
         LL = 0;
        }



    /* Calculate which branch to visit first */
    vector<int> visit (Box& s, int t);
    
    void ForwardSub(real_t** L, real_t* Y, real_t* result);

    /* computatiuon for ab approximated tree*/
    void centroid_case (Box& s, int t);
    /* Base case calculation */
    void base_case (Box& s, int t);
    /* Check if subtree should be pruned */
    bool prune_subtree (Box& s, int t);


};

/* Template implementation */

/**
 * This function will measure the (L^-1)Y and return the result
*/

template <typename Tree>
void  LL_rules<Tree>::ForwardSub(real_t** L, real_t* Y, real_t* result) {
 result[0] = Y[0]/L[0][0];
  real_t temp;
  for (size_t i = 1; i < Dim; ++i) {
    temp = Y[i];
    for (size_t j = 0; j < i; ++j)
      temp -= L[i][j] * result[j];
    result[i] = temp / L[i][i];
  }
}

template <typename Tree>
real_t 
//LL_rules<Tree>::Gaussian(real_t*  datai, real_t* mui, int f) {
LL_rules<Tree>::Gaussian(Point  datai, real_t* mui, int f) {

  real_t res[Dim];
  double inner_product = 0;
  res[0] = (datai[0]-mui[0])/Cholesky_all[f][0][0];
  real_t temp;
  for (size_t i = 1; i < Dim; ++i) {
    temp = datai[i]-mui[0];
    for (size_t j = 0; j < i; ++j)
      temp -= Cholesky_all[f][i][j] * res[j];
    res[i] = temp / Cholesky_all[f][i][i];
    inner_product += res[i]*res[i];
  }

 real_t result = determinant_all[f] * exp(-0.5 * inner_product );

 return result;
}

template <typename Tree>
real_t 
LL_rules<Tree>::Gaussian(real_t*  datai, real_t* mui, int f) {

  real_t res[Dim];
  double inner_product = 0;
  res[0] = (datai[0]-mui[0])/Cholesky_all[f][0][0];
  real_t temp;
  for (size_t i = 1; i < Dim; ++i) {
    temp = datai[i]-mui[0];
    for (size_t j = 0; j < i; ++j)
      temp -= Cholesky_all[f][i][j] * res[j];
    res[i] = temp / Cholesky_all[f][i][i];
    inner_product += res[i]*res[i];
  }

 real_t result = determinant_all[f] * exp(-0.5 * inner_product );

 return result;
}



/*
* Base case calculation
*/

template<typename Tree>
void
LL_rules<Tree>::base_case (Box& s, int t) {

  real_t liklihood = 0;
  real_t datai[Dim]; 
  for (size_t i = s.begin(); i < s.end(); ++i) {
    real_t temp = 0;

    for (size_t l = 0 ; l < Dim; ++l) 
      datai[l] = src_data[i][l];
    
    for (int j = 0; j < NumGuassian; ++j)
      temp += pi[j] * Gaussian(datai, mean[j], j);

    liklihood += log(temp);
  }
  LL += liklihood;
 
}


/*
* Centroid case calculation
*/
template<typename Tree>
void
LL_rules<Tree>::centroid_case (Box& s, int t) {

    real_t sum_lo = 0;
    real_t sum_hi = 0;
    real_t sum_center = 0;
    real_t lo_weights[NumGuassian];
    real_t hi_weights[NumGuassian];

    for (size_t i = 0; i < NumGuassian; ++i) {

      for (size_t j = 0 ; j < pow(2, Dim) ; ++j) {
        real_t* tem_point = new real_t[Dim];
        int decimal_number = j ;
        for (size_t k = 0; k < Dim; ++k) {
          tem_point[k] = (decimal_number %2 ==0) ? s.bounds.lo[k]: s.bounds.hi[k];
          decimal_number /= 2;
        }
        real_t Guassian_temp = pi[i] * Gaussian(tem_point, mean[i], i);
        if (j == 0) {
          lo_weights[i] = Guassian_temp;
          hi_weights[i] = Guassian_temp;
        }else {
          lo_weights[i] = (Guassian_temp < lo_weights[i]) ? Guassian_temp : lo_weights[i] ;
          hi_weights[i] = (Guassian_temp > hi_weights[i]) ? Guassian_temp : hi_weights[i] ;
        }
        delete[] tem_point;
      }

      sum_center += pi[i] * Gaussian(src_tree.centers[s.index()], mean[i], i);
      sum_lo += lo_weights[i];
      sum_hi += hi_weights[i];
    }

  LL += (s.size() * log(sum_center));
}


/**
 * Check if the hypersphere from the current farthest neighbor intersects the hyperrectange.
 */
template<typename Tree>
inline
bool
//LL_rules<Tree>::prune_subtree (Box& s, real_t* center_box) {
LL_rules<Tree>::prune_subtree (Box& s, int t) {

    real_t sum_lo = 0;
    real_t sum_hi = 0;
    real_t sum_center = 0;
    real_t lo_weights[NumGuassian];
    real_t hi_weights[NumGuassian];

    for (size_t i = 0; i < NumGuassian; ++i) {
     for (size_t j = 0 ; j < pow(2, Dim) ; ++j) {
        real_t tem_point[Dim];
        int decimal_number = j ;
        for (size_t k = 0; k < Dim; ++k) {
          tem_point[k] = (decimal_number %2 ==0) ? s.bounds.lo[k]: s.bounds.hi[k];
          decimal_number /= 2;
        }
        real_t Guassian_temp = pi[i] * Gaussian(tem_point, mean[i], i);
        if (j == 0) {
          lo_weights[i] = Guassian_temp;
          hi_weights[i] = Guassian_temp;
        }else {
          lo_weights[i] = (Guassian_temp < lo_weights[i]) ? Guassian_temp : lo_weights[i] ;
          hi_weights[i] = (Guassian_temp > hi_weights[i]) ? Guassian_temp : hi_weights[i] ;
        }
      }

      sum_center += pi[i] * Gaussian(src_tree.centers[s.index()], mean[i], i);
      sum_lo += lo_weights[i];
      sum_hi += hi_weights[i];
    }
   

    if ((log(sum_hi/sum_lo) < 0.1 * abs(log(sum_center)))) {
    // if ((log(sum_hi/sum_lo) < 0.05 * abs(log(sum_center)))) {
    // if ((log(sum_hi/sum_lo) < 0.5 * abs(log(sum_center)))) { --> almost 2X faster with same convergence
      return true; 
    }

  return false;
}

/**
 * Check which brach should be explored first.
 */
template<typename Tree>
inline
vector<int>
LL_rules<Tree>::visit (Box& s, int t) {
  vector<int> visit_order;
  for (int i = 0; i < s.num_child(); i++)
    visit_order.push_back(i);
  return visit_order;
}

#endif
