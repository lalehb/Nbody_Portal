#ifndef __EM_GMM__
#define __EM_GMM__

struct MeanSigma{
  real_t** mean;
  real_t*** sigma;
};

class EMBase
{
  private:

    Points_t& data;    // NumData x Dim -> data matrix with rows as elements of data
    real_t** mean;    // NumGuassian x Dim -> matrix of guassian means
    real_t*** sigma;  // NumGussian x Dim x Dim -> matrix of gussian covariance (dxd)
    real_t* pi;       // NumGuassian -> vector of probabilities for each guassian class
    real_t** resp;    // NumData x NumGaussian -> matrix of responsibilities
    int NumData;      // Number of data points [n]
    int NumGaussian;  // Number of Guassian Mixtures [k]
    int Dim;          // dimention of data [d]
    real_t Threshold; // Convergence threshold
    real_t* nk;
    int Iter;

    real_t LogLiklihood();
    real_t Gaussian(Point datai , real_t*  mui, real_t** sigmai); // result of multivariate gaussian
    void Estep();      // running the E step of algorithm
    void Mstep();      // running the M step by calling SetPiMean and SetSigma
    void SetPiMean();  // updating pi and Mean
    void SetSigma();   // updating sigma

  public:

    EMBase(int n, int k , int d, int Iter, real_t t, Points_t& dataInput);

    void IterationEM();  // The main EM iteration

    void ResultPrinter(); // Printing the result of EM iteration
    void Result1(real_t** meani, real_t*** sigmai);
    MeanSigma Result();
};

/* Template instantiation */
#include "EM.cpp"

#endif /* defined(__EM_GMM__EM__) */
