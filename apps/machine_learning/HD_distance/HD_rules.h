#ifndef _HD_RULES_H_
#define _HD_RULES_H_
#define counter_type unsigned long long

#include <cfloat>

#include "kNN_vector.h"
#include "Metric.h"


#include "range.h"

typedef Metric<Point, Point> MetricType;
typedef SquaredEuclideanMetric<Point, Point> SquaredEuclideanMetricType;


class HD_distance {
  public:
    real_t squared_distance;    /* Squared distance of the referenced element to an implicit point */
    int index;  /* Index of the feature vector in the data set */


    /* Default and convenience constructors */
    HD_distance() : squared_distance(DBL_MAX), index(-1) {}
    HD_distance(int i, real_t sq_dist) : squared_distance(sq_dist), index(i) {}

    bool operator< (const HD_distance &v) const {
      return squared_distance < v.squared_distance;
    }
};

typedef vector<HD_distance> Neighbors;
typedef vector<Neighbors> resultMaxMin;
typedef kNN_vector<HD_distance> kvector;
EuclideanMetric<Point, Point> metric2;


/* kept the structure of kNN_vector here for the case if we want to expand the problem of HD_distance */

template <typename Tree, typename S, typename C = kNN_vector<HD_distance> >
class HD_rules {
  public:
    typedef typename Tree::NodeTree Box;
    // EuclideanMetric<Point, Point> metric;
    int dim;
    /* the final value of Hausdarff distance */
    real_t MaxMin = -1;
    /* Number of maxmins, Note that for now it is one */
    unsigned int k;

    Tree& src_tree;
    Tree& trg_tree;

    /* Points to permutated source array */
    Points_t& src_data;
    /* Reference input target array */
    Points_t& trg_data;

    MetricType* metric;

    counter_type& num_prunes;


    /* Valid distance computed so far and its counter for updating the MaxMin */
    std::vector<real_t> valid_distance;
    std::vector<real_t> distance_counter;

    /* prune generation would be used */
    int pg_rule = 0;


    HD_rules(int d, unsigned int k, Tree& st, Tree& tt, Points_t& s, Points_t& t, MetricType* m, vector<C>& n, counter_type& num_p)
        : dim(d), k(k), src_tree(st), trg_tree(tt), src_data(s), trg_data(t), metric(m),
          valid_distance(trg_tree.points(), DBL_MAX), distance_counter(trg_tree.points(), 0), num_prunes(num_p) {}


    /* Base case calculation */
    void base_case (Box& s, int t);
    void base_case (Box& s, Box& t);
    void base_case (vector<Box*> t);


    /* Check if subtree should be pruned */
    bool prune_subtree (Box& s, int t);
    bool prune_subtree (Box& s, Box& t);
    bool prune_subtree (vector<Box*> t);

    /* Calculate which branch to visit first */
    vector<int> visit (Box& s, int t);
    vector<int> visit (Box& s, Box& t);
    vector<int> visit (vector<Box*> t, int div);

    /* Update distance */
    void update_state (Box& s, Box& t);
    void update_state (vector<Box*> t);

    /* Update the current value of the distance to the nearest point in the hyperrectangle */
    real_t distance_update (Box& s, int t);
    real_t distance_update (Box& s, Box& t);
    real_t distance_update (vector<Box*> t);


    /* centorid case computation for the subtree is will be prunes, in this case since it's prune it does nothing*/
    void centroid_case (Box& s, int t) {};
    void centroid_case (Box& s, Box& t) {};
    void centroid_case (vector<Box*> t) {};

    void set_temp(int temp) {};
    void adjustPartitionCounter(int , int , int , int){};
    double get_temp(){};
};

// Template implementation

template<typename Tree, typename S, typename C>
void
HD_rules<Tree,S,C>::base_case (Box& s, int t) {
  // SquaredEuclideanMetric<Point, Point> metric;
  EuclideanMetric<Point, Point> metric;
  real_t dist;
  real_t minDist = DBL_MAX;
  for (size_t j = s.begin(); j < s.end(); ++j) {
    dist = metric.compute_distance (trg_data[t], src_data[j]);
    if (dist > 0 && dist < minDist)
            minDist = dist;
  }
   /* Update the values for each distance */
   distance_counter[t] += s.size();
   if (minDist < valid_distance[t])
        valid_distance[t] = minDist;
   if ((distance_counter[t]  == src_data.size()) && ( valid_distance[t] > MaxMin) ){
      #pragma omp critical
        MaxMin = valid_distance[t];
   }

  /* Update current farthest nearest neighbor distance */
}

template<typename Tree, typename S, typename C>
void
HD_rules<Tree,S,C>::base_case (Box& s, Box& t) {
  // SquaredEuclideanMetric<Point, Point> metric;
  EuclideanMetric<Point, Point> metric;
  real_t dist, minDist;

  for (size_t i = t.begin(); i < t.end(); ++i) {
    minDist = DBL_MAX;
    for (size_t j = s.begin(); j < s.end(); ++j) {
      dist = metric.compute_distance (trg_data[i], src_data[j]);
      if ( dist < minDist)
            minDist = dist;
    }
    /* Update the values for each distance */
   distance_counter[i] += s.size();
   if (minDist < valid_distance[i])
        valid_distance[i] = minDist;
   if ((distance_counter[i]  == src_data.size()) && ( valid_distance[i] > MaxMin) ) {
      // #pragma omp critical
      MaxMin = valid_distance[i];
   }

  }
}

 /* k or _k is set to 1 for KNN with two dataset and has been used
  * for the multi-tree, in the case of an algorithm with more
  * than two dataset the kernel function and k should have
  * change accordingly
  */


template<typename Tree, typename S, typename C>
void
HD_rules<Tree,S,C>::base_case (vector<Box*> s) {
  int k = 1;

  EuclideanMetric<Point, Point> metric;
  real_t dist;
  for (size_t i = s[k]->begin(); i < s[k]->end(); ++i) {
    real_t minDist = DBL_MAX;
    for (size_t j = s[k-1]->begin(); j < s[k-1]->end(); ++j) {
      dist = metric.compute_distance (trg_data[i], src_data[j]);
      if (dist > 0 && dist < minDist)
            minDist = dist;
    }
    /* Update the values for each distance */
   distance_counter[i] += s.size();
   if (minDist < valid_distance[i])
        valid_distance[i] = minDist;
   if ((distance_counter[i]  == src_data.size()) && ( valid_distance[i] > MaxMin) ) {
      #pragma omp critical
      MaxMin = valid_distance[i];
   }

  }
}

template<typename Tree, typename S, typename C>
inline
bool
HD_rules<Tree,S,C>::prune_subtree (Box& s, int t) {
  int i = s.index();
  return s.min_distance(trg_data[t]) >= valid_distance[t];
}

/**
 * Checks for intersection of two boxes for the prune opportunities
 */
template<typename Tree, typename S, typename C>
inline
bool
HD_rules<Tree,S,C>::prune_subtree (Box& s, Box& t) {
  int i = s.index();
  int j = t.index();
  int check = 0;
  check = 1;

  /* cheeper prune check but will prune less of data */
  if ((s.max_distance(t) < MaxMin)){
    check = 0;
  }
  return (check == 0);
 /* expensive prune check because it checks all the borders */
  for ( int w = 0; w < pow(2,dim) ; w++ ) {
    Vec temp_point(dim);
    int decimal_number = w ;
    for (size_t k = 0; k < dim; ++k) {
      temp_point[k] = (decimal_number %2 ==0) ? t.bounds.lo[k]: t.bounds.hi[k];
      decimal_number /= 2;
    }

    for ( int j = 0; j < pow(2,dim) ; j++ ) {
      Vec temp_point_inner(dim);
      int decimal_number_inner = j ;
      double distance = 0;
      for (size_t kk = 0; kk < dim; ++kk) {
         temp_point_inner[kk] = (decimal_number_inner %2 ==0) ? s.bounds.lo[kk]: s.bounds.hi[kk];
         decimal_number_inner /= 2;
         distance +=  (temp_point[kk]- temp_point_inner[kk]) * (temp_point[kk]- temp_point_inner[kk]);
      }
      if (distance > MaxMin)
        check =1;
    }

  }


  if(check == 0) {
    num_prunes += s.size() *  t.size();
    for(size_t i = t.begin(); i < t.end(); ++i) {
      distance_counter[i] += s.size();
      if ((distance_counter[i]  == src_data.size()) && ( valid_distance[i] > MaxMin) && (valid_distance[i] < DBL_MAX) )
          MaxMin = valid_distance[i];
    }
  }
  return (check == 0);
}

template<typename Tree, typename S, typename C>
inline
bool
HD_rules<Tree,S,C>::prune_subtree (vector<Box*> s) {
  int _k = 1;
  int i = s[_k-1]->index();
  int j = s[_k]->index();
  int check = 0;
  for ( int w = 0; w < dim ; w++ ) {
    Vec temp_point(dim);
    int decimal_number = w ;
    for (size_t k = 0; k < dim; ++k) {
      temp_point[k] = (decimal_number %2 ==0) ? s[_k]->bounds.lo[k]: s[_k]->bounds.hi[k];
      decimal_number /= 2;
    }
    for ( int j = 0; j < pow(2,dim) ; j++ ) {
      Vec temp_point_inner(dim);
      int decimal_number_inner = j ;
      double distance = 0;
      for (size_t kk = 0; kk < dim; ++kk) {
         temp_point_inner[kk] = (decimal_number_inner %2 ==0) ? s[_k-1]->bounds.lo[kk]: s[_k-1]->bounds.hi[kk];
         decimal_number_inner /= 2;
         distance +=  (temp_point[kk]- temp_point_inner[kk]) * (temp_point[kk]- temp_point_inner[kk]);
      }
      if (distance > MaxMin)
        check =1;
    }
  }
  if (check == 0) {
    num_prunes += s[_k-1]->size() *  s[_k]->size();
    for(size_t i = s[_k]->begin(); i < s[_k]->end(); ++i) {
      distance_counter[i] += s[_k-1]->size();
      if ((distance_counter[i]  == src_data.size()) && ( valid_distance[i] > MaxMin) && (valid_distance[i] < DBL_MAX) )
          MaxMin = valid_distance[i];
    }
  }
  return (check == 0);
}

/**
 * Check which brach should be explored first.
 */
template<typename Tree, typename S, typename C>
inline
vector<int>
HD_rules<Tree,S,C>::visit (Box& s, int t) {
  vector<int> visit_order;
  vector< pair<real_t, int> >dist_index;
  for (int i = 0; i < s.num_child(); i++) {
    real_t score = distance_update(src_tree.node_data[s.child+i], t);
    dist_index.push_back( make_pair(score, i) );
  }
   struct {
        bool operator()(std::pair<real_t, int> a, std::pair<real_t, int> b) const{return a.first > b.first;}
  } customMore;
  sort(dist_index.begin(), dist_index.end(), customMore );
  for (int i = 0; i < dist_index.size(); i++)
    visit_order.push_back(dist_index[i].second);
  return visit_order;
}

template<typename Tree, typename S, typename C>
inline
vector<int>
HD_rules<Tree,S,C>::visit (Box& s, Box& t) {

  vector<int> visit_order;
  vector< pair<real_t, int> >dist_index;
  for (int i = 0 ; i < s.num_child(); i++) {
    real_t score = distance_update(src_tree.node_data[s.child+i], t);
    dist_index.push_back(make_pair(score, i));
  }
  struct {
        bool operator()(std::pair<real_t, int> a, std::pair<real_t, int> b) const{return a.first > b.first;}
  } customMore;

  sort(dist_index.begin(), dist_index.end(), customMore);
  for (int i = 0; i < dist_index.size(); i++)
    visit_order.push_back(dist_index[i].second);



    #ifdef _DEBUG
    real_t left_score  = distance_update(src_tree.node_data[s.child  ], t);
    real_t right_score = distance_update(src_tree.node_data[s.child+1], t);
    if (right_score > left_score) {
      if ((visit_order[0] !=1) or (visit_order[1] !=0)) {
        cout << s.index() << " , " << t.index() <<
                   " : <" << visit_order[0]  << " , "<< visit_order[1] << "> , (1,0)" << endl;
      exit(0);
      }
    }
    else {
      if ((visit_order[0] !=0) or (visit_order[1] !=1)) {
        cout << s.index() << " , " << t.index() <<
                   " : <" << visit_order[0]  << " , "<< visit_order[1] << "> , (0,1)" << endl;
      exit(0);
      }
    }

  #endif





  return visit_order;
}
template<typename Tree, typename S, typename C>
inline
vector<int>
HD_rules<Tree,S,C>::visit (vector<Box*> s, int div) {
  int next = (div == s.size() - 1) ? div - 1: div + 1;
  vector<int> visit_order;
  vector< pair<real_t, int> >dist_index;
  for (int i = 0 ; i < s[div]->num_child(); i++) {
    real_t score = distance_update(src_tree.node_data[s[div]->child+i],
                                   *s[next]);
    dist_index.push_back(make_pair(score, i));
  }
   struct {
        bool operator()(std::pair<real_t, int> a, std::pair<real_t, int> b) const{return a.first > b.first;}
  } customMore;
  sort(dist_index.begin(), dist_index.end(), customMore);
  for (int i = 0; i < dist_index.size(); i++)
    visit_order.push_back(dist_index[i].second);
  return visit_order;
}

/**
 * Update distance information.
 */
template<typename Tree, typename S, typename C>
inline
void
HD_rules<Tree,S,C>::update_state (Box& s, Box& t) {
  for (int i = 0; i < t.num_child(); i++)
    distance_update(s, trg_tree.node_data[t.child+i]);
}

template<typename Tree, typename S, typename C>
inline
void
HD_rules<Tree,S,C>::update_state (vector<Box*> s) {
  int k = 1;
  distance_update(s[k-1], trg_tree.node_data[s[k]->child  ]);
  distance_update(s[k-1], trg_tree.node_data[s[k]->child+1]);
}

/**
 * Intersection data is updated.
 */
template<typename Tree, typename S, typename C>
real_t
HD_rules<Tree,S,C>::distance_update (Box& s, int t) {
  int i = s.index();
  return  s.min_distance(trg_data[t]);
}

/**
 * Intersection data is computed.
 */
template<typename Tree, typename S, typename C>
real_t
HD_rules<Tree,S,C>::distance_update (Box& s, Box& t) {
  int i = s.index();
  int j = t.index();
  return  t.min_distance(s);
}
template<typename Tree, typename S, typename C>
real_t
HD_rules<Tree,S,C>::distance_update (vector<Box*> s) {
  int k = 1;
  int i = s[k-1]->index();
  int j = s[k]->index();
  return  s[k]->min_distance(*s[k-1]);
}

#endif
