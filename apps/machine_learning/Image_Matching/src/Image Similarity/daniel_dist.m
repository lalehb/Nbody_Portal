% daniel_dist() version 1.0
% By Daniel Jooryabi
% This function is used to calculate the distance between two images


function y = daniel_dist(file1, file2)

% check if image is already black and white

if (size(imread(file1),3) == 1)
    im1 = single(imread(file1)); 
else
    im1 = single(rgb2gray(imread(file1)));
end

if (size(imread(file2),3) == 1)
    im2 = single(imread(file2)); 
else
    im2 = single(rgb2gray(imread(file2)));
end


[f1,d1] = vl_sift(im1); 
[f2,d2] = vl_sift(im2);

ni = size(f1,2); 
nj = size(f2,2);

[matches,scores] = vl_ubcmatch(d1,d2);  

% cardM = size(matches,2); 

cardM = matchings(f1, f2, d1, d2); 

y = (ni + nj)/2 - cardM; 












end