
function accuracyTest()

% feel free to add more items to the test set
cd('C:\Users\Daniel\Documents\hpclab\apps\machine_learning\Image_Matching\res\caltech_images\');

SET = {'testset', ...
        'cup', ...
    'dollar_bill', ... % reference image
    'chair', ...
    'airplanes', ...
    'beaver', ...
    'camera', ...
    'butterfly', ...
    'cellphone', ...
    'crab', ...
    'crayfish', ...
    'accordion', ...
    'flamingo', ...
    'anchor', ...
    'barrel', ...
    'cougar_face', ...
    'crab', ...
    'crocodile', ...
    'crayfish', ...
    'dalmatian', ...
    'dolphin', ...
    'emu', ...
    'Faces_easy', ...
    'Faces', ...
    'rooster'
    };

% begin code
testSet = SET{1}; 
correctSet = 'cup';
trials = 0;
correct = 0;
NUMSET = 17; 
 
% loop over number of test images
parfor i = 1: NUMSET
    bestDistance = 1000000000;
     
    bestSet = 'null';
    trials = trials + 1;
    
    for p = 2: 6
        
        for j = 1: 40
            
            if (daniel_dist(strcat(testSet,'\', 'a(', num2str(i), ').jpg'), strcat(SET{p},'\','a(', num2str(j), ').jpg')) ...
                    < bestDistance)
                
                bestDistance = daniel_dist(strcat(testSet,'\', 'a(', num2str(i), ').jpg'), strcat(SET{p},'\','a(', num2str(j), ').jpg'));
                bestSet = SET{p};
            end
            %fprintf('working...\n'); 
        end
    end
    
    if (strcmp(bestSet, correctSet))
        correct = correct + 1;
       
    end
    
   % fprintf('\n%.2f done\n', i/NUMSET * 100); 
    
end

accuracy = correct / trials * 100; 

fprintf('Testing done...\nAccuracy is %f%%\n', accuracy);



end

function printResult(im1,im2,dist)

fprintf(strcat(im1, '<---->', im2, '===', num2str(dist), '\n'));
end