
% This function can be used to test distance between batches of images
function y = batchTest()
    
tic; 
A_B = 0;
A_A = 0; 
B_B = 0; 



% the reference image from set A must be labeled as a(1).jpg
% Likewise for set B

parfor i = 1 : 40
   
    
    A_B = A_B + daniel_dist('a(1).jpg', strcat('b(', num2str(i), ').jpg'));
    
    
end

parfor i = 2: 41
    
    B_B = B_B + daniel_dist('b(1).jpg', strcat('b(', num2str(i), ').jpg'));
    
    
end

parfor i = 2 : 41
        A_A = A_A + daniel_dist('a(1).jpg', strcat('a(', num2str(i), ').jpg'));
    
end

% normalization process to get average distance
A_B = A_B / 40; 
A_A = A_A / 40; 
B_B = B_B / 40; 

fprintf('image A with Set B average distance = %f\n', A_B);
fprintf('image A with Set A average distance = %f\n', A_A);
fprintf('image B with Set B average distance = %f\n', B_B);

y = 'Done matching';
toc; 
end