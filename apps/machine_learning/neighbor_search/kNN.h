#ifndef _KNN_H_
#define _KNN_H_
#define prune_type unsigned long long

#include "kNN_rules.h"
#include "Single_tree_traversal_Pascal.h"
#include "Dual_tree_traversal_Pascal.h"
#include "Multi_tree_traversal_Pascal.h"
#include "Metric.h"
#include "Clock.hpp"
#include <omp.h>

template <typename Tree>
class kNN {
  public:
    int dim;

    /* Pointer to the source tree */
    Tree& src_tree;
    /* Pointer to the target tree */
    Tree& trg_tree;


    // prune_generator<Tree> *prune_g;
    int pg_exist = 0 ;

    int multi;;
    int level;

    MetricType* metric;

    kNN(int d, int l, Tree& st, Tree& tt, int multi_tree = 0) : dim(d), level(l), src_tree(st), trg_tree(tt), multi(multi_tree),  metric(new SquaredEuclideanMetricType()) { }

    kNN(int d, Tree& st, Tree& tt ) : dim(d), src_tree(st), trg_tree(tt),  metric(new SquaredEuclideanMetricType()) { }

    kNN(int d, Tree& st, Tree& tt, MetricType* m) : dim(d), src_tree(st), trg_tree(tt), metric(m) { }

    /* Get the k nearest neighbours of a point. Estimated average cost: O(log k log n) */
    void nearest_neighbors (unsigned int k, resultNeighbors& neighbors, real_t epsilon) const ;
};

// Template instantiation

/**
 * Find the K nearest neighbors of a given point
 *
 * \param p         Points whose \a k nearest neighbors should be found
 * \param k         Number of nearest neighbors to find
 * \param neighbor  Array where the nearest neighbors are stored in sorted order
 * \param epsilon   Distance margin to ignore points for approximate NN calculation
 */

template<typename Tree>
void
kNN<Tree>::nearest_neighbors (unsigned int k, resultNeighbors& neighbors, real_t epsilon) const {
  /* Check if source-tree or dual-tree search should be used */
  int single_mode = getenv__single_mode();
  prune_type num_prunes = 0;

  /* Create a vector to store the current k nearest neighbors */
  // vector<kvector> current_k(trg_tree.points(), kvector(k));
  vector<kvector> current_k(trg_tree.points_column_major(), kvector(k));
  cout << "in neghibors--------> " << current_k.size() << " , " << current_k[0].size() << "\n";

  Clock timer;
  double time_knn;
  if (single_mode) {
    /* Create a helper object for tree traversal and intersection calculation */
    typedef kNN_rules<Tree, Vec> Rule;
    // Rule rules(dim, k, src_tree, trg_tree, src_tree.data_perm, trg_tree.data, metric, current_k);
    Rule rules(dim, k, src_tree, trg_tree, src_tree.data_perm, trg_tree.data, metric, current_k);

    /* Generate the rule with the prune generator */
    // if (pg_exist)
    //   Rule rules(dim, k, src_tree, trg_tree, src_tree.data_perm, trg_tree.data, metric, current_k, pg_exist, prune_g);


    /* Create the traverser */
    SingleTreeTraversal<Tree, Rule> traverser(src_tree, rules, num_prunes);

    timer.start();
    for (size_t i = 0; i < trg_tree.points(); ++i) {
      /* Start traversal from the root of the tree */
      traverser.traverse (src_tree.root(), i);
      traverser.printVal();
    }
    time_knn = timer.seconds();
  }
  else {
    /* Create a helper object for tree traversal and intersection calculation */
    typedef kNN_rules<Tree, Mat> Rule;
    Rule rules(dim, k, src_tree, trg_tree, src_tree.data_perm, trg_tree.data_perm, metric, current_k);

    /* Generate the rule with the prune generator */
    // if (pg_exist)
    //   Rule rules(dim, k, src_tree, trg_tree, src_tree.data_perm, trg_tree.data_perm, metric, current_k, pg_exist, prune_g);


    if (multi == 0) {
      /* Create the traverser */
      DualTreeTraversal<Tree, Rule, Rule > traverser(src_tree, trg_tree, rules, rules, num_prunes, level);

      /* Start traversal from the root of the trees */
      timer.start();
      traverser.traverse (src_tree.root(), trg_tree.root());
      time_knn = timer.seconds();
    }
    else {

      vector<Tree*> trees;
      trees.push_back(&src_tree);
      trees.push_back(&trg_tree);

      /* Create the traverser */
      MultiTreeTraversal<Tree, Rule, Rule > traverserM(trees, rules, rules, num_prunes, level);

      /* Start traversal from the root of the trees */
      timer.start();
      traverserM.traverse ();
      time_knn = timer.seconds();
    }
  }

  /* Push the nearest neighbors to the neighbor vector in the order of increasing distance */
  for (size_t i = 0; i < trg_tree.points_column_major(); ++i) {
    int k;
    int id = 0;
    if (!single_mode)
      k = trg_tree.index[i];
    else
      k = i;
    for (auto j = current_k[i].begin(); j < current_k[i].end(); ++j) {
      kNN_distance n = *j;
      n.index = src_tree.index[n.index];
      neighbors[k][id] = n;
      id++;
    }
  }




  // for (size_t i = 0; i < trg_tree.points(); ++i) {
  //   int k;
  //   int id = 0;
  //   if (!single_mode)
  //     k = trg_tree.index[i];
  //   else
  //     k = i;
  //   for (auto j = current_k[i].begin(); j < current_k[i].end(); ++j) {
  //     kNN_distance n = *j;
  //     n.index = src_tree.index[n.index];
  //     neighbors[k][id] = n;
  //     id++;
  //   }
  // }
  if (single_mode)
    cerr << "Single-tree kNN time: " << time_knn << " seconds\n";
  else if (!multi)
    cerr << "Dual-tree kNN time: " << time_knn << " seconds\n";
  else
     cerr << "Multi-tree kNN time: " << time_knn << " seconds\n";
}


#endif
