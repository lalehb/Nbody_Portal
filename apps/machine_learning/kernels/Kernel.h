#ifndef _KERNEL_H_
#define _KERNEL_H_

#include <boost/math/special_functions/gamma.hpp>
#include "Metric.h"


/* the abstract class for the Kernel definition */
class Kernel
{

// MetricType* metric;

  public:
  /* Construct  kernel */
  Kernel () { }

  /* Evaluates the kernel betweem the two points */
  virtual real_t compute (const Point& a, const Point&b) const = 0 ;

  /* Evaluates the kernel given the squared distance between two points */
  virtual real_t compute (const real_t sqdist) const = 0 ;

  /* Returns the normalization constant of the kernel  */
  virtual real_t norm (size_t dim) = 0 ;
};

#endif
