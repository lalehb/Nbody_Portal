#ifndef _GENERAL_KERNEL_H_
#define _GENERAL_KERNEL_H_

#include <boost/math/special_functions/gamma.hpp>
#include "Metric.h"
#include "Kernel.h"
#include "exprtk.hpp"

typedef exprtk::expression<real_t>     expression_t;
typedef Metric<Point, Point> MetricType;

class GeneralKernel : public Kernel
{

  MetricType* metric;
  expression_t expr;
  public:

  /* Construct general kernel */
  GeneralKernel (expression_t E, MetricType* m): metric(m), expr(E) { }

  /* Evaluates the kernel */
  real_t compute (const Point& a, const Point&b) const {
    real_t d = metric->compute_distance(a,b);
    // symbol_table_t symbol_table;
    // symbol_table.add_variable("D",d);
    // expr.register_symbol_table(symbol_table);
    return expr.value(); 
  }

  /* Evaluates the kernel given the squared distance between two points */
  real_t compute (const real_t sqdist) const {
    real_t d = sqdist;
    // symbol_table_t symbol_table;
    // symbol_table.add_variable("D",d);
    // expr.register_symbol_table(symbol_table);
    return expr.value();
  }

  /* Returns the normalization constant of the kernel  */
  real_t norm (size_t dim) {
    return 1;
  }
};

#endif
