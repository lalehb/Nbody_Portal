#ifndef _SPHERICAL_KERNEL_H_
#define _SPHERICAL_KERNEL_H_

#include <boost/math/special_functions/gamma.hpp>
#include "Metric.h"
#include "Kernel.h"

typedef EuclideanMetric<Point, Point> EuclideanMetricType;
//typedef Metric<Point, Point> MetricType;

/** @brief Spherical kernel
 *
 * Formally described as
 *
 * \f[
 *     K(x, y) = 1 - \frac{3}{2} \frac{|x-y|}{\sigma}
 *     + \frac{1}{2} \left( \frac{|x-y|}{\sigma} \right)^3
 *     \mbox{if}~ |x-y| \leq \sigma \mbox{, zero otherwise}
 * \f]
 *
 */

class SphericalKernel : public Kernel
{
  /* Kernel bandwidth */
  real_t bandwidth;

  MetricType* metric;

  public:
  /* Construct gaussian kernel */
  SphericalKernel (real_t width) : bandwidth(width), metric(new EuclideanMetricType()) { }

  /* Evaluates the kernel */
  real_t compute (const Point& a, const Point&b) const {
    real_t dist = metric->compute_distance(a, b);
    real_t dbratio = dist/bandwidth;
    if (dist < bandwidth)
      return 1.0 - 1.5 * dbratio + 1.5 * pow(dbratio, 3);
    else
      return 0;
  }

  /* Evaluates the kernel given the squared distance between two points */
  real_t compute (const real_t sqdist) const {
    real_t dist = std::sqrt(sqdist);
    real_t dbratio = dist/bandwidth;
    if (dist < bandwidth)
      return 1.0 - 1.5 * dbratio + 1.5 * pow(dbratio, 3);
    else
      return 0;
  }

  /* Returns the normalization constant of the Spherical kernel
   * given by 1/Vd where Vd is the volume of a d-dimensiional
   * hypersphere.
   */
  real_t norm (size_t dim) {
    return boost::math::tgamma(dim / 2.0 + 1.0) /
      (pow(bandwidth, dim) * std::pow(M_PI, dim / 2.0));
  }
};

#endif
