#ifndef _METRIC_H_
#define _METRIC_H_

#include <algorithm>
// #include "Binary_tree.h"
#include "Simd_helper.h"
#include <omp.h>
#include <pmmintrin.h>

#if !defined (USE_FLOAT)
/** \brief Floating-point type for a real number. */
typedef double real_t;
#else
/** \brief Floating-point type for a real number. */
typedef float real_t;
#endif


template <typename Point1Type, typename Point2Type>
class Metric
{
  public:
    Metric() {};

    /* Compute the distance between two points */
    virtual real_t compute_distance (const Point1Type& p, const Point2Type& q) const = 0;

    /* Compute the distance between two points with early exit*/
    virtual real_t compute_distance (const Point1Type& p, const Point2Type& q, real_t upper_bound) const = 0;
};

template <typename Point1Type, typename Point2Type>
class EuclideanMetric : public Metric<Point1Type, Point2Type>
{
  public:
    EuclideanMetric() {};

    /**
     * Compute the Euclidean distance between any two d-dimentional
     * points/elements.
     */
    inline real_t compute_distance (const Point1Type& p, const Point2Type& q) const {
      // assert (p.size() == q.size());
      size_t dim = p.size();
      // dim = 2;
      real_t sum = 0.0;

      size_t i;
      // SIMD_REG SUM__ = SIMD_ZERO;
      // for (i = 0; (i+SIMD_LEN) <= dim; i+=SIMD_LEN) {
      //   SIMD_REG D__ = SIMD_SUB (SIMD_LOAD_U (&p[i]), SIMD_LOAD_U (&q[i]));
      //   SUM__ = SIMD_ADD(SUM__, SIMD_MUL (D__, D__));
      // }
      // SUM__ = SIMD_HADD(SUM__, SUM__);
      // SIMD_STORE_S(&sum, SUM__);

      // for (; i < dim; i++)
      //   sum += (p[i] - q[i]) * (p[i] - q[i]);
     // #pragma omp paralle for simd reduction(+:sum)
     for (size_t i = 0; i < dim; ++i)
       sum += (p[i] - q[i]) * (p[i] - q[i]);
     // return sum;
      return sqrt(sum);
    }


    /**
     * Compute the Euclidean distance between any two d-dimentional
     * points/elements. Exit the function if the upper bound value is
     * reached.
     */
    inline real_t compute_distance (const Point1Type& p, const Point2Type& q, real_t upper_bound) const {
      assert (p.size() == q.size());
      size_t dim = p.size();
      // dim = 2;
      real_t sum = 0.0;

      // Calculate constant for loop limit
      const size_t dim_cutoff = (size_t) (0.4 * dim);

      // Compute squared distance in two stages. First stage, accumate partial result w/o comparisons
      for (size_t i = 0; i < dim_cutoff; ++i) {
        sum += (p[i] - q[i]) * (p[i] - q[i]);
      }

      // Second stage accumate with comparisons
      for (size_t i = dim_cutoff; i < dim; ++i) {
        sum += (p[i] - q[i]) * (p[i] - q[i]);
        if (sum > upper_bound)
          break;
      }

      return sqrt(sum);
    }
};

template <typename Point1Type, typename Point2Type>
class SquaredEuclideanMetric : public Metric<Point1Type, Point2Type>
{
  public:
    SquaredEuclideanMetric() {};

    /**
     * Compute the squared Euclidean distance between any two d-dimentional
     * points/elements.
     */
    inline real_t compute_distance (const Point1Type& p, const Point2Type& q) const {
      assert (p.size() == q.size());
      size_t dim = p.size();
      // dim = 2;
      real_t sum = 0.0;

      size_t i;
      // SIMD_REG SUM__ = SIMD_ZERO;
      // for (i = 0; (i+SIMD_LEN) <= dim; i+=SIMD_LEN) {
      //   SIMD_REG D__ = SIMD_SUB (SIMD_LOAD_U (&p[i]), SIMD_LOAD_U (&q[i]));
      //   SUM__ = SIMD_ADD(SUM__, SIMD_MUL (D__, D__));
      // }
      // SUM__ = SIMD_HADD(SUM__, SUM__);
      // SIMD_STORE_S(&sum, SUM__);
      //
      // for (; i < dim; i++)
      //   sum += (p[i] - q[i]) * (p[i] - q[i]);


      for (size_t i = 0; i < dim; ++i)
        sum += (p[i] - q[i]) * (p[i] - q[i]);

      return sum;
    }


    /**
     * Compute the squared Euclidean distance between any two d-dimentional
     * points/elements. Exit the function if the upper bound value is
     * reached.
     */
    inline real_t compute_distance (const Point1Type& p, const Point2Type& q, real_t upper_bound) const {
      assert (p.size() == q.size());
      size_t dim = p.size();
      dim = 2;
      real_t sum = 0.0;

      // Calculate constant for loop limit
      const size_t dim_cutoff = (size_t) (0.25 * dim);

      // Compute squared distance in two stages. First stage, accumate partial result w/o comparisons
      for (size_t i = 0; i < dim_cutoff; ++i) {
        sum += (p[i] - q[i]) * (p[i] - q[i]);
      }

      // Second stage accumate with comparisons
      for (size_t i = dim_cutoff; i < dim; ++i) {
        sum += (p[i] - q[i]) * (p[i] - q[i]);
        if (sum > upper_bound)
          break;
      }
      return sum;
    }
};

template <typename Point1Type, typename Point2Type>
class ManhattanMetric : public Metric<Point1Type, Point2Type>
{
  public:
    ManhattanMetric() {};

    /**
     * Compute the Manhattan distance between any two d-dimentional
     * points/elements.
     */
    inline real_t compute_distance (const Point1Type& p, const Point2Type& q) const {
      assert (p.size() == q.size());
      size_t dim = p.size();
      real_t sum = 0.0;

      for (size_t i = 0; i < dim; ++i)
        sum += abs(p[i] - q[i]);

      return sum;
    }


    /**
     * Compute the Manhattan distance between any two d-dimentional
     * points/elements. Exit the function if the upper bound value is
     * reached.
     */
    inline real_t compute_distance (const Point1Type& p, const Point2Type& q, real_t upper_bound) const {
      assert (p.size() == q.size());
      size_t dim = p.size();
      // dim = 2;
      real_t sum = 0.0;

      // Calculate constant for loop limit
      const size_t dim_cutoff = (size_t) (0.4 * dim);

      // Compute squared distance in two stages. First stage, accumate partial result w/o comparisons
      for (size_t i = 0; i < dim_cutoff; ++i) {
        sum += abs(p[i] - q[i]);
      }

      // Second stage accumate with comparisons
      for (size_t i = dim_cutoff; i < dim; ++i) {
        sum += abs(p[i] - q[i]);
        if (sum > upper_bound)
          break;
      }

      return sum;
    }
};

template <typename Point1Type, typename Point2Type>
class ChebyshevMetric : public Metric<Point1Type, Point2Type>
{
  public:
    ChebyshevMetric() {};

    /**
     * Compute the Chebyshev distance between any two d-dimentional
     * points/elements.
     */
    inline real_t compute_distance (const Point1Type& p, const Point2Type& q) const {
      assert (p.size() == q.size());
      size_t dim = p.size();
      // dim = 2;
      real_t result = 0.0;

      for (size_t i = 0; i < dim; ++i)
        result = max(result, abs(p[i] - q[i]));

      return result;
    }


    /**
     * Compute the Chebyshev distance between any two d-dimentional
     * points/elements. Exit the function if the upper bound value is
     * reached.
     */
    inline real_t compute_distance (const Point1Type& p, const Point2Type& q, real_t upper_bound) const {
      assert (p.size() == q.size());
      size_t dim = p.size();
      // dim = 2;
      real_t result = 0.0;

      // Calculate constant for loop limit
      const size_t dim_cutoff = (size_t) (0.4 * dim);

      // Compute squared distance in two stages. First stage, accumate partial result w/o comparisons
      for (size_t i = 0; i < dim_cutoff; ++i) {
        result = max(result, abs(p[i] - q[i]));
      }

      // Second stage accumate with comparisons
      for (size_t i = dim_cutoff; i < dim; ++i) {
        result = max(result, abs(p[i] - q[i]));
        if (result > upper_bound)
          break;
      }

      return result;
    }
};



/* TODO
 * Impelement this for general distance function if we decided that we want a general distance function
 */
template <typename Point1Type, typename Point2Type>
class GeneralMetric : public Metric<Point1Type, Point2Type>
{
  public:
    GeneralMetric() {};

    /**
     * Compute the user-defined distance between any two d-dimentional
     * points/elements.
     */
    inline real_t compute_distance (const Point1Type& p, const Point2Type& q) const {
      assert (p.size() == q.size());
      size_t dim = p.size();
      // dim = 2;
      real_t sum = 0.0;

      return sum;
    }


    /**
     * Compute the user-defined  distance between any two d-dimentional
     * points/elements. Exit the function if the upper bound value is
     * reached.
     */
    inline real_t compute_distance (const Point1Type& p, const Point2Type& q, real_t upper_bound) const {
      assert (p.size() == q.size());
      size_t dim = p.size();
      // dim = 2;
      real_t sum = 0.0;

      return sum;
    }

};
#endif
