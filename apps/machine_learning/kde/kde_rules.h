#ifndef _KDE_RULES_H_
#define _KDE_RULES_H_

#include <cfloat>
#include <algorithm>
// #include "Binary_tree.h"
#include "Metric.h"
//#include "prune_generator.cpp"

typedef Metric<Point, Point> MetricType;
typedef SquaredEuclideanMetric<Point, Point> SquaredEuclideanMetricType;

/* Helper structure for tree traversal and incremental hyperrectangle-hypersphere intersection data */
template <typename Tree, typename S, typename KernelType>
class KDERules {
  public:
    typedef typename Tree::NodeTree Box;
    int dim;

    real_t atol;
    real_t rtol;

    /* Kernel */
    KernelType& kernel;

    /* Pointer to the source tree */
    Tree& src_tree;
    /* Pointer to the target tree */
    Tree& trg_tree;

    /* Points to permutated source array */
    Points_t& src_data;
    /* Reference input target array */
    Points_t& trg_data;

    /* prune generation would be used */
    int pg_rule = 0;
//    prune_generator<Tree> *prune_rule;

    Vec& density;

    MetricType* metric;

    S min_bound;
    S max_bound;
    S spread;

    real_t global_bound;
    real_t global_spread;
    real_t kernel_norm;

    KDERules(int d, real_t atol, real_t rtol, KernelType& k, Tree& st, Tree& tt, Points_t& s, Points_t& t, Vec& density, MetricType* m) :
             dim(d), atol(atol), rtol(rtol), kernel(k), src_tree(st), trg_tree(tt), src_data(s), trg_data(t), density(density), metric(m),
             min_bound(), max_bound(), spread(), global_bound(0.0), global_spread(0.0), kernel_norm(kernel.norm(dim)) {}

   // KDERules(int d, real_t atol, real_t rtol, KernelType& k, Tree& st, Tree& tt, Points& s, Points& t, Vec& density, MetricType* m, int pg,
   //          prune_generator<Tree> *pg_r) : dim(d), atol(atol), rtol(rtol), kernel(k), src_tree(st), trg_tree(tt), src_data(s), trg_data(t), density(density)             , metric(m), min_bound(), max_bound(), spread(), global_bound(0.0), global_spread(0.0), kernel_norm(kernel.norm(dim)) {}
    /* Base case calculation */
    void base_case (Box& s, int t);
    void base_case (Box& s, Box& t);
    void base_case (vector<Box*> t);

    /* Check if subtree should be pruned */
    bool prune_subtree (Box& s, int t);
    bool prune_subtree (Box& s, Box& t);
    bool prune_subtree (vector<Box*> t);

    /* Calculate which branch to visit first */
    std::vector<int> visit (Box& s, int t);
    std::vector<int> visit (Box& s, Box& t);
    std::vector<int> visit (vector<Box*> t, int div);

    /* Update distance */
    void update_state (Box& s, Box& t);
    void update_state (vector<Box*> t);

    /* Update the current value of the distance to the nearest point in the hyperrectangle */
    void bound_update (Box& s, int t);
    void bound_update (Box& s, Box& t);
    void bound_update (vector<Box*> t);

    /* centorid case computation for the subtree is will be prunes, in this case since it's prune it does nothing*/
    void centroid_case (Box& s, int t) {};
    void centroid_case (Box& s, Box& t) {};
    void centroid_case (vector<Box*> t) {};

    void set_temp(int temp) {};
    void adjustPartitionCounter(int , int , int , int){};
    double get_temp(){};
};

// Template implementation
template<typename Tree, typename S, typename Kernel>
void
KDERules<Tree,S,Kernel>::base_case (Box& s, int t) {
  real_t kval;
  global_bound = global_bound - min_bound[s.index()];
  global_spread = global_spread - spread[s.index()];

  for (size_t j = s.begin(); j < s.end(); ++j) {
    kval = kernel.compute (trg_data[t], src_data[j]);
    global_bound += kval;
  }
}

template<typename Tree, typename S, typename Kernel>
void
KDERules<Tree,S,Kernel>::base_case (Box& s, Box& t) {
  real_t kval;
    global_bound = global_bound - min_bound[s.index()][t.index()];
    global_spread = global_spread - spread[s.index()][t.index()];

  real_t bandwidth = kernel.bandwidth;
  for (size_t i = t.begin(); i < t.end(); ++i) {
    real_t q = 0.0;
    for (size_t d = 0; d < dim; ++d) {
      for (size_t j = s.begin(); j < s.end(); ++j) {
          q += exp((-0.5 * pow(bandwidth, -2.0)) * ((trg_data[d][i] - src_data[d][j]) * (trg_data[d][i] - src_data[d][j])));
        }
    }
    global_bound += q;
    density[i] += q;
  }
}

 /* k is set to 1 for KNN with two dataset and has been used
  * for the multi-tree, in the case of an algorithm with more
  * than two dataset the kernel function and k should have
  * change accordingly
  */


template<typename Tree, typename S, typename Kernel>
void
KDERules<Tree,S,Kernel>::base_case (vector<Box*> s) {
  real_t kval;
  int k = 1;
  global_bound = global_bound - min_bound[s[k-1]->index()][s[k]->index()];
  global_spread = global_spread - spread[s[k-1]->index()][s[k]->index()];

  for (size_t i = s[k]->begin(); i < s[k]->end(); ++i) {
    real_t q = 0.0;
    for (size_t j = s[k-1]->begin(); j < s[k-1]->end(); ++j) {
      kval = kernel.compute (trg_data[i], src_data[j]);
      q += kval;
    }
    global_bound += q;
    density[i] += q;
  }
}

/**
 * Check if the hypersphere from the current farthest neighbor intersects the hyperrectange.
 */
template<typename Tree, typename S, typename Kernel>
inline
bool
KDERules<Tree,S,Kernel>::prune_subtree (Box& s, int t) {
  int i = s.index();
  int num_pts = src_tree.points();
  /* If local bound criterion is met, prune */
  if (((spread[i] * num_pts * kernel_norm) / s.size()) < (atol + (rtol * min_bound[i] * kernel_norm)))
    return true;

  /* If global criterion is met, prune */
  if ((global_spread * kernel_norm) < (atol + (rtol * global_bound * kernel_norm)))
    return true;
  return false;
}

/**
 * Check if the hypersphere from the current farthest neighbor intersects the hyperrectange.
 */
template<typename Tree, typename S, typename Kernel>
inline
bool
KDERules<Tree,S,Kernel>::prune_subtree (Box& s, Box& t) {
  int i = s.index();
  int j = t.index();
  int num_pts = src_tree.points() * trg_tree.points();

  /* Compute local bound criterion */
  bool local_criterion = ((spread[i][j] * num_pts * kernel_norm) / (s.size() * t.size()) < (atol + (rtol *  min_bound[i][j] * kernel_norm)));

  /* Compute global bound criterion */
  bool global_criterion = (global_spread * kernel_norm) < (atol + (rtol * global_bound * kernel_norm));

  /* If local or global criterion is met, increase log density of all target/query points by K(mean + spread/2) */
  if (global_criterion || local_criterion) {
    real_t center_density = (min_bound[i][j] + spread[i][j]/2) / t.size();
    for (size_t k = t.begin(); k < t.end(); ++k)
      density[k] += center_density;
    return true;
  }
  return false;
}

template<typename Tree, typename S, typename Kernel>
inline
bool
KDERules<Tree,S,Kernel>::prune_subtree (vector<Box*> s) {
  int k = 1;
  int i = s[k-1]->index();
  int j = s[k]->index();
  int num_pts = src_tree.points() * trg_tree.points();

  /* Compute local bound criterion */
  bool local_criterion = ((spread[i][j] * num_pts * kernel_norm) / (s[k-1]->size() * s[k]->size()) < (atol + (rtol *  min_bound[i][j] * kernel_norm)));

  /* Compute global bound criterion */
  bool global_criterion = (global_spread * kernel_norm) < (atol + (rtol * global_bound * kernel_norm));

  /* If local or global criterion is met, increase log density of all target/query points by K(mean + spread/2) */
  if (global_criterion || local_criterion) {
    real_t center_density = (min_bound[i][j] + spread[i][j]/2) / s[k]->size();
    for (size_t f = s[k]->begin(); f < s[k]->end(); ++f)
      density[f] += center_density;
    return true;
  }
  return false;
}

/**
 * Check which brach should be explored first.
 */
template<typename Tree, typename S, typename Kernel>
inline
vector<int>
KDERules<Tree,S,Kernel>::visit (Box& s, int t) {
  vector<Box>& stree = src_tree.node_data;
  bound_update(stree[s.child], t);
  bound_update(stree[s.child+1], t);

  /* Update global bounds */
  global_bound = global_bound - min_bound[s.index()];
  global_bound = global_bound + min_bound[s.child];
  global_bound = global_bound + min_bound[s.child+1];

  global_spread = global_spread - spread[s.index()];
  global_spread = global_spread + spread[s.child];
  global_spread = global_spread + spread[s.child+1];

  /* Traversal order doesn't matter, return left */
  return vector<int> {0,1};
}

/**
 * Check which brach should be explored first.
 */
template<typename Tree, typename S, typename Kernel>
vector<int>
KDERules<Tree,S,Kernel>::visit (Box& s, Box& t) {
  int j = t.index();
  vector<int> visit_order;
  vector<Box>& stree = src_tree.node_data;
  for (int i = 0; i < s.num_child(); i++) {
    visit_order.push_back(i);
    bound_update(stree[s.child+i], t);
    global_bound = global_bound + min_bound[s.child+i][j];
    global_spread = global_spread + spread[s.child+i][j];
  }

  /* Update global bounds */
  global_bound = global_bound - min_bound[s.index()][j];
  global_spread = global_spread - spread[s.index()][j];
  return vector<int> {0,1};
  // return visit_order;
}

template<typename Tree, typename S, typename Kernel>
inline
vector<int>
KDERules<Tree,S,Kernel>::visit (vector<Box*> s, int div) {
  int next = (div == s.size() - 1) ? div - 1 : div + 1;
  int j = s[next]->index();
  vector<int> visit_order;
  vector<Box>& stree = src_tree.node_data;
  for (int i = 0; i < s[div]->num_child(); ++i) {
    visit_order.push_back(i);
    bound_update(stree[s[div]->child+i], *s[next]);
    global_bound = global_bound + min_bound[s[div]->child+i][j];
    global_spread = global_spread + spread[s[div]->child+i][j];
  }
  /* Update global bounds */
  global_bound = global_bound - min_bound[s[div]->index()][j];

  global_spread = global_spread - spread[s[div]->index()][j];

  /* Traversal order doesn't matter, return left */
  return visit_order;
}

/**
 * Update distance information.
 */
template<typename Tree, typename S, typename Kernel>
inline
void
KDERules<Tree,S,Kernel>::update_state (Box& s, Box& t) {
  int i = s.index();
  vector<Box>& ttree = trg_tree.node_data;
  for (int j = 0; j < t.num_child(); j++) {
    bound_update(s, ttree[t.child+j]);
    global_bound = global_bound + min_bound[i][t.child+j];
    global_spread = global_spread + spread[i][t.child+j];
  }
  /* Update global bounds */
  global_bound = global_bound - min_bound[i][t.index()];

  global_spread = global_spread - spread[i][t.index()];
}

template<typename Tree, typename S, typename Kernel>
inline
void
KDERules<Tree,S,Kernel>::update_state (vector<Box*> s) {
  int i = s[0]->index();
  vector<Box>& ttree = trg_tree.node_data;
  bound_update(s[0], ttree[s[1]->child]);
  bound_update(s, ttree[s[1]->child+1]);

  /* Update global bounds */
  global_bound = global_bound - min_bound[i][s[1]->index()];
  global_bound = global_bound + min_bound[i][s[1]->child];
  global_bound = global_bound + min_bound[i][s[1]->child+1];

  global_spread = global_spread - spread[i][s[1]->index()];
  global_spread = global_spread + spread[i][s[1]->child];
  global_spread = global_spread + spread[i][s[1]->child+1];
}
/**
 * Intersection data is computed.
 */
template<typename Tree, typename S, typename Kernel>
void
KDERules<Tree,S,Kernel>::bound_update (Box& s, int t) {
  int i = s.index();
  Range dist = s.range_distance(trg_data[t]);
  min_bound[i] = s.size() * kernel.compute(dist.max());
  max_bound[i] = s.size() * kernel.compute(dist.min());
  spread[i] = max_bound[i] - min_bound[i];
}

/**
 * Intersection data is computed.
 */
template<typename Tree, typename S, typename Kernel>
void
KDERules<Tree,S,Kernel>::bound_update (Box& s, Box& t) {
  int i = s.index();
  int j = t.index();
  Range dist = t.range_distance(s);
  min_bound[i][j] = t.size() * s.size() * kernel.compute(dist.max());
  max_bound[i][j] = t.size() * s.size() * kernel.compute(dist.min());
  spread[i][j] = max_bound[i][j] - min_bound[i][j];
}

#endif
