#ifndef _KDE_H_
#define _KDE_H_

#define prune_type unsigned long long

// #include "Binary_tree.h"
#include "kde_rules.h"
#include "Single_tree_traversal_Pascal.h"
#include "Dual_tree_traversal_Pascal.h"
#include "Multi_tree_traversal_Pascal.h"
// #include "Dual_tree_traversal.h"
// #include "Multi_tree_traversal.h"
#include "Metric.h"
//#include "prune_generator.cpp"

template <typename Tree, typename KernelType>
class KernelDensity {
  public:
    int dim;

    /* Pointer to the source tree */
    Tree& src_tree;
    /* Pointer to the target tree */
    Tree& trg_tree;

    MetricType* metric;
    
    /* level for parallelization in traversal*/
    int level;

    int pg_rule = 0 ;
    int multi;

    KernelDensity(int d, Tree& st, Tree& tt, int multi_tree = 0) : dim(d), src_tree(st), trg_tree(tt), multi(multi_tree), metric(new SquaredEuclideanMetricType()) { }
    
    KernelDensity(int d, int l, Tree& st, Tree& tt, int multi_tree = 0) : dim(d), level(l), src_tree(st), trg_tree(tt), multi(multi_tree), metric(new SquaredEuclideanMetricType()) { }
    
    KernelDensity(int d, Tree& st, Tree& tt, MetricType* m, int multi_tree = 0) : dim(d), src_tree(st), trg_tree(tt), multi(multi_tree), metric(m) { }

    /* Compute the log of kernel density */
    void compute_kernel_density (int num_pts, KernelType& kernel, Vec& density, real_t atol, real_t rtol) const;
};

// Template instantiation

/**
 * Compute the log of kernel density
 *
 * \param h         Width of the kernel
 * \param density   Array where the kernel density is stored
 * \param atol      Absolute tolerance value
 * \param rtol      Relative tolerance value
 */

template <typename Tree, typename KernelType>
void
KernelDensity<Tree,KernelType>::compute_kernel_density (int num_pts, KernelType& kernel, Vec& density, real_t atol, real_t rtol) const {
  /* Check if source-tree or dual-tree search should be used */
  int single_mode = getenv__single_mode();
  prune_type num_prunes = 0;

  clock_t ts_kde = clock();
  if (single_mode) {
    /* Create a helper object for tree traversal and intersection calculation */
    typedef KDERules<Tree, Vec, KernelType> Rule;

    real_t natol = atol * num_pts;
    Rule rules(dim, natol, rtol, kernel, src_tree, trg_tree, src_tree.data_perm, trg_tree.data, density, metric);
    rules.min_bound = Vec(src_tree.nodes());
    rules.max_bound = Vec(src_tree.nodes());
    rules.spread = Vec(src_tree.nodes());

    /* Create the traverser */
    SingleTreeTraversal<Tree, Rule> traverser(src_tree, rules, num_prunes);

    for (size_t i = 0; i < trg_tree.points(); ++i) {
      /* Compute bounds on density for the root node */
      Box root = src_tree.root();
      size_t rid = root.index();
      Range dist = root.range_distance(trg_tree.data[i]);
      rules.min_bound[rid] = root.size() * kernel.compute(dist.max());
      rules.max_bound[rid] = root.size() * kernel.compute(dist.min());
      rules.spread[rid] = rules.max_bound[rid] - rules.min_bound[rid];
      rules.global_bound = rules.min_bound[rid];
      rules.global_spread = rules.spread[rid];

      /* Start traversal from the root of the tree */
      traverser.traverse (root, i);
      density[i] = (rules.global_bound + (rules.global_spread/2)) * rules.kernel_norm;
    }
  }
  else {
    /* Create a helper object for tree traversal and intersection calculation */
    typedef KDERules<Tree, Mat, KernelType> Rule;
    real_t natol = atol * trg_tree.points() * src_tree.points();
    for (size_t i = 0; i < trg_tree.points(); ++i)
      density[i] = 0.0;
    Rule rules(dim, natol, rtol, kernel, src_tree, trg_tree, src_tree.data_perm, trg_tree.data_perm, density, metric);
    rules.min_bound = Mat(src_tree.nodes(), trg_tree.nodes());
    rules.max_bound = Mat(src_tree.nodes(), trg_tree.nodes());
    rules.spread = Mat(src_tree.nodes(), trg_tree.nodes());


    /* Compute bounds on density for the root node */
    Box sroot = src_tree.root();
    Box troot = trg_tree.root();
    size_t sid = sroot.index();
    size_t tid = troot.index();
    Range dist = troot.range_distance(sroot);
    rules.min_bound[sid][tid] = troot.size() * sroot.size() * kernel.compute(dist.max());
    rules.max_bound[sid][tid] = troot.size() * sroot.size() * kernel.compute(dist.min());
    rules.spread[sid][tid] = rules.max_bound[sid][tid] - rules.min_bound[sid][tid];
    rules.global_bound = rules.min_bound[sid][tid];
    rules.global_spread = rules.spread[sid][tid];

    if (multi == 0) {
      /* Create the traverser */
      DualTreeTraversal<Tree, Rule, Rule> traverser(src_tree, trg_tree, rules, rules, num_prunes, level);
    
      /* Start traversal from the root of the tree */
      traverser.traverse (src_tree.root(), trg_tree.root());
    } 
    else {
      vector<Tree*> trees;
      trees.push_back(&src_tree);
      trees.push_back(&trg_tree);

      /* Create the traverser */
      MultiTreeTraversal<Tree, Rule, Rule> traverserM(trees, rules, rules, num_prunes, level);

      /* Start traversal from the root of the trees */
      traverserM.traverse ();
    }
 
    /* Update log density */
    for (size_t i = 0; i < trg_tree.points(); ++i)
      density[i] *= rules.kernel_norm;
  }
  clock_t te_kde = clock();

  real_t time_kde = (te_kde - ts_kde) / (real_t) CLOCKS_PER_SEC;
  if (single_mode)
    cerr << "Single-tree kernel density estimation time: " << time_kde << " seconds\n";
  else if (!multi)
    cerr << "Dual-tree kde time: " << time_kde << " seconds\n";
  else
     cerr << "Multi-tree kde time: " << time_kde << " seconds\n";
}

#endif
