#ifndef _KDE_LOG_H_
#define _KDE_LOG_H_

#include "Binary_tree.h"
#include "kde_rules_log.h"
#include "Single_tree_traversal_Pascal.h"
#include "Dual_tree_traversal_Pascal.h"
#include "Metric.h"

template <typename Tree, typename KernelType>
class KernelDensity {
  public:
    int dim;

    /* Pointer to the source tree */
    Tree& src_tree;
    /* Pointer to the target tree */
    Tree& trg_tree;

    MetricType* metric;

    KernelDensity(int d, Tree& st, Tree& tt) : dim(d), src_tree(st), trg_tree(tt), metric(new SquaredEuclideanMetricType()) { }

    KernelDensity(int d, Tree& st, Tree& tt, MetricType* m) : dim(d), src_tree(st), trg_tree(tt), metric(m) { }

    /* Compute the log of kernel density */
    void compute_kernel_density (int num_pts, KernelType& kernel, Vec& log_density, real_t atol, real_t rtol) const;
};

// Template instantiation

/**
 * Compute the log of kernel density
 *
 * \param h         Width of the kernel
 * \param density   Array where the log of the kernel density is stored
 * \param atol      Absolute tolerance value
 * \param rtol      Relative tolerance value
 */

template <typename Tree, typename KernelType>
void
KernelDensity<Tree,KernelType>::compute_kernel_density (int num_pts, KernelType& kernel, Vec& log_density, real_t atol, real_t rtol) const {
  /* Check if source-tree or dual-tree search should be used */
  int single_mode = getenv__single_mode();
  int num_prunes = 0;

  clock_t ts_kde = clock();
  if (single_mode) {
    /* Create a helper object for tree traversal and intersection calculation */
    typedef KDERules<Tree, Vec, KernelType> Rule;
    real_t log_atol = log(atol * num_pts);
    real_t log_rtol = log(rtol);
    Rule rules(dim, log_atol, log_rtol, kernel, src_tree, trg_tree, src_tree.data_perm, trg_tree.data, log_density, metric);
    rules.min_bound = Vec(src_tree.nodes());
    rules.max_bound = Vec(src_tree.nodes());
    rules.spread = Vec(src_tree.nodes());

    /* Create the traverser */
    SingleTreeTraversal<Tree, Rule> traverser(src_tree, rules, num_prunes);

    for (size_t i = 0; i < trg_tree.points(); ++i) {
      /* Compute bounds on density for the root node */
      Box root = src_tree.root();
      size_t rid = root.index();
      Range dist = root.range_distance(trg_tree.data[i]);
      rules.min_bound[rid] = log(root.size()) + kernel.log_compute(dist.max());
      rules.max_bound[rid] = log(root.size()) + kernel.log_compute(dist.min());
      rules.spread[rid] = rules.logdiffexp(rules.max_bound[rid], rules.min_bound[rid]);
      rules.global_bound = rules.min_bound[rid];
      rules.global_spread = rules.spread[rid];

      /* Start traversal from the root of the tree */
      traverser.traverse (root, i);
      log_density[i] = rules.logsumexp(rules.global_bound, rules.global_spread-log(2))+rules.kernel_norm;
    }
  }
  else {
    /* Create a helper object for tree traversal and intersection calculation */
    typedef KDERules<Tree, Mat, KernelType> Rule;
    real_t log_atol = log(atol * trg_tree.points() * src_tree.points());
    real_t log_rtol = log(rtol);
    for (size_t i = 0; i < trg_tree.points(); ++i)
      log_density[i] = -INFINITY;
    Rule rules(dim, log_atol, log_rtol, kernel, src_tree, trg_tree, src_tree.data_perm, trg_tree.data_perm, log_density, metric);
    rules.min_bound = Mat(src_tree.nodes(), trg_tree.nodes());
    rules.max_bound = Mat(src_tree.nodes(), trg_tree.nodes());
    rules.spread = Mat(src_tree.nodes(), trg_tree.nodes());

    /* Create the traverser */
    DualTreeTraversal<Tree, Rule> traverser(src_tree, trg_tree, rules, num_prunes);

    /* Compute bounds on density for the root node */
    Box sroot = src_tree.root();
    Box troot = trg_tree.root();
    size_t sid = sroot.index();
    size_t tid = troot.index();
    Range dist = troot.range_distance(sroot);
    rules.min_bound[sid][tid] = log(troot.size()) + log(sroot.size()) + kernel.log_compute(dist.max());
    rules.max_bound[sid][tid] = log(troot.size()) + log(sroot.size()) + kernel.log_compute(dist.min());
    rules.spread[sid][tid] = rules.logdiffexp(rules.max_bound[sid][tid], rules.min_bound[sid][tid]);
    rules.global_bound = rules.min_bound[sid][tid];
    rules.global_spread = rules.spread[sid][tid];

    /* Start traversal from the root of the tree */
    traverser.traverse (src_tree.root(), trg_tree.root());

    /* Update log density */
    for (size_t i = 0; i < trg_tree.points(); ++i)
      log_density[i] += rules.kernel_norm;
  }
  clock_t te_kde = clock();

  real_t time_kde = (te_kde - ts_kde) / (real_t) CLOCKS_PER_SEC;
  if (single_mode)
    cerr << "Single-tree kernel density estimation time: " << time_kde << " seconds\n";
  else
    cerr << "Dual-tree kernel density estimation time: " << time_kde << " seconds\n";
}

#endif
