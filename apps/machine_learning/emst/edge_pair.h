#ifndef _EDGE_PAIR_H_
#define _EDGE_PAIR_H_

#include "union_find.h"

/**
 * An edge pair is two indices and a distance.  It is used as the
 * basic element of an edge list when computing a minimum spanning tree.
 */
class EdgePair
{
 private:
  /* Edge indices */
  std::pair<size_t, size_t> index;
  /* Distance between two indices */
  real_t sq_distance;

 public:
  /**
   * Initialize an EdgePair with two indices and a distance.
   */
  EdgePair(const size_t e1, const size_t e2, const real_t dist) : sq_distance(dist)
  {
    assert(e1 != e2 && "Edge pair indices cannot be equal.");
    index = std::make_pair(e1, e2);
  }

  /* Get the smaller index */
  size_t first() const { return index.first; }
  size_t& first() { return index.first; }

  /* Get the larger index */
  size_t second() const { return index.second; }
  size_t& second() { return index.second; }

  /* Get the distance */
  double distance() const { return sq_distance; }
  double& distance() { return sq_distance; }

}; 

struct CompEdges {
  bool operator()(const EdgePair& a, const EdgePair& b) const {
    return a.distance() < b.distance();
  }
} CompEdges;
#endif 
