/*
 *  computing the range search results
 *  compile comand: g++ -std=c++11 RS.cpp -o  RS
 *  Run: /RS.out 200 100 4 1 3
 */

#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <array>
#include <vector>
using namespace std;


void  RSCompute(int num_pts1,int num_pts2, int dim, int min, int max, double** sources, double** targets, std::vector<int>& neighbors) {
 

  /* Evaluate test/target point one by one */
  for (int i = 0; i < num_pts1; i++) {
    /* Calculate brute-force nearest neighbor for validation */
    for (int j = 0; j < num_pts2; j++) {
      int dist = 0;
      for (int d = 0; d < dim; d++)
        dist += (sources[j][d] - targets[i][d]) * (sources[j][d] - targets[i][d]);
      if ((dist > min) and (dist < max)) {
        neighbors.push_back(i);
        neighbors.push_back(j);
        neighbors.push_back(dist);
      }
    }
  }
}


int main(int argc , char**  argv) {

  double** d1;
  double** d2;
  int num_pts1, num_pts2, Dim;
  double min, max;
  if (argc != 6) {
    fprintf (stderr, "<num_of_pts1> <num_of_pts2> <Dim> <min> <max> ");
  }

  num_pts1 = atoi (argv[1]);
  num_pts2 = atoi (argv[2]);
  Dim      = atoi (argv[3]);
  min      = atoi (argv[4]);
  max      = atoi (argv[5]);

  d1 = new double*[num_pts1];
  d2 = new double*[num_pts2];
  for (int i = 0; i < num_pts1; i++) {
    d1[i] = new double[Dim];
    for (int d = 0; d < Dim; d++)
       d1[i][d] = rand();
  }

  for (int i = 0; i < num_pts2; i++) {
    d2[i] = new double[Dim];
    for (int d = 0; d < Dim; d++)
       d2[i][d] = rand();
 }
  std::vector<int > neighbors;
 
  RSCompute(num_pts1, num_pts2, Dim, min, max, d1, d2, neighbors);
  return 0;
}
