/* compile with this group of flags
   c++  Naive-llvm.cpp -lpthread `llvm-config --cxxflags  --libs core  --ldflags --system-libs` -w -o naive-llvm
 * then run it for prinintg hte LLVM IRs
   ./naive-llvm 200 100 2
 */

#include<iostream>
#include "llvm/ADT/APFloat.h"
#include "llvm/ADT/STLExtras.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/DerivedTypes.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Type.h"
#include "llvm/IR/Verifier.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Function.h"
#include "llvm/PassManager.h"
#include "llvm/IR/CallingConv.h"
#include "llvm/Analysis/Verifier.h"
#include "llvm/Assembly/PrintModulePass.h"
#include <llvm/Pass.h>
#include <llvm/PassManager.h>
#include <llvm/ADT/SmallVector.h>
#include <llvm/Analysis/Verifier.h>
#include <llvm/Assembly/PrintModulePass.h>

#include "llvm/Support/RandomNumberGenerator.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Support/Debug.h"
#include "llvm/Support/raw_ostream.h"

using namespace llvm;
using namespace std;


llvm::LLVMContext* context = new LLVMContext();
llvm::Module* module = new llvm::Module("NearestNeighbor", *context);
PointerType* PointerTy = PointerType::get(Type::getDoubleTy(*context), 0);
Constant* c = module->getOrInsertFunction("naiveCompute",
                                            Type::getDoubleTy(*context),
                                            Type::getInt32Ty(*context),
                                            Type::getInt32Ty(*context),
                                            PointerType::get(PointerTy, 0),
                                            PointerType::get(PointerTy, 0),
                                            Type::getInt32Ty(*context),
                                            NULL);


Function* naiveCompute = cast<Function>(c);

/* Generates LLVM IRs for the NaiveCompute function in the Naive.cpp file */
int generate_naive_compute(Value* num_pts1_in, Value* num_pts2_in, Value* dataset1_in, Value* dataset2_in, Value* Dim_in) {

  Function::arg_iterator args = naiveCompute->arg_begin();
 
  Value* num_pts1 = &*args++;
  num_pts1->setName("num_pts1");
  Value* num_pts2 = &*args++;
  num_pts2->setName("num_pts2");
  Value* dataset1 = &*args++;
  dataset1->setName("dataset1");
  Value* dataset2 = &*args++;
  dataset2->setName("dataset2");
  Value* Dim = &*args++;
  Dim->setName("Dim");

  BasicBlock* block = BasicBlock::Create(*context, "entry", naiveCompute); 
  IRBuilder<> builder(block);
 
  num_pts1 = &*num_pts1_in;
  num_pts2 = &*num_pts2_in;
  dataset1 = &*dataset1_in;
  dataset2 = &*dataset2_in;
  Dim_in   = &*Dim_in;

  /* Setup for computing the default value of min*/

  Value* minT = ( ConstantFP::get(*context, APFloat(0.0)));
  
  BasicBlock *PreheaderBB = builder.GetInsertBlock();
  Value* StartVal = ConstantInt::get(Type::getInt32Ty(*context), 0);  
  BasicBlock *ForCond = BasicBlock::Create(*context, "for.cond", naiveCompute);
  BasicBlock *ForBody = BasicBlock::Create(*context, "for.body", naiveCompute);
  BasicBlock *ForInc = BasicBlock::Create(*context, "for.inc", naiveCompute);  
  BasicBlock *ForEnd = BasicBlock::Create(*context, "for.end", naiveCompute);

  /*---For Condition---*/

  /* Insert an explicit fall through from the current block to the ForCond*/
  builder.CreateBr(ForCond);

  /* Start insertion in ForCond */
  builder.SetInsertPoint(ForCond);

  /* Generating the PHI node with an entry for Start. */
  PHINode *Variable = builder.CreatePHI(Type::getInt32Ty(*context), 2, "d");
  Variable->addIncoming(StartVal, PreheaderBB);
  
  Value* EndCond = builder.CreateICmpULT(Variable, Dim, "EndCond" );
  BranchInst::Create(ForBody, ForEnd, EndCond, ForCond);

  /*--- For Body ---*/

  builder.SetInsertPoint(ForBody);
  
  Value* temp = ConstantInt::get(Type::getInt32Ty(*context), 0);
  std::vector<Value*> theList;
  theList.push_back(temp);
  ArrayRef<Value*> IdxList(theList);
  
  Value* a = builder.CreateGEP(dataset1, IdxList);
  Value* b = builder.CreateGEP(dataset2, IdxList);
  Value* d = builder.CreateLoad(a);
  Value* e = builder.CreateLoad(b);
 
  std::vector<Value*> DimList;
  DimList.push_back(Variable);
  ArrayRef<Value*> IdxList2(DimList);

  Value* d2 = builder.CreateGEP(d, IdxList2);
  Value* e2 = builder.CreateGEP(e,IdxList2);
  Value* d3 = builder.CreateLoad(d2);
  Value* e3 = builder.CreateLoad(e2);
  
  Value* d1_d2     = builder.CreateFSub( d3 , e3, "d1-d2");
  Value* d1_d2_pow = builder.CreateFMul(d1_d2, d1_d2, "d1-d2-pow");

  PHINode *min = builder.CreatePHI(Type::getDoubleTy(*context), 2, "min");
  min->addIncoming(minT, PreheaderBB);
  Value* mintemp = builder.CreateFAdd(min, d1_d2_pow, "mintemp");
  min->addIncoming(mintemp, ForBody);

  builder.CreateBr(ForInc);

  /*---For Increment ---*/
  
  builder.SetInsertPoint(ForInc);
  Value* StepVal =  ConstantInt::get(Type::getInt32Ty(*context),1 );
  Value* NextVar = builder.CreateAdd(Variable, StepVal, "nextvar");

  Variable->addIncoming(NextVar, ForInc);
  builder.CreateBr(ForCond);

  /*--- For End ---*/

  builder.SetInsertPoint(ForEnd);

/* Generating the 3 nested loop for measuremnet of min distance */

  Value* DistT = ConstantFP::get(*context, APFloat(0.0));
  BasicBlock *PreheaderBB2 = builder.GetInsertBlock();

  BasicBlock *ForCond1 = BasicBlock::Create(*context, "for.cond1", naiveCompute);
  BasicBlock *ForCond2 = BasicBlock::Create(*context, "for.cond2", naiveCompute);
  BasicBlock *ForCond3 = BasicBlock::Create(*context, "for.cond3", naiveCompute);
  BasicBlock *ForBody1 = BasicBlock::Create(*context, "for.body1", naiveCompute);
  BasicBlock *ForInc3  = BasicBlock::Create(*context, "for.inc3", naiveCompute);
  BasicBlock *ForInc2  = BasicBlock::Create(*context, "for.inc2", naiveCompute);
  BasicBlock *ForInc1  = BasicBlock::Create(*context, "for.inc1", naiveCompute);
  BasicBlock *ForEnd3  = BasicBlock::Create(*context, "for.end3", naiveCompute);
  BasicBlock *ForEnd2  = BasicBlock::Create(*context, "for.end2", naiveCompute);
  BasicBlock *ForEnd1  = BasicBlock::Create(*context, "for.end1", naiveCompute);

  /*--- For#1 Condition (outer most loop) ---*/

  builder.CreateBr(ForCond1);
  builder.SetInsertPoint(ForCond1);

  PHINode *Variable1 = builder.CreatePHI(Type::getInt32Ty(*context), 2, "d");
  Variable1->addIncoming(StartVal, PreheaderBB2);

  Value* EndCond1 = builder.CreateICmpULT(Variable1, num_pts1, "EndCond1" );
  BranchInst::Create(ForCond2, ForEnd1, EndCond1, ForCond1);

  /*--- For#2 Condition ---*/

  builder.SetInsertPoint(ForCond2);

  PHINode *Variable2 = builder.CreatePHI(Type::getInt32Ty(*context), 2, "d");
  Variable2->addIncoming(StartVal, PreheaderBB2);

  Value* EndCond2 = builder.CreateICmpULT(Variable2, num_pts2, "EndCond2" );
  BranchInst::Create(ForCond3, ForEnd2, EndCond2, ForCond2);

  /*--- For#3 Condition (inner most loop)---*/

  builder.SetInsertPoint(ForCond3);

  PHINode *Variable3 = builder.CreatePHI(Type::getInt32Ty(*context), 2, "d");
  Variable3->addIncoming(StartVal, PreheaderBB2);

  Value* EndCond3 = builder.CreateICmpULT(Variable3, Dim, "EndCond3" );
  BranchInst::Create(ForBody1, ForEnd3, EndCond3, ForCond3);

  /*--- For Body ---*/
  
  builder.SetInsertPoint(ForBody1);

  std::vector<Value*> D1List;
  D1List.push_back(Variable1);
  ArrayRef<Value*> D1IdxList(D1List);
  
  std::vector<Value*> D2List;
  D2List.push_back(Variable2);
  ArrayRef<Value*> D2IdxList(D2List);

  a = builder.CreateGEP(dataset1, D1IdxList);
  b = builder.CreateGEP(dataset2, D2IdxList);
  d = builder.CreateLoad(a);
  e = builder.CreateLoad(b);

  std::vector<Value*> DimList2;
  DimList2.push_back(Variable3);
  ArrayRef<Value*> IdxList2_2(DimList2);


  d2 = builder.CreateGEP(d, IdxList2_2);
  e2 = builder.CreateGEP(e, IdxList2_2);
  d3 = builder.CreateLoad(d2);
  e3 = builder.CreateLoad(e2);

  d1_d2 = builder.CreateFSub( d3 , e3, "d1-d2");
  d1_d2_pow = builder.CreateFMul(d1_d2, d1_d2, "d1-d2-pow");

  PHINode *Dist = builder.CreatePHI(Type::getDoubleTy(*context), 2, "Dist");
  Dist->addIncoming(DistT, PreheaderBB2);
  Value* distTemp = builder.CreateFAdd(Dist, d1_d2_pow, "distTemp");
  Dist->addIncoming(distTemp, ForBody1);

  builder.CreateBr(ForInc3);

  /*--- For#3 Increament ---*/
  builder.SetInsertPoint(ForInc3);

  Value* NextVar3 = builder.CreateAdd(Variable3, StepVal, "nextvarFor3");
  Variable3->addIncoming(NextVar3, ForInc3);
  builder.CreateBr(ForCond3);

  /*--- For#3 Ending ---*/
  builder.SetInsertPoint(ForEnd3);
  builder.CreateBr(ForInc2);

  /*--- For#2 Increament ---*/
  builder.SetInsertPoint(ForInc2);

  Value* NextVar2 = builder.CreateAdd(Variable2, StepVal, "nextvarFor2");
  Variable2->addIncoming(NextVar2, ForInc2);
  builder.CreateBr(ForCond2);

  /*--- For2 End ---*/
  builder.SetInsertPoint(ForEnd2);
  builder.CreateBr(ForInc1);

  /*--- For#1 Increament ---*/
  builder.SetInsertPoint(ForInc1);

  Value* NextVar1 = builder.CreateAdd(Variable1, StepVal, "nextvarFor1");
  Variable1->addIncoming(NextVar1, ForInc1);
  builder.CreateBr(ForCond1);

  /*--- For1 End ---*/
  builder.SetInsertPoint(ForEnd1); 

    
  return 0;

}

/* Generates the LLVM IR for the Main function in the Naive.cpp example*/
int  generate_main( int main_num_pts1,int  main_num_pts2, int main_Dim) {

  Constant* c2 = module->getOrInsertFunction("Main",
                                            Type::getDoubleTy(*context),
                                            Type::getInt32Ty(*context),
                                            Type::getInt32Ty(*context),
                                            Type::getInt32Ty(*context),
                                            NULL);


  Function* mainF = cast<Function>(c2);
  Function::arg_iterator argsMain = mainF->arg_begin();
  Value* num_pts1 = &*argsMain++;
  num_pts1->setName("num_pts1");
  Value* num_pts2 = &*argsMain++;
  num_pts2->setName("num_pts2");
  Value* Dim = &*argsMain++;
  Dim->setName("Dim");


  num_pts1 = ConstantInt::get(Type::getInt32Ty(*context), main_num_pts1 );
  num_pts2 = ConstantInt::get(Type::getInt32Ty(*context), main_num_pts2 );
  Dim = ConstantInt::get(Type::getInt32Ty(*context), main_Dim );

  BasicBlock* block = BasicBlock::Create(*context, "entry", mainF);
  IRBuilder<> builder(block);
 
  Value* data11 = builder.CreateAlloca(PointerType::get(PointerTy,0), num_pts1);
  Value* data1 = builder.CreateAlloca(PointerType::get(Type::getDoubleTy(*context),0), num_pts1);
  
  Type* a12 = data11->getType();
  BasicBlock *PreheaderBB = builder.GetInsertBlock();
  Value* StartVal = ConstantInt::get(Type::getInt32Ty(*context), 0);

  BasicBlock *ForCond1 = BasicBlock::Create(*context, "for.cond1", mainF);
  BasicBlock *ForCond2 = BasicBlock::Create(*context, "for.cond2", mainF);
  BasicBlock *ForBody1 = BasicBlock::Create(*context, "for.body1", mainF);
  BasicBlock *ForInc2  = BasicBlock::Create(*context, "for.inc2",  mainF);
  BasicBlock *ForInc1  = BasicBlock::Create(*context, "for.inc1",  mainF);
  BasicBlock *ForEnd2  = BasicBlock::Create(*context, "for.end2",  mainF);
  BasicBlock *ForEnd1  = BasicBlock::Create(*context, "for.end1",  mainF);

  /*--- For#1 Condition (outer loop)---*/
  builder.CreateBr(ForCond1);
  builder.SetInsertPoint(ForCond1);

  PHINode *Variable1 = builder.CreatePHI(Type::getInt32Ty(*context), 2, "d");
  Variable1->addIncoming(StartVal, PreheaderBB);

  Value* EndCond1 = builder.CreateICmpULT(Variable1, num_pts1, "EndCond1" );
  BranchInst::Create(ForCond2, ForEnd1, EndCond1, ForCond1);

  /*--- For2 Cndition (inner loop)---*/
  builder.SetInsertPoint(ForCond2);

  PHINode *Variable2 = builder.CreatePHI(Type::getInt32Ty(*context), 2, "d");
  Variable2->addIncoming(StartVal, PreheaderBB);

  Value* EndCond2 = builder.CreateICmpULT(Variable2, Dim, "EndCond2" );
  BranchInst::Create(ForBody1, ForEnd2, EndCond2, ForCond2);

  /*--- For Body ---*/
  builder.SetInsertPoint(ForBody1);

  std::vector<Value*> sizeList;
  sizeList.push_back(Variable1);
  ArrayRef<Value*> IdxList(sizeList);

  Value* a = builder.CreateGEP(data1, IdxList);
  Value* d = builder.CreateLoad(a);

  std::vector<Value*> DimList;
  DimList.push_back(Variable2);
  ArrayRef<Value*> IdxList2(DimList);

  Value* d2 = builder.CreateGEP(d, IdxList2);
  Value* d3 = builder.CreateLoad(d2);
  
  Value* temp1 = ConstantInt::get(Type::getInt32Ty(*context),1 );;
  Value* temp2 = builder.CreateAdd(Variable1, temp1,"temp2add");
  Value* temp3 = builder.CreateAdd(Variable2, temp1,"temp3add");
  Value* temp4 = builder.CreateMul(temp2, temp3,"final");
    
  CastInst* temp5 = new SIToFPInst(temp4, Type::getDoubleTy(*context), "d1", ForBody1);
  Value* ss = builder.CreateStore(temp5,d2);

  builder.CreateBr(ForInc2);

  /*--- For#2 Increament  ---*/
  builder.SetInsertPoint(ForInc2);

  Value* StepVal =  ConstantInt::get(Type::getInt32Ty(*context),1 );
  Value* NextVar2 = builder.CreateAdd(Variable2, StepVal, "nextvarFor2");

  Variable2->addIncoming(NextVar2, ForInc2);
  builder.CreateBr(ForCond2);

  /*--- For#2 End ---*/
   builder.SetInsertPoint(ForEnd2);
   builder.CreateBr(ForInc1);

  /*--- For#1 Inc ---*/
  builder.SetInsertPoint(ForInc1);

  Value* NextVar1 = builder.CreateAdd(Variable1, StepVal, "nextvarFor1");
  Variable1->addIncoming(NextVar1, ForInc1);
  builder.CreateBr(ForCond1);

  /*---- For#1 End ---*/
  builder.SetInsertPoint(ForEnd1); 

  /* Same pattern for second dataset */

  Value* data22 = builder.CreateAlloca(PointerType::get(PointerTy,0), num_pts2);
  Value* data2 = builder.CreateAlloca(PointerType::get(Type::getDoubleTy(*context),0), num_pts2);
  
  BasicBlock *PreheaderBB2 = builder.GetInsertBlock();

  BasicBlock *ForCond12 = BasicBlock::Create(*context, "for.cond12", mainF);
  BasicBlock *ForCond22 = BasicBlock::Create(*context, "for.cond22", mainF);
  BasicBlock *ForBody12 = BasicBlock::Create(*context, "for.body12", mainF);
  BasicBlock *ForInc22  = BasicBlock::Create(*context, "for.inc22", mainF);
  BasicBlock *ForInc12  = BasicBlock::Create(*context, "for.inc12", mainF);
  BasicBlock *ForEnd22  = BasicBlock::Create(*context, "for.end22", mainF);
  BasicBlock *ForEnd12  = BasicBlock::Create(*context, "for.end12", mainF);

  /*--- For#1 Condition ---*/
  builder.CreateBr(ForCond12);
  builder.SetInsertPoint(ForCond12);

  PHINode *Variable12 = builder.CreatePHI(Type::getInt32Ty(*context), 2, "d");
  Variable12->addIncoming(StartVal, PreheaderBB2);

  Value* EndCond12 = builder.CreateICmpULT(Variable2, num_pts2, "EndCond12" );
  BranchInst::Create(ForCond22, ForEnd12, EndCond12, ForCond12);

  /*--- For#2 Condition ---*/
  builder.SetInsertPoint(ForCond22);

  PHINode *Variable22 = builder.CreatePHI(Type::getInt32Ty(*context), 2, "d");
  Variable22->addIncoming(StartVal, PreheaderBB2);

  Value* EndCond22 = builder.CreateICmpULT(Variable22, Dim, "EndCond22" );
  BranchInst::Create(ForBody12, ForEnd22, EndCond22, ForCond22);
  
  /*--- For Body ---*/  
  builder.SetInsertPoint(ForBody12);

  std::vector<Value*> sizeList22;
  sizeList22.push_back(Variable12);
  ArrayRef<Value*> IdxList22(sizeList22);

  Value* b  = builder.CreateGEP(data2, IdxList2);
  Value* e  = builder.CreateLoad(b);

  std::vector<Value*> DimList2;
  DimList2.push_back(Variable22);
  ArrayRef<Value*> IdxList2_2(DimList2);

  Value* e2 = builder.CreateGEP(e,IdxList2_2);
  Value* e3 = builder.CreateLoad(e2);

  Value* temp12 = ConstantInt::get(Type::getInt32Ty(*context),1 );;
  Value* temp22 = builder.CreateAdd(Variable12, temp12,"temp22add");
  Value* temp32 = builder.CreateAdd(Variable22, temp12,"temp32add");
  Value* temp42 = builder.CreateMul(temp22, temp32,"final2");
  
  CastInst* temp52 = new SIToFPInst(temp42, Type::getDoubleTy(*context),  "d2", ForBody12);
  Value* ss2 = builder.CreateStore(temp52,e2);

  builder.CreateBr(ForInc2);

  /*--- For#2 Increament ---*/ 
  builder.SetInsertPoint(ForInc22);

  Value* NextVar22 = builder.CreateAdd(Variable22, StepVal, "nextvarFor22");

  Variable22->addIncoming(NextVar22, ForInc22);
  builder.CreateBr(ForCond22);

  /*--- For2 End ---*/
  builder.SetInsertPoint(ForEnd22);
  builder.CreateBr(ForInc12);

  /*--- For#1 Increament ---*/
  builder.SetInsertPoint(ForInc12);

  Value* NextVar12 = builder.CreateAdd(Variable12, StepVal, "nextvarFor12");
  Variable12->addIncoming(NextVar12, ForInc12);
  builder.CreateBr(ForCond12);

  /*--- For1 End ---*/
  builder.SetInsertPoint(ForEnd12);  


  std::vector<Value*> args3;
  args3.push_back(num_pts1);
  args3.push_back(num_pts2);
  args3.push_back(data1);
  args3.push_back(data2);
  args3.push_back(Dim);
  
  Value* ret_naiveCompute = builder.CreateCall( naiveCompute, args3 , "naive_compute");
  builder.CreateRet(ret_naiveCompute);

  generate_naive_compute(num_pts1, num_pts2, data1, data2, Dim);
  return 0;
}



int  main(int argc, char** argv) {

  int num_pts1, num_pts2, Dim;
  if (argc == 4) {
    num_pts1 = atoi (argv[1]);
    num_pts2 = atoi (argv[2]);
    Dim = atoi (argv[3]);
  } 
  else {
    num_pts1 = 100;
    num_pts2 = 200;
    Dim = 2;  
  }
  generate_main( num_pts1, num_pts2, Dim);
  module->dump();   
  return 0;
}

