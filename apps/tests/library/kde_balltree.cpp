#include <iostream>
#include <cstdio>
#include <ctime>
#include "utils.h"
#include "Binary_tree.h"
#include "Hsphere_bounds.h"
#include "kde.h"
#include "Gaussian_kernel.h"
//#include "Epanechnikov_kernel.h"

using namespace std;
typedef GaussianKernel KernelType;
//typedef EpanechnikovKernel KernelType;

static inline
  void
usage__ (const char* use)
{
  fprintf (stderr, "usage: %s <N> <dim> <filename> <bandwidth> <level>\n", use);
}

bool validate (int num_pts, int dim, KernelType& kernel, real_t atol, real_t rtol, Points_t& sources, Points_t& targets, Vec& result) {
  bool error = false;
  real_t norm = kernel.norm(dim);

  /* Allocate memory for exhaustive calculation of kernel density */
  Vec density(num_pts);

  /* Evaluate test/target point one by one */
  for (int i = 0; i < num_pts; i++) {
    /* Calculate brute-force nearest neighbor for validation */
    real_t kval = 0.0;
    for (int j = 0; j < num_pts; j++) {
      kval += kernel.compute(targets[i], sources[j]);
    }
    density[i] = kval * norm;

	  /* Set tolerance value for validation */
    real_t tolerance = (atol == 0.0 && rtol == 0.0) ? 1e-2 : atol + rtol * density[i];
    if (fabs(density[i] - result[i]) >= tolerance) {
      cerr << "Target point: " << i << endl;
      cerr << "Correct density: " << density[i] << endl;
      cerr << "Computed density: " << result[i] << endl;
      error = true;
      exit(0);
    }
  }
  return error;
}

int main (int argc, char** argv)
{
  char* filename;
  int num_pts;
  int dim;
  real_t bandwidth;
  real_t atol = 0.0;
  real_t rtol = 0.1;
  bool error = false;
  int level = 10;

  if ((argc != 5) && (argc != 6)) {
    usage__ (argv[0]);
    return -1;
  }

  /** Command line input */
  num_pts = atoi (argv[1]);
  dim = atoi (argv[2]);
  filename = argv[3];
  bandwidth = atof (argv[4]);
  if (argc == 6)
    level = atoi(argv[5]);

  /* Allocate memory for original source and target dataset */
  Points_t sources(num_pts, dim);
  Points_t targets(num_pts, dim);

  from_file(sources, targets, filename, filename);

  /* Allocate memory for the permuted source and target points */
  Points_t s_perm (num_pts, dim);
  Points_t t_perm (num_pts, dim);

  /* Create and build source and target tree */
  typedef Tree<Hsphere> TreeType;
  TreeType stree(sources, s_perm);
  TreeType ttree(targets, t_perm);

  fprintf (stderr, "Building source tree...\n");
  clock_t ts_stree = clock();
  stree.build_balltree();
  clock_t te_stree = clock();
  fprintf (stderr, "Source tree built.\n");

  /* Check if source-tree or dual-tree search should be used */
  int single_mode = getenv__single_mode();
  clock_t ts_ttree = 0;
  clock_t te_ttree = 0;
  if (!single_mode) {
    fprintf (stderr, "Building target tree...\n");
    ts_ttree = clock();
    ttree.build_balltree();
    te_ttree = clock();
    fprintf (stderr, "Target tree built.\n");
  }

#ifdef _DEBUG
  std::cout << "Sources: " << std::endl;
  std::cout << sources << std::endl;

  cout << "In the main program...\n";
  std::cout << s_perm << std::endl;

  cout << "\n Tree structure...\n";
  std::cout << stree << std::endl;
#endif

  /* Construst a Gaussian kernel object */
  KernelType kernel(bandwidth);

  /* Compute the kernel density estimation of the kernel */
  KernelDensity<TreeType, KernelType> kde(dim, level, stree, ttree);

  Vec result(num_pts);
	kde.compute_kernel_density(num_pts, kernel, result, atol, rtol);

#ifdef _DEBUG
  /* Print the neighbors indices and distances */
  for (int i = 0; i < num_pts; i++) {
    cout << result[i] << endl;
  }
#endif

  /* Validate the results */
  // error = validate(num_pts, dim, kernel, atol, rtol, sources, targets, result);

  /* Calculate time */
  real_t time_tree = ((te_stree - ts_stree) + (te_ttree - ts_ttree)) / (real_t) CLOCKS_PER_SEC;

  /* Print timing results */
  cerr << "Tree construction time: " << time_tree << " seconds\n";
  if (error)
    cerr << "Program failed, error!\n";
  else
    cerr << "Passed.\n";

  return 0;
}
