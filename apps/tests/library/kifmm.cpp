#include <iostream>
#include <cstdio>

#include "Octree.h"
#include "Laplace_kernel.h"
//#include "Stokes_kernel.h"
//#include "Stokes_f_kernel.h"
#include "Fmm.h"

using namespace std;
typedef LaplaceKernel<points_iter, vec_iter> KernelType;
typedef LaplaceKernel<points_iter, vec_iter> KernelMatrixType;
//typedef StokesKernel<points_iter, vec_iter> KernelType;
//typedef StokesFKernel<points_iter, vec_iter> KernelMatrixType;

static inline
  void
usage__ (const char* use)
{
  fprintf (stderr, "usage: %s <N> <distribution>\n", use);
}

int main (int argc, char** argv)
{
  char* distribution;
  unsigned int num_pts;
  real_t rerr;

  if (argc != 3) {
    usage__ (argv[0]);
    return -1;
  }

  /** Command line input */
  num_pts = atoi (argv[1]);
  distribution = argv[2];

  /* Initialize seed */
  util::default_generator.seed(1337);

  /* Allocate memory for original source and target points */
  Points sources(num_pts);
  Points targets(num_pts);

  fprintf(stderr, "Generating source dataset...\n");
  initialize_dataset(sources, distribution);

  /* Check if the source and target set of points are identical */
  int coincide = getenv__coincide();
  if (coincide) {
    fprintf (stderr, "Using same dataset for sources and targets.\n");
    targets = sources;
  }
  else {
    fprintf (stderr, "Generating target dataset...\n");
    initialize_dataset(targets, distribution);
  }

  /* Create source and target tree */
  Tree stree(sources);
  Tree ttree(targets);
  stree.build_tree();
  ttree.build_tree();

  /** Compute U-, V-, W- and X-lists */
  vector<Tree::NodeTree>& ntree = ttree.node_data;
  for (size_t i = 0; i < ntree.size(); ++i) {
    ttree.compute_lists(i);
  }

  /* Kernel intialization */
  KernelType knl;
  size_t sdof = knl.src_dof();
  size_t tdof = knl.trg_dof();
  Vec charge(sdof * num_pts);
  Vec result(tdof * num_pts);
  for (size_t i = 0; i < num_pts * sdof; ++i)
    charge[i] = drand48();

  /* Setup the translation operators */
  KernelMatrixType knl_mm;
  Trans<KernelMatrixType> tr(&knl_mm);
  tr.setup();

  /* Permute the souce and target points */
  FMM<KernelType, KernelMatrixType> f(stree, ttree, tr, knl, charge, result);
  f.setup();
  f.run();

  /* Verify the results */
  f.validate (num_pts, rerr);

  cout << "Relative error: " << rerr << endl;
  cout << "Passed." << endl;

  return 0;
}
