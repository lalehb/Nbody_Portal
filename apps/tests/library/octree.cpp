#include <iostream>
#include <cstdio>

#include "utils.h"
#include "Octree.h"
#include "Clock.hpp"

//#define __DEBUG 1

using namespace std;

static inline
void
usage__ (const char* use)
{
  fprintf (stderr, "usage: %s <N> <distribution>\n", use);
}

int main (int argc, char** argv)
{
  char* distribution;
  unsigned int num_pts;
  Clock timer;

  if (argc != 3) {
    usage__ (argv[0]);
    return -1;
  }

  /** Command line input */
  num_pts = atoi (argv[1]);
  distribution = argv[2];

  /* Initialize seed */
  util::default_generator.seed(1337);

  /* Allocate memory for original dataset */
  Points s(num_pts);

  initialize_dataset(s, distribution);

  /* Create and build tree */
  Tree tt(s);
  timer.start();
  tt.build_tree();
  double time_tree = timer.seconds();

  /* Print time */
  cerr << "Octree construction time: " << time_tree << " seconds\n";
  cout << "Passed." << endl;
  return 0;
}
