#include <iostream>
#include <cstdio>

#include "utils.h"
#include "Binary_tree.h"
#include "Hrect_bounds.h"
#include "HD.h"
#include "Clock.hpp"

using namespace std;

static inline
void
usage__ (const char* use)
{
  fprintf (stderr, "usage: %s <N> <dim> <Target filename> <Source filename> <level> <partition: 1 = midevalue, 0 = midpoint> \n", use);
}

bool validate(int num_pts, int dim, int k, Tree<Hrect>& stree, Points_t& targets, real_t MaxMin) {
  bool error = false;

  /* Allocate memory for exhaustive calculation of near neighbors */
  real_t maxmin = -1;
  /* Evaluate test/target point one by one */
  for (int i = 0; i < num_pts; i++) {
    real_t  min = DBL_MAX;
    /* Calculate brute-force  for validation */
    for (int j = 0; j < num_pts; j++) {
      real_t dist = 0.0;
      for (int d = 0; d < dim; d++)
        dist += (stree.data[j][d]-targets[i][d]) * (stree.data[j][d]-targets[i][d]);
      dist = sqrt(dist);
     // if ((dist > 0) && ( dist < min))
     if ( ( dist < min))
         min = dist;
    }
    if (min > maxmin)
       maxmin = min;
 }
 return (maxmin != MaxMin);
}


int main (int argc, char** argv)
{
  char* filenameS; // source filename
  char* filenameT; // target filename
  int num_pts;
  int dim;
  int k = 1;
  real_t epsilon = 0.0;  /* Set to non-zero value for approximate k-nn */
  bool error;
  Clock timer;
  int level = 10;
  int partition = 0;

  if ((argc != 6) && (argc != 5)) {
    usage__ (argv[0]);
    return -1;
  }

  /** Command line input */
  num_pts = atoi (argv[1]);
  dim = atoi (argv[2]);
  filenameT = argv[3];
  filenameS = argv[4];
  if (argc > 5)
    level = atoi(argv[5]);
  if (argc > 6)
    partition = atoi(argv[6]);



  /* Allocate memory for original source and target dataset */
  Points_t sources(num_pts, dim);
  Points_t targets(num_pts, dim);

  from_file(sources, filenameS);
  from_file(targets, filenameT);

  /* Allocate memory for the permuted source and target points */
  Points_t s_perm(num_pts, dim);
  Points_t t_perm(num_pts, dim);

  /* Create and build source and target tree */
  typedef Tree<Hrect> TreeType;
  TreeType stree(sources, s_perm);
  TreeType ttree(targets, t_perm);

  fprintf (stderr, "Building source tree...\n");
  timer.start();
  if (partition == 1)
    stree.build_kdtree_midvalue();
  else
    stree.build_kdtree();
  double time_tree = timer.seconds();
  stree.compute_maxdist(stree.root());
  fprintf (stderr, "Source tree built.\n");

  /* Check if source-tree or dual-tree search should be used */
  int single_mode = getenv__single_mode();
  if (!single_mode) {
    fprintf (stderr, "Building target tree...\n");
    timer.start();
    if (partition == 1)
      ttree.build_kdtree_midvalue();
    else
      ttree.build_kdtree();
    time_tree += timer.seconds();
    ttree.compute_maxdist(ttree.root());
    fprintf (stderr, "Target tree built.\n");
  }

#ifdef _DEBUG
  std::cout << "Sources: " << std::endl;
  std::cout << sources << std::endl;

  cout << "In the main program...\n";
  std::cout << s_perm << std::endl;

  cout << "\n Tree structure...\n";
  std::cout << stree << std::endl;
#endif

  /* class definition for Hausdarff distance calculation */
  HD<TreeType> hd(dim, level, stree, ttree);

  /* Calculate the Hausdarff distances using the pruning algorithm */
  resultMaxMin result (num_pts, vector<HD_distance>(k));
	hd.max_min(k, result, epsilon);


  /* Validate the results */
  error = validate(num_pts, dim, k, stree, targets, hd.MaxMin);

  /* Print timing results */
  cerr << "Tree construction time: " << time_tree << " seconds\n";
  if (error)
    cerr << "Program failed, error!\n";
  else
    cerr << "Passed.\n";

  return 0;
}
