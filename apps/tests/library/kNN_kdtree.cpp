#include <iostream>
#include <cstdio>

#include "utils.h"
#include "Binary_tree.h"
#include "Hrect_bounds.h"
#include "kNN.h"
#include "Clock.hpp"

using namespace std;

static inline
void
usage__ (const char* use)
{
  fprintf (stderr, "usage: %s <N_source> <N_targets> <dim> <filename_source> <filename_target> <#neighbors> <level> <partition: 1 = midevalue, 0 = midpoint>\n", use);
}

bool validate(int num_pts_srource, int num_pts_targets, int dim, int k, Tree<Hrect>& stree, Points_t& targets, resultNeighbors& result) {
  bool error = false;
	/* Set tolerance value for validation */
	const real_t tolerance = 1e-2;
  int counter = 0;

  /* Allocate memory for exhaustive calculation of near neighbors */
  kNN_distance* nearest = new kNN_distance[num_pts_targets];

  /* Evaluate test/target point one by one */
  for (int i = 0; i < num_pts_targets; i++) {
    /* Calculate brute-force nearest neighbor for validation */
    for (int j = 0; j < num_pts_srource; j++) {
      double dist = 0.0;
      int same = 0;
      for (int d = 0; d < dim; d++) {
        dist += (stree.data[j][d]-targets[i][d]) * (stree.data[j][d]-targets[i][d]);
      }
      nearest[j] = kNN_distance(j, dist);
    }

    /* Sort the source points by its distance to the target points */
    sort (nearest, nearest + num_pts_srource);

    kNeighbors kneighbors = result[i];
    for (int j = 0; j < k; j++) {

      if (fabs(kneighbors[j].squared_distance - nearest[j].squared_distance) >= tolerance) {

        cerr << "failed for " << i << " -> "<< j <<  " : Correct " << nearest[j].index <<  " , "
        << nearest[j].squared_distance <<  " -- Computed :  " << kneighbors[j].index <<  " , " << kneighbors[j].squared_distance << endl;
        counter++;
        error = true;
        // exit(0);
      }
    }
  }
  cout << "Total fails: " << counter << "\n";
  return error;
}


bool validate_column_major(int num_pts_srource, int num_pts_targets, int dim, int k, Tree<Hrect>& stree, Points_t& targets, resultNeighbors& result) {
  bool error = false;
	/* Set tolerance value for validation */
	const real_t tolerance = 1e-2;
  int counter = 0;

  /* Allocate memory for exhaustive calculation of near neighbors */
  kNN_distance* nearest = new kNN_distance[num_pts_targets];

  /* Evaluate test/target point one by one */
  for (int i = 0; i < num_pts_targets; i++) {
    /* Calculate brute-force nearest neighbor for validation */
    for (int j = 0; j < num_pts_srource; j++) {
      double dist = 0.0;
      int same = 0;
      for (int d = 0; d < dim; d++) {
        dist += (stree.data[d][j]-targets[d][i]) * (stree.data[d][j]-targets[d][i]);
      }
      nearest[j] = kNN_distance(j, dist);
    }

    /* Sort the source points by its distance to the target points */
    sort (nearest, nearest + num_pts_srource);

    kNeighbors kneighbors = result[i];
    for (int j = 0; j < k; j++) {

      if (fabs(kneighbors[j].squared_distance - nearest[j].squared_distance) >= tolerance) {

        cerr << "failed for " << i << " -> "<< j <<  " : Correct " << nearest[j].index <<  " , "
        << nearest[j].squared_distance <<  " -- Computed :  " << kneighbors[j].index <<  " , " << kneighbors[j].squared_distance << endl;
        counter++;
        error = true;
        // exit(0);
      }
    }
  }
  cout << "Total fails: " << counter << "\n";
  return error;
}

int main (int argc, char** argv)
{
  char* filename_source;
  char* filename_target;
  int num_pts_srource;
  int num_pts_targets;
  int dim;
  int k;
  real_t epsilon = 0.0;  /* Set to non-zero value for approximate k-nn */
  bool error;
  Clock timer;
  int level;
  int partition= 0;

  if (!((argc == 8) || (argc == 9)))  {
    usage__ (argv[0]);
   return -1;
  }


  /** Command line input */
  num_pts_srource = atoi (argv[1]);
  num_pts_targets = atoi (argv[2]);
  dim = atoi (argv[3]);
  filename_source = argv[4];
  filename_target = argv[5];
  k = atoi (argv[6]);
  level = atoi(argv[7]);
  partition = atoi(argv[8]);

  // util::default_generator.seed(1337);
  // Points_t sources2(num_pts_srource, dim);
  // initialize_dataset(sources2, "uniform");


  /* Allocate memory for original source and target dataset */
  Points_t sources(dim, num_pts_srource);
  Points_t targets(dim, num_pts_targets);

  from_file_column_major(sources, targets, filename_source, filename_target);

  /* Allocate memory for the permuted source and target points */
  Points_t s_perm(dim, num_pts_srource);
  Points_t t_perm(dim, num_pts_targets);

// for (int j = 0 ; j < dim; j++){
//   for (int i  = 0; i  < num_pts_srource; i++) {
//     cout << sources[j][i] << " , " ;
//   }
//   cout << "\n\n";
// }




  // /* Allocate memory for original source and target dataset */
  // Points_t sources(num_pts_srource, dim);
  // Points_t targets(num_pts_targets, dim);
  //
  // from_file(sources, targets, filename_source, filename_target);
  //
  // /* Allocate memory for the permuted source and target points */
  // Points_t s_perm(num_pts_srource, dim);
  // Points_t t_perm(num_pts_targets, dim);


  /* Create and build source and target tree */
  typedef Tree<Hrect> TreeType;
  TreeType stree(sources, s_perm);
  TreeType ttree(targets, t_perm);

  fprintf (stderr, "Building source tree...\n");
  timer.start();
  stree.build_kdtree_column_major();
  // if (partition == 1)
  //   stree.build_kdtree_midpoint_tight();
  // else
  //   stree.build_kdtree();
  double time_tree = timer.seconds();
  // stree.compute_maxdist(stree.root());
  fprintf (stderr, "Source tree built.\n");

  // Check if source-tree or dual-tree search should be used
  int single_mode = getenv__single_mode();
  if (!single_mode) {
    fprintf (stderr, "Building target tree...\n");
    timer.start();
    ttree.build_kdtree_column_major();
    // if (partition == 1)
    //   ttree.build_kdtree_midpoint_tight();
    // else
    //   ttree.build_kdtree();
    time_tree += timer.seconds();
    // ttree.compute_maxdist(ttree.root());
    fprintf (stderr, "Target tree built.\n");
  }

#ifdef _DEBUG
  std::cout << "Sources: " << std::endl;
  std::cout << sources << std::endl;

  cout << "In the main program...\n";
  std::cout << s_perm << std::endl;

  cout << "\n Tree structure...\n";
  std::cout << stree << std::endl;

  cout << "target tree nodes: " << ttree.node_data.size() << endl;
#endif


  // Get the k nearest neighbours
  kNN<TreeType> knn(dim, level, stree, ttree);

  // Calculate k-nearest neighbors using the pruning algorithm
  resultNeighbors result (num_pts_targets, vector<kNN_distance>(k));
	knn.nearest_neighbors(k, result, epsilon);


#ifdef _DEBUG
  // Print the neighbors indices and distances
  for (int i = 0; i < num_pts_targets; i++) {
    kNeighbors kneighbors = result[i];
    for (int j = 0; j < k; j++)
      cout << kneighbors[j].index << "  ";
    cout << endl;
  }
  for (int i = 0; i < num_pts_targets; i++) {
    kNeighbors kneighbors = result[i];
    for (int j = 0; j < k; j++)
      cout << kneighbors[j].distance << "  ";
    cout << endl;
  }
#endif

  /* Validate the results */
  // error = validate(num_pts_srource, num_pts_targets, dim, k, stree, targets, result);
  error = validate_column_major(num_pts_srource, num_pts_targets, dim, k, stree, targets, result);


  // Print timing results
  cerr << "Tree construction time: " << time_tree << " seconds\n";
  if (error)
    cerr << "Program failed, error!\n";
  else
    cerr << "Passed.\n";

  return 0;
}
