#include <iostream>

#include "utils.h"
#include "Binary_tree.h"
#include "EM.h"

using namespace std;

static inline
void
usage__ (const char* use)
{
  fprintf (stderr, "usage: %s <N> <dim> <k> <Iter> <filename> \n", use);
}

int main(int argc, char** argv)
{
  char* filename;
  int num_pts; // number of data points
  int dim; // dimension of data
  int k; // number of clusters
  real_t epsilon = 0.001;  // criteria to test convergence
  int Iter;

  if (argc != 5) {
    usage__ (argv[0]);
    return -1;
  }

  /** Command line input */
  num_pts = atoi (argv[1]);
  dim = atoi (argv[2]);
  k = atoi (argv[3]);
  Iter = atoi(argv[4]);
  filename = argv[5];

  /* Allocate memory for the input dataset */
  Points_t DataInput(num_pts, dim);

  /* Read data from file */
  from_file(DataInput, filename);

  /* EM calculation */
  EMBase emTest(num_pts, k, dim, Iter, epsilon, DataInput);

  emTest.IterationEM();
  emTest.ResultPrinter();

  return 0;
}
