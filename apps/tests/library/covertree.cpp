#include <iostream>
#include <cstdio>

#include "utils.h"
#include "Cover_tree.h"
#include "Cover_bounds.h"
#include "Clock.hpp"
using namespace std;

static inline
void
usage__ (const char* use)
{
  fprintf (stderr, "usage: %s <N> <dim> <distribution>\n", use);
}

int main (int argc, char** argv)
{
  char* distribution;
  unsigned int num_pts;
  unsigned int dim;
  Clock timer;

  if (argc != 4) {
    usage__ (argv[0]);
    return -1;
  }

  /** Command line input */
  num_pts = atoi (argv[1]);
  dim = atoi (argv[2]);
  distribution = argv[3];

  /* Initialize seed */
  util::default_generator.seed(1337);

  /* Allocate memory for original dataset */
  Points_t s(num_pts, dim);
  initialize_dataset(s, distribution);

  /* Allocate memory for permuted dataset */
  Points_t s_perm(num_pts, dim);

  /* Create and build tree */
  CTree<Cover> tt(s, s);
  timer.start();
  tt.build_covertree ();
  double time_tree = timer.seconds();

#ifdef __DEBUG
  cout << "In the main program...\n";
  for (size_t i = 0; i < num_pts; i++) {
    for (size_t j = 0; j < dim; j++)
      cout << tt.data[i][j] << " ";
    cout << endl;
  }

  for (size_t i = 0; i < num_pts; i++) {
      cout << tt.index[i] << " ";
  }
  cout << endl;

  for (size_t i = 0; i < num_pts; i++) {
    for (size_t j = 0; j < dim; j++)
      cout << tt.data_perm[i][j] << " ";
    cout << endl;
  }

  vector<CTree<Cover>::NodeTree>& ntree = tt.node_data;
  for (size_t i = 0; i < ntree.size(); i++) {
    fprintf (stderr, "\nNode: %lu\n", i);
    for (size_t d = 0; d < dim; d++) {
      fprintf (stderr, "%lf  %lf \n", ntree[i].bounds.center[d], ntree[i].bounds.radius);
    }
  }
  cout << endl;
#endif

  /* Print time */
  cerr << "Cover-tree construction time: " << time_tree << " seconds\n";
  cout << "Passed." << endl;
  return 0;
}

