#include <iostream>
#include <cstdio>

#include "utils.h"
#include "Binary_tree.h"
#include "Hrect_bounds.h"
#include "Metric.h"
#include "kNN.h"
#include "Clock.hpp"

using namespace std;

static inline
void
usage__ (const char* use)
{
  fprintf (stderr, "usage: %s <N> <dim> <filename> <# clusters>\n", use);
}

void initialize_centroids (size_t num_pts, size_t dim, size_t num_clusters, Tree<Hrect>& stree, Points_t& centroids) {
  for (size_t i = 0; i < num_clusters; ++i) {
    // Randomly sample a point from the dataset 
    const size_t index = rand() % num_pts;
    centroids[i] = stree.data[index];
  }
}

/* Implements the naive k-means algorithm (Llyod's) */
bool validate(size_t num_pts, size_t dim, size_t num_clusters, Tree<Hrect>& stree, Points_t& centroids) {
  bool error = false;
	/* Set tolerance value for validation */
	const real_t tolerance = 1e-2;

  /* Cluster centroid change */
  real_t norm = DBL_MAX;

  /* Keep track of the iterations */
  size_t iter = 0;
  size_t niter = 100; //TODO: make this an env arg

  /* Allocate memory for exhaustive calculation of near clusters */
  kNN_distance* nearest = new kNN_distance[num_pts];

  /* Allocate memory for new centroids */
  Points_t new_centroids(num_clusters, dim);
  int counts[num_clusters];

  /* Define the metric type for distance calculations */
  SquaredEuclideanMetric<Point, Point> metric;

  for (size_t i = 0; i < num_clusters; ++i) 
    counts[i] = 0;

  while ((norm > tolerance) && (iter < niter)) {
    for (size_t i = 0; i < num_clusters; ++i)
      for (size_t d = 0; d < dim; ++d)
        new_centroids[i][d] = 0.0;

    // Find the new assignment of points to centroids
    for (size_t i = 0; i < num_pts; ++i) {
      real_t min_dist = DBL_MAX;
      for (size_t j = 0; j < num_clusters; ++j) {
        real_t dist = metric.compute_distance (stree.data[i], centroids[j]);
        if (dist < min_dist) {
          min_dist = dist;
          nearest[i] = kNN_distance(j, dist);
        }
      }
      for (size_t d = 0; d < dim; ++d) {
        new_centroids[nearest[i].index][d] += stree.data[i][d];
      }
      counts[nearest[i].index]++;
    }
    
    // Normalize the new centroids
    for (size_t i = 0; i < num_clusters; ++i) {
      if (counts[i] != 0) {
        for (size_t d = 0; d < dim; ++d)
          new_centroids[i][d] /= counts[i];
      counts[i] = 0;
      }
    }
  
    norm = 0.0;
    // Calcluate the change in cluster centers
    for (size_t i = 0; i < num_clusters; ++i) {
      norm += metric.compute_distance(centroids[i], new_centroids[i]);
    }
    norm = std::sqrt(norm);
    fprintf (stderr, "Norm: %lf\n", norm);
    centroids = new_centroids;
    iter++;
  } 
  return error;
}

int main (int argc, char** argv)
{
  char* filename;
  int num_pts;
  int dim;
  int num_clusters;
  bool error;
  Clock timer;

  if (argc != 5) {
    usage__ (argv[0]);
    return -1;
  }

  /** Command line input */
  num_pts = atoi (argv[1]);
  dim = atoi (argv[2]);
  filename = argv[3];
  num_clusters = atoi (argv[4]);

  /* Allocate memory for original source and centroid dataset */
  Points_t sources(num_pts, dim);
  Points_t centroids(num_clusters, dim);

  from_file(sources, filename);

  /* Allocate memory for the permuted source points */
  Points_t s_perm(num_pts, dim);

  /* Create and build source tree */
  typedef Tree<Hrect> TreeType;
  TreeType stree(sources, s_perm);

  fprintf (stderr, "Building source tree...\n");
  timer.start();
  stree.build_kdtree();
  double time_tree = timer.seconds();
  fprintf (stderr, "Source tree built.\n");

#ifdef _DEBUG
  std::cout << "Sources: " << std::endl;
  std::cout << sources << std::endl;

  cout << "In the main program...\n";
  std::cout << s_perm << std::endl;
#endif

  /* Initialize centroids */
  initialize_centroids (num_pts, dim, num_clusters, stree, centroids);

  /* Validate the results */
  error = validate(num_pts, dim, num_clusters, stree, centroids);

  /* Print timing results */
  cerr << "Tree construction time: " << time_tree << " seconds\n";
  if (error)
    cerr << "Program failed, error!\n";
  else
    cerr << "Passed.\n";

  return 0;
}
