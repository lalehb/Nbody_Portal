#!/usr/bin/env python

import os
from os import path
import time
import argparse
import csv

# ----- main directories for executions ----- #
#TOP_DIR = "/home/Laleh/Workspace/Nbody-ML/"
TOP_DIR ="/home/Laleh/Workspace/Nbody-ML-OpemMP-untied/"
RUN_DIR = TOP_DIR + "apps/tests/library"
OUT_DIR = TOP_DIR +  "apps/tests/results/"
#OUT_DIR = TOP_DIR +  "apps/tests/results-eachThreads/"
DATASET_DIR = "/home/real_datasets/"
RESULTS_DIR = TOP_DIR +  "apps/tests/"


# ----- setups for scaling experiments ----- #
TREES = ["kdtree"]

TRAVERSALS = ["yes"]

EXPR_NUMBERS = [1, 2, 3]
LEVELS = [1, 2, 4, 8, 10, 16, 32, 64, 100, 128]

# ----- threads for scaling -----#
THREADS = [32]

# ---- very small data set jut for checking ----- #

# ---- real data sets with smaller size ----- #
DATASETS = ["IHEPC"]
DIM = [9]
SIZE = [2075259]



# ----- scaling function  for kNN algorithm ----- #
def result_collector():

  
  outputName = OUT_DIR + "results.csv"
  outputfile = open(outputName, 'w+')
  for traversal in TRAVERSALS:
    for expNum in EXPR_NUMBERS:
      level = 0
      for dataset in DATASETS:
        for threads in THREADS:
          for levelP in LEVELS:
          
            inputName = OUT_DIR + "perf-kNN-kdtree-%s-th%d-expr%d-level%d.csv"%(dataset, threads, expNum, levelP)
            #datasetName = DATASET_DIR + dataset + ".csv"
            insertingWord = str(levelP)+ ","
            mainFile = open(inputName, 'r');
            for line in mainFile:
              words = line.split();
              if len(words) > 3:
                if words[0] == "thread":
                   insertingWord += str(words[3]) +  ","
                elif words[0] == "Dual-tree":
                   insertingWord += str(words[3]) + ","
                elif words[0] == "Tree":
                   insertingWord += words[3] + ","
            insertingWord += "\n"
            mainFile.close()
            outputfile.write(insertingWord)
        level = level + 1

  outputfile.close()




def main():
  # ----- making a directory for results ----- #
  #output_dir = RESULTS_DIR + "results"
  #os.system("mkdir " + output_dir )
  # ----- call to seperate functions for each algorithm ----- #
  result_collector()

if __name__ == "__main__":
  main()


