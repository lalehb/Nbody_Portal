#!/bin/bash

SIZE=10000
DIM=10
INPUT=../input/uniform_random/n${SIZE}_d${DIM}.csv
OUT=results.out

git checkout 9e70bd906ce2fd2cd75256e608761b601280535c -- ../tree/Dual_tree_traversal.h
make clean
make test--kNN_kdtree
make test--kNN_balltree

for i in 2 4 8 16 32 64 128
do
		echo "Dual kNN LS=$i SERIAL KDTREE" 2>&1 | tee -a ${OUT}
		env LS=$i S=no ./test--kNN_kdtree ${SIZE} ${DIM} ${INPUT} 3 2>&1 | tee -a ${OUT}
		echo "Dual kNN LS=$i SERIAL BALLTREE" 2>&1 | tee -a ${OUT}
		env LS=$i S=no ./test--kNN_balltree ${SIZE} ${DIM} ${INPUT} 3 2>&1 | tee -a ${OUT}
done

git checkout 685bfd353c6e0ff4a56ba7c247818cc0ae8078cd -- ../tree/Dual_tree_traversal.h
make clean
make test--kNN_kdtree
make test--kNN_balltree

for i in 2 4 8 16 32 64 128
do
		echo "Dual kNN LS=$i PARALLEL KDTREE" 2>&1 | tee -a ${OUT}
		env LS=$i S=no ./test--kNN_kdtree ${SIZE} ${DIM} ${INPUT} 3 2>&1 | tee -a ${OUT}
		echo "Dual kNN LS=$i PARALLEL BALLTREE" 2>&1 | tee -a ${OUT}
		env LS=$i S=no ./test--kNN_balltree ${SIZE} ${DIM} ${INPUT} 3 2>&1 | tee -a ${OUT}
done
