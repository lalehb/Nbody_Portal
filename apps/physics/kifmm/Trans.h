#ifndef __TRANS_H__
#define __TRANS_H__

#include <fftw3.h>

#include "Octree.h"

using namespace std;

/*  UE = Upper Equivalent
 * UC = Upper Check
 * DE = Downward Equivalent
 * DC = Downward Check
 */
enum {UE=0, UC=1, DE=2, DC=3,};

template <typename KernelMatrixType>
class Trans {
  public:
    KernelMatrixType* knl;

    Points sampos[4];
    Points regpos;

    real_t *UC2UE;
    real_t **UE2UC;
    real_t **UE2DC;
    real_t *DC2DE;
    real_t **DE2DC;

    Trans() {}
    Trans(KernelMatrixType* k) : knl(k) { }

    /* Setup */
    int setup();

    /* Calculate sample position grid */
    int compute_sampos(int n, real_t R, Points& pos);

    /* Calculate regular position grid */
    int compute_regpos(int n, real_t R, Points& pos);

    /* Calculate local position */
    int compute_localpos(int tp, Point3 center, real_t R, Points& pos);

    int compute_UC2UE_matrix ();

    int compute_UE2UC_matrix (Index3 idx);

    int compute_DC2DE_matrix ();

    int compute_DE2DC_matrix (Index3 idx);

    int compute_UE2DC_matrix ();

    /* Pointwise multiply */
    int pointwise_mult (int n, double alpha, FFT_COMPLEX* x, int ix, FFT_COMPLEX* y, int iy, FFT_COMPLEX* z, int iz);

    int plnden_to_effden(int l, real_t* pln_den, real_t* reg_den, real_t* tmp_den);
    int samden_to_regden(const real_t* sam_den, real_t* reg_den);
    int regval_to_samval(const real_t* reg_val, real_t* sam_val);
    int eff_data_size(int tp);
    int pln_size(int tp);

    real_t alt();

    /* src degree of freedom */
    int src_dof() { return knl->src_dof(); }

    /* trg degree of freedom */
    int trg_dof() { return knl->trg_dof(); }

    static real_t tmpden[];
    //  real_t* tmpden;
};

#include "Trans.cpp"

#endif
