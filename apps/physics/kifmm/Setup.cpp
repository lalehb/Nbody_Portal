#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>

#include "Octree.h"
#include "Fmm.h"

template<typename Kernel, typename KMatrix>
  void
FMM<Kernel,KMatrix>::setup ()
{
  //  trans.tmpden = (real_t *) reals_alloc__aligned (trans.src_dof()*trans.sampos[UE].n);

  permute_src ();
  permute_trg ();
}

template<typename Kernel, typename KMatrix>
  void
FMM<Kernel,KMatrix>::permute_src()
{
  size_t i, j, k, d;
  unsigned int dim = 3;
  vector<Tree::NodeTree>& nt = stree.node_data;
  Points& orig = stree.data;
  unsigned int num_pts = orig.size();
  size_t sdof = knl.src_dof();

  /* Allocate memory for permuted source points */
  s_perm = Points(num_pts);
  charge_perm.resize(sdof * num_pts);

  /* Create vectors */
  src_upw_equ_den_.resize(nt.size() * trans.pln_size(UE));
  src_upw_chk_val_.resize(nt.size() * trans.pln_size(UC));
  eff_den_.resize(nt.size() * trans.eff_data_size(UE));

  /* Permute source positions and density */
  for (i = 0; i < nt.size(); i++) {
    for (j = 0; j < nt[i].num; j++) {
      k = nt[i].vecid[j];
      for (d = 0; d < dim; ++d)
        s_perm[nt[i].beg+j][d] = orig[k][d];
      for (d = 0; d < sdof; ++d)
        charge_perm[(nt[i].beg+j)*sdof+d] = charge[k*sdof+d];
    }
  }
}

template<typename Kernel, typename KMatrix>
void
FMM<Kernel,KMatrix>::permute_trg()
{
  size_t i, j, k, d;
  unsigned int dim = 3;
  vector<Tree::NodeTree>& nt = ttree.node_data;
  Points& orig = ttree.data;
  unsigned int num_pts = orig.size();
  size_t tdof = knl.src_dof();

  /* Allocate memory for permuted target points */
  t_perm = Points(num_pts);
  result_perm.resize(tdof * num_pts);

  /* Create vectors */
  trg_dwn_equ_den_.resize(nt.size() * trans.pln_size(DE));
  trg_dwn_chk_val_.resize(nt.size() * trans.pln_size(DC));
  eff_val_.resize(nt.size() * trans.eff_data_size(DC));

  /* Permute target positions */
  for (i = 0; i < nt.size(); i++) {
    for (j = 0; j < nt[i].num; j++) {
      k = nt[i].vecid[j];
      for (d = 0; d < dim; ++d)
        t_perm[nt[i].beg+j][d] = orig[k][d];
    }
  }
}

