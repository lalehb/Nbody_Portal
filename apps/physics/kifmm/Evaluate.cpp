#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>
#include <omp.h>

#include "Octree.h"
#include "Fmm.h"
#include "VectorMatrixOp.h"
#include "Utils.h"

//real_t Trans::tmpden[16384];

template<typename Kernel, typename KMatrix>
  int
FMM<Kernel,KMatrix>::run ()
{
  fprintf (stderr, "Performing Up calculation ...\n");
  up_calc ();
  fprintf (stderr, "Done Up.\n");

  fprintf (stderr, "Performing U-list calculation (direct evaluation)...\n");
  ulist_calc ();
  fprintf (stderr, "Done Ulist.\n");

  fprintf (stderr, "Performing V-list calculation (pointwise multiply)...\n");
  vlist_calc ();
  fprintf (stderr, "Done Vlist.\n ");

  fprintf (stderr, "Performing W-list calculation...\n");
  wlist_calc ();
  fprintf (stderr, "Done Wlist.\n ");

  fprintf (stderr, "Performing X-list calculation...\n");
  xlist_calc ();
  fprintf (stderr, "Done Xlist.\n ");

  fprintf (stderr, "Performing Down calculation ...\n");
  down_calc ();
  fprintf (stderr, "Done Down.\n");

  copy_trg_val ();

  return 0;
}

/* ------------------------------------------------------------------------
 */

/* Upward Calculation */
template<typename Kernel, typename KMatrix>
  int
FMM<Kernel,KMatrix>::up_calc ()
{
  vector<Tree::NodeTree>& nt = stree.node_data;
  vector<int> boxes;

  stree.order_bottomup(boxes);

  for (size_t i = 0; i < boxes.size(); i++) {
    int j = boxes[i];
    if (nt[j].depth >=0) {
      if (nt[j].child == -1)
        S2M (j);
      else
        M2M (j);
      upward_density_calc (j);
    }
  }
  return 0;
}

/* Source to Multiple Calculation */
template<typename Kernel, typename KMatrix>
  int
FMM<Kernel,KMatrix>::S2M(int i)
{
  Point3 c;
  real_t r;

  Points tpos(trans.sampos[UC].size());

  /* Upward check potential */
  c = stree.center(i);
  r = stree.radius(i);
  trans.compute_localpos (UC, c, r, tpos);

  auto upot = src_upw_chk_val_it(i);
  points_iter tposi = boost::make_iterator_range(tpos.begin(), tpos.end());
  knl.kernel_evaluation (tposi, source(i), density(i), upot);

  return 0;
}

/* Multipole to Multipole Calculation */
template<typename Kernel, typename KMatrix>
  int
FMM<Kernel,KMatrix>::M2M(int i)
{
  vector<Tree::NodeTree>& nt = stree.node_data;

  for(int a=0; a<2; a++) {
    for(int b=0; b<2; b++) {
      for(int c=0; c<2; c++) {
        Index3 idx(a,b,c);
        int chi = stree.child(i, idx);
        auto uden = src_upw_equ_den_data(chi);
        auto upot = src_upw_chk_val_data(i);
        real_t *tmpden = (real_t *) reals_alloc__aligned (trans.src_dof()*trans.sampos[UE].size());
        reals_zero (trans.src_dof()*trans.sampos[UE].size(), tmpden);
        int l = nt[chi].depth;
        real_t degvec[trans.src_dof()];
        trans.knl->homogeneous_degree (degvec);
        Vec sclvec(trans.src_dof());
        for(int s=0; s<trans.src_dof(); s++)
          sclvec[s] = pow(2.0, l*degvec[s]);
        int cnt = 0;
        for(int i=0; i < trans.sampos[UE].size(); i++)
          for(int s=0; s < trans.src_dof(); s++) {
            tmpden[cnt] = uden[cnt] * sclvec[s];
            cnt++;
          }
        real_t* UE2UCii = trans.UE2UC[idx(2)+idx(1)*2+idx(0)*2*2];
        int m = trans.pln_size (UC);
        int n = trans.pln_size (UE);
        dgemv(m, n, 1.0, UE2UCii, tmpden, 1.0, upot);
      }
    }
  }

  return 0;
}

/* Upward equivalent density calculation */
template<typename Kernel, typename KMatrix>
void
FMM<Kernel,KMatrix>::upward_density_calc (int i) {
  vector<Tree::NodeTree>& nt = stree.node_data;

  real_t *tmpden = (real_t *) reals_alloc__aligned (trans.src_dof()*trans.sampos[UE].size());
  reals_zero (trans.src_dof()*trans.sampos[UE].size(), tmpden);
  auto upot = src_upw_chk_val_data(i);
  int r = trans.pln_size (UC);
  int c = trans.pln_size (UE);

  dgemv(c, r, 1.0, trans.UC2UE, upot, 1.0, tmpden);

  //scale
  auto uden = src_upw_equ_den_data(i);
  int l = nt[i].depth;
  real_t degvec[trans.src_dof()];
  trans.knl->homogeneous_degree (degvec);
  real_t sclvec[trans.src_dof()];
  for(int s=0; s<trans.src_dof(); s++)
    sclvec[s] = pow(2.0, -l*degvec[s]);
  int cnt = 0;
  for(int i=0; i < trans.sampos[UE].size(); i++)
    for(int s=0; s < trans.src_dof(); s++) {
      uden[cnt] = uden[cnt] + tmpden[cnt] * sclvec[s];
      cnt++;
    }
}

/* ------------------------------------------------------------------------
 */

/* Direct (U-list) Calculation */
template<typename Kernel, typename KMatrix>
  int
FMM<Kernel,KMatrix>::ulist_calc ()
{
  vector<Tree::NodeTree>& nt = ttree.node_data;

  for (size_t i = 0; i < nt.size(); i++) {
    if (nt[i].child == -1) {
      for (size_t k = 0; k < nt[i].Unodes.size(); k++) {
        int src = nt[i].Unodes[k];
        knl.kernel_evaluation (target(i), source(src), density(src), potential(i));
      }
    }
  }
  return 0;
}
/* ------------------------------------------------------------------------
 */

/* Far-field Calculation (Multipole to Local) */
template<typename Kernel, typename KMatrix>
  int
FMM<Kernel,KMatrix>::vlist_calc ()
{
  int effnum;
  int dim = 3;
  real_t *UpwEqu2DwnChkii;

  vector<Tree::NodeTree>& nt = ttree.node_data;

  compute_fft_src ();

  int np = getenv__accuracy();
  effnum = (2*np+2)*(2*np)*(2*np);

  for (size_t i = 0; i < nt.size(); i++) {
    if (nt[i].Vnodes.size()>0) {
      auto eval = eff_val_data(i);
      Point3 trg_center(ttree.center(i));
      real_t D = 2.0 * ttree.radius (i);
      for (size_t j = 0; j < nt[i].Vnodes.size(); j++) {
        int src = nt[i].Vnodes[j];
        Point3 src_center(stree.center(src));
        Index3 idx;
        for(int d=0; d<dim; d++) {
          idx(d) = int (round( (src_center[d]-trg_center[d])/D ));
        }
        auto eden = eff_den_data(src);
        //fft mult
        int id = (idx(0)+3) + (idx(1)+3)*7 + (idx(2)+3)*7*7;
        UpwEqu2DwnChkii = trans.UE2DC[id];
        real_t nrmfc = 1.0/real_t(trans.regpos.size());
        FFT_COMPLEX* matPtr  = (FFT_COMPLEX*)(UpwEqu2DwnChkii);
        FFT_COMPLEX* denPtr = (FFT_COMPLEX*)(eden);
        FFT_COMPLEX* chkPtr   = (FFT_COMPLEX*)(eval);
        int matStp  = trans.src_dof()*trans.trg_dof();
        int denStp = trans.src_dof();
        int chkStp   = trans.trg_dof();

        real_t newalpha = nrmfc;
        for(int r=0; r<trans.trg_dof(); r++)
          for(int k=0; k<trans.src_dof(); k++) {
            int matOff = k*trans.trg_dof() + r;
            int denOff = k;
            int chkOff = r;
            trans.pointwise_mult (effnum/2, newalpha, matPtr+matOff, matStp, denPtr+denOff, denStp, chkPtr+chkOff, chkStp);
          }
      }
    }
  }

  compute_ifft_trg ();

  return 0;
}

template<typename Kernel, typename KMatrix>
  int
FMM<Kernel,KMatrix>::compute_fft_src ()
{
  size_t i = 0;
  FFT_PLAN forplan = NULL;

  vector<Tree::NodeTree>& nt = stree.node_data;

  Vec reg_den(trans.src_dof() * trans.regpos.size());
  Vec tmpden(trans.src_dof()*trans.sampos[UE].size());

  auto eden = eff_den_data(i);
  auto uden = src_upw_equ_den_.data() + i*trans.pln_size(UE);
  auto rden = reg_den.data();
  auto tden = tmpden.data();
  trans.plnden_to_effden(nt[i].depth, uden, rden, tden);
  int np = getenv__accuracy();
  int nnn[3];  nnn[0] = 2*np;  nnn[1] = 2*np;  nnn[2] = 2*np;
  forplan = FFT_CREATE(3, nnn, trans.src_dof(), rden, NULL, trans.src_dof(), 1, (FFT_COMPLEX*)(eden), NULL, trans.src_dof(), 1, FFTW_ESTIMATE);
  FFT_EXECUTE(forplan);

  for(i = 1; i<nt.size(); i++) {
    auto eden = eff_den_data(i);
    auto uden = src_upw_equ_den_data(i);
    auto rden = reg_den.data();
    auto tden = tmpden.data();
    trans.plnden_to_effden(nt[i].depth, uden, rden, tden);
    FFT_RE_EXECUTE(forplan, rden, (FFT_COMPLEX*)(eden));
  }

  return 0;
}

template<typename Kernel, typename KMatrix>
  int
FMM<Kernel,KMatrix>::compute_ifft_trg ()
{
  size_t i = 0;
  int invset = 0;
  FFT_PLAN invplan;

  vector<Tree::NodeTree>& nt = ttree.node_data;

  Vec reg_val(trans.regpos.size() * trans.trg_dof());

  while(invset == 0) {
    if (nt[i].Vnodes.size()>0) {
      auto tval = trg_dwn_chk_val_.data() + i*trans.pln_size(DC);
      auto eval = eff_val_data(i);
      auto rval = reg_val.data();
      int np = getenv__accuracy();
      int nnn[3];  nnn[0] = 2*np;  nnn[1] = 2*np;  nnn[2] = 2*np;
      invplan = IFFT_CREATE(3, nnn, trans.trg_dof(), (FFT_COMPLEX*)(eval), NULL, trans.trg_dof(), 1, rval, NULL, trans.trg_dof(), 1, FFTW_ESTIMATE);
      IFFT_EXECUTE(invplan);

      trans.regval_to_samval(rval, tval);
      invset = 1;
    }
    i++;
  }

  for(size_t j=i; j < nt.size(); j++) {
    if (nt[j].Vnodes.size()>0) {
      auto tval = trg_dwn_chk_val_data(j);
      auto eval = eff_val_data(j);
      auto rval = reg_val.data();
      IFFT_RE_EXECUTE(invplan, (FFT_COMPLEX*)(eval), rval);
      trans.regval_to_samval(rval, tval);
    }
  }

  FFT_DESTROY(invplan);

  return 0;
}

/* ------------------------------------------------------------------------
 */
template<typename Kernel, typename KMatrix>
  int
FMM<Kernel,KMatrix>::wlist_calc ()
{
  Point3 c;
  real_t r;
  vector<Tree::NodeTree>& nt = ttree.node_data;
  vector<Tree::NodeTree>& ns = stree.node_data;

  Points spos(trans.sampos[UE].size());

  for (size_t i = 0; i < nt.size(); i++) {
    if (nt[i].child == -1) {
      for (size_t j = 0; j < nt[i].Wnodes.size(); j++) {
        int src = nt[i].Wnodes[j];
        if(ns[src].child == -1 && ns[src].num*knl.src_dof()<trans.pln_size(UE)) {
          /* S2T */
          knl.kernel_evaluation (target(i), source(src), density(src), potential(i));
        }
        else {
          /* M2T */
          c = stree.center(src);
          r = stree.radius(src);
          trans.compute_localpos (UE, c, r, spos);
          auto sden = src_upw_equ_den_it(src);
          points_iter sposi = boost::make_iterator_range(spos.begin(), spos.end());
          knl.kernel_evaluation (target(i), sposi, sden, potential(i));
        }
      }
    }
  }
  return 0;
}

/* ------------------------------------------------------------------------
 */
template<typename Kernel, typename KMatrix>
  int
FMM<Kernel,KMatrix>::xlist_calc ()
{
  Point3 c;
  real_t r;
  vector<Tree::NodeTree>& nt = ttree.node_data;

  Points tpos(trans.sampos[DC].size());

  for (size_t i = 0; i < nt.size(); i++) {
    for (size_t j = 0; j < nt[i].Xnodes.size(); j++) {
      int src = nt[i].Xnodes[j];
      if(nt[i].child == -1 && nt[i].num*knl.trg_dof()<trans.pln_size(DC)) {
        /* S2T */
        knl.kernel_evaluation (target(i), source(src), density(src), potential(i));
      }
      else {
        /* S2L */
        c = ttree.center(i);
        r = ttree.radius(i);
        trans.compute_localpos (DC, c, r, tpos);
        auto tval = trg_dwn_chk_val_it(i);
        points_iter tposi = boost::make_iterator_range(tpos.begin(), tpos.end());
        knl.kernel_evaluation (tposi, source(src), density(src), tval);
      }
    }
  }
  return 0;
}

/* ------------------------------------------------------------------------
 */

/* Downward Calculation */
template<typename Kernel, typename KMatrix>
  int
FMM<Kernel,KMatrix>::down_calc ()
{
  vector<Tree::NodeTree>& nt = ttree.node_data;
  vector<int> boxes;

  ttree.order_topdown(boxes);

  for (size_t i = 0; i < boxes.size(); i++) {
    int j = boxes[i];
    if (nt[j].depth >= 3)
      L2L_DE2DC (j);
    if (nt[j].depth >= 2)
      L2L_DC2DE (j);
    if (nt[j].child == -1)
      L2T (j);
  }
  return 0;
}

/* Local to Local calculation */
template<typename Kernel, typename KMatrix>
  int
FMM<Kernel,KMatrix>::L2L_DE2DC(int i)
{
  vector<Tree::NodeTree>& nt = ttree.node_data;

  int parent = nt[i].parent;
  Index3 cidx(nt[i].path2node-2 * nt[parent].path2node);

  auto tval = trg_dwn_chk_val_data(i);
  auto dden = trg_dwn_equ_den_data(parent);
  Vec tmpden(trans.sampos[DE].size()*trans.src_dof());

  int l = nt[parent].depth;
  real_t degvec[trans.src_dof()];
  trans.knl->homogeneous_degree (degvec);
  real_t sclvec[trans.src_dof()];
  for(int s=0; s<trans.src_dof(); s++)
    sclvec[s] = pow(2.0, l*degvec[s]);
  int cnt = 0;
  for(int k=0; k < trans.sampos[DE].size(); k++)
    for(int s=0; s < trans.src_dof(); s++) {
      tmpden[cnt] = dden[cnt] * sclvec[s];
      cnt++;
    }
  real_t *DE2DCii = trans.DE2DC[cidx(2)+cidx(1)*2+cidx(0)*2*2];
  int r = trans.pln_size (DC);
  int c = trans.pln_size (DE);

  dgemv(r, c, 1.0, DE2DCii, tmpden.data(), 1.0, tval);
  return 0;
}

template<typename Kernel, typename KMatrix>
  int
FMM<Kernel,KMatrix>::L2L_DC2DE(int i)
{
  vector<Tree::NodeTree>& nt = ttree.node_data;

  auto tval = trg_dwn_chk_val_data(i);
  auto dden = trg_dwn_equ_den_data(i);
  Vec tmpden(trans.sampos[DE].size()*trans.src_dof());
  std::fill(tmpden.begin(), tmpden.end(), 0.0);


  int r = trans.pln_size (DC);
  int c = trans.pln_size (DE);
  dgemv(c, r, 1.0, trans.DC2DE, tval, 1.0, tmpden.data());

  int l = nt[i].depth;
  real_t degvec[trans.src_dof()];
  trans.knl->homogeneous_degree (degvec);
  real_t sclvec[trans.src_dof()];
  for(int s=0; s<trans.src_dof(); s++)	{
    sclvec[s] = pow(2.0, -l*degvec[s]);
  }
  int cnt = 0;
  for(int k=0; k < trans.sampos[DE].size(); k++)
    for(int s=0; s < trans.src_dof(); s++) {
      dden[cnt] = dden[cnt] + tmpden[cnt] * sclvec[s];
      cnt++;
    }
  return 0;
}

/* Local to Target Calculation */
template<typename Kernel, typename KMatrix>
  int
FMM<Kernel,KMatrix>::L2T(int i)
{
  Point3 c;
  real_t r;

  Points spos(trans.sampos[DE].size());
  c = ttree.center (i);
  r = ttree.radius (i);
  trans.compute_localpos (DE, c, r, spos);

  auto dden = trg_dwn_equ_den_it(i);
  points_iter sposi = boost::make_iterator_range(spos.begin(), spos.end());
  trans.knl->kernel_evaluation (target(i), sposi, dden, potential(i));
  return 0;
}

/* ------------------------------------------------------------------------
 */

template<typename Kernel, typename KMatrix>
  int
FMM<Kernel,KMatrix>::copy_trg_val ()
{
  int k;
  vector<Tree::NodeTree>& nt = ttree.node_data;
  size_t tdof = knl.trg_dof();

  for (size_t i = 0; i < nt.size(); i++)
    if (nt[i].child == -1) {
      for (size_t j = 0; j < nt[i].num; j++) {
        k = nt[i].vecid[j];
        for (size_t d = 0; d < tdof; d++) {
          result[k*tdof+d] = result_perm[(nt[i].beg+j)*tdof+d];
        }
      }
    }
  return 0;
}
/* ----------------------------------------------------------------------------------------------------------
 * eof
 */
