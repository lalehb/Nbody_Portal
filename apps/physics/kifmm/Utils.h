#ifndef __UTILS_H__
#define __UTILS_H__

  size_t getenv__int (const char* var, int default_value);
  size_t getenv__flag (const char* var, int default_value);
  size_t getenv__numa (void);
  size_t getenv__upbs (void);
  size_t getenv__accuracy (void);
  size_t getenv__block_size (void);
  size_t getenv__validate (void);

#include "Utils.cpp"

#endif
