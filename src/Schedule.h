#ifndef INC_SCHEDULE_H
#define INC_SCHEDULE_H

namespace Nbody {

/* Internal representation of a schedule for a Nbody function */

struct Schedule {
  /* Evaluating a function is done by generating a loop nest that
  spans its dimensions. The inputs to the function are scheduled
  by injecting evaluations for them at particular points in the
  loop body. Loc structure defines those locations. */
  struct Loc {
    std::string func, var;

    Loc(std::string f, std::string v) : func(f), var(v) {}
//    Loc(Func f, Var v) : Loc(f.name(), v.name()) {}
    
    /* Empty Loc implies that the function should be inlined */
    Loc() {}

    /* Check if the function should be inlined */
    bool do_inline() const {
      return var.empty();
    }

    /* root is a special Loc value that represents the location
    outside all loops */
    static Loc root() {
      return Loc("", "<root>");
    }

    /* Check if the loc is 'root' or outside all loops */
    bool is_root() const {
      return var == "<root>";
    }

    /* Check if this loc refers to a point inside the given loop */
    bool match(const std::string& loop) const {
      return begins_with(loop, func + ".") && ends_with(loop, "." + var);
    }
  };

  /* Locations we inject the allocation and computation of this
  function */
  Loc store_loc, compute_loc;

  /* Dim structure for defining dimensions of this function */
  struct Dim {
    std::string var;
    int stride;
    For::ForType ftype;
  };
  /* The list and ordering of dimensions for evaluating this function.
  The first dimension corresponds to the innermost and the last
  dimension to the outermost for loop. */
  std::vector<Dim> dim;
};

}

#endif
