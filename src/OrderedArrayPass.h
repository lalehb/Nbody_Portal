#ifndef INC_ORDEREDARRAY_PASS_H
#define INC_ORDEREDARRAY_PASS_H

#include "IRPass.h"

namespace Nbody {

class OrderedArray : public IRPass {

  void insertAndShift(std::string arrayName , Expr valueToInsert , Expr insertionIndex , int k){
	/*
	value - value to insert
	i - index to shift at
	k - length of storage block
	kth = k - 1
	while(i < kth){
		MEM[kth] = MEM[kth - 1]
		kth = kth - 1
	}
	MEM[i] = value
	*/
	std::vector<Stmt> operation;
	std::vector<Stmt> inWhile;

	operation.push_back(Assign::make("kth" , Integer::make(k - 1)));

	Expr prevValue = Load::make(Float(32) , arrayName , Variable::make("kth") - 1 , true , 0);
	inWhile.push_back(Store::make(arrayName , prevValue , Variable::make("kth")));
	inWhile.push_back(Assign::make("kth" , Variable::make("kth") - 1));

	operation.push_back(While::make("insertAndShift" , insertionIndex < Variable::make("kth") , BlockStmt::make(inWhile)));
	operation.push_back(Store::make(arrayName , valueToInsert , insertionIndex));

	stmt = BlockStmt::make(operation);
  }



  void visit (const OrderedArray_Insert* op) {
	/*
		This is what is called when this OrderedArray_pass is used and when the OrderedArray_Insert Node is encountered

		The OrderedArray_Insert node has 4 attributes
			std::string arrayName - variable that contains the pointers to the array we are inserting to.
			int k;
				How many values we are reducing to.
			Expr value;
				The value we are inserting, can be a variable name
			Expr index;
				The index of the value we are inserting, can be a variable name
				This can be useful to the karg operators

	*/

      /*
    	value - value to insert
    	index - default value
    	k - length of storage block


    	kth = k - 1

      while((value < M[kth])&& (kth > 0)){
    		MEM[kth] = MEM[kth - 1]
    		kth = kth - 1
    	}
      MEM[kth] = value

    	*/

      std::vector<Stmt> operation;
      std::vector<Stmt> inWhile;

      operation.push_back(Assign::make("kth" , Integer::make(op->k - 1)));

      Expr prev = Load::make(Float(32) , op->arrayName , op->index + Variable::make("kth") , false , 1);
      Expr comp = ComparisonOp::make(op->value , "<" , prev);
      operation.push_back(Store::make(op->arrayName , Select::make( comp, op->value, prev ), op->index+ Variable::make("kth") ));


      Expr prevValue = Load::make(Float(32) , op->arrayName , op->index + Variable::make("kth") - 1 , false , 1);
      inWhile.push_back(Store::make(op->arrayName , prevValue , op->index +Variable::make("kth")));
      inWhile.push_back(Assign::make("kth" , Variable::make("kth") - 1));

      Expr Cond1 = ComparisonOp::make(op->value , "<" , 	prevValue);
      Expr Cond2 = ComparisonOp::make(Integer::make(0) , "<" , 	Variable::make("kth"));
      Expr Cond_final = ComparisonOp::make(Cond1 , "&&" , 	Cond2);
      operation.push_back(While::make("insertAndShift" ,  Cond_final , BlockStmt::make(inWhile)));


      Expr After_val = Load::make(Float(32) , op->arrayName , op->index + Variable::make("kth") , false , 1);
      Expr comp2 = ComparisonOp::make(op->value , "<" , After_val);
      operation.push_back(Store::make(op->arrayName , Select::make( comp2, op->value, After_val ), op->index+ Variable::make("kth") ));

      stmt = BlockStmt::make(operation);

  }


};

inline Stmt OrderedArray_pass(Stmt s) {
  return OrderedArray().mutate(s);
}

}
#endif
