#include "KeyValueStorage.h"

KeyValueStorage::KeyValueStorage(){
	
}

KeyValueStorage::KeyValueStorage(StorageObject key , StorageObject value){
	this->k = key;
	this->v = value;
}

StorageObject KeyValueStorage::getKey(){
	return this->k;
}

StorageObject KeyValueStorage::getValue(){
	return this->v;
}