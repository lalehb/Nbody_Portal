#ifndef INC_FINDCALLS_H
#define INC_FINDCALLS_H

#include <string>
#include <map>
#include "IRNode.h"

namespace Nbody {

/* Find all Nbody calls in an expr */
class FindCalls : public IRVisitor {
public:
  std::map<std::string, Function> calls;

  void include_function(Function f) {
    std::map<std::string, Function>::iterator iter = calls.find(f.name());
    if (iter == calls.end()) 
      calls[f.name()] = f;
    else 
      assert(iter->second.same_as(f) && "Can't compile using multiple functions with same name");
  }

  void visit(const Call *call) {
    if (call->ctype == Call::Nbody && call->func.defined()) {
      Function f(call->func);
      include_function(f);
    }
  }
};

void populate_environment(Function f, std::map<std::string, Function> &env, bool recursive = true) {
  std::map<std::string, Function>::const_iterator iter = env.find(f.name());
  if (iter != env.end()) {
    assert(iter->second.same_as(f) && "Can't compile using multiple functions with same name");
    return;
  }

  FindCalls calls;
  f.value().accept(&calls);

  if (!recursive)
    env.insert(calls.calls.begin(), calls.calls.end());
  else {
    env[f.name()] = f;
    for (std::pair<std::string, Function> i : calls.calls)
      populate_environment(i.second, env);
  }
}

/* Construct a map from name to function definition object for all Nbody
 functions called directly in the definition of the function f */
std::map<std::string, Function> find_direct_calls(Function f) {
  std::map<std::string, Function> res;
  populate_environment(f, res, false);
  return res;
}

/* Construct a map from name to function definition object for all Nbody
 functions called directly in the definition of the function f, or
 indirectly in those functions' definitions, recursively. */ 
std::map<std::string, Function> find_transitive_calls(Function f) {
  std::map<std::string, Function> res;
  populate_environment(f, res, true);
  return res;
}

}

#endif
