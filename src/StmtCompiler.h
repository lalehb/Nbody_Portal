#ifdef _DEBUG
#define DEBUG_MSG(str) do { std::cout << str; } while( false )
#else
#define DEBUG_MSG(str) do { } while ( false )
#endif

#ifndef INC_STMTCOMPILER_H
#define INC_STMTCOMPILER_H

#include "IRNode.h"
#include "Codegen.h"
#include <utility>


/* Generates assembly, bitcode, machine code, or
jit-compiled module from the given input statement. */

namespace Nbody {

class StmtCompiler {
  Ptr<CodeGenContext> contents;

public:
  StmtCompiler() {
    contents = new CodeGenContext();
  }

  void compile(Stmt stmt, std::string name, const std::vector<Argument>& args) {
    contents.ptr->compile(stmt, name, args);
  }

  void compile_to_bitcode(const std::string& filename) {
    contents.ptr->compile_to_bitcode(filename);
  }

  JITModule compile_to_function_pointers() {
    JITModule module = contents.ptr->compile_to_function_pointers();
	DEBUG_MSG("StmtCompiler compile_to_function_pointers \n");
	return module;
  }
  
  JITModule compile_to_function_pointers(std::vector<std::pair<std::string , void *>> extFuncts) {
    JITModule module = contents.ptr->compile_to_function_pointers(extFuncts);
	DEBUG_MSG("StmtCompiler compile_to_function_pointers \n");
	return module;
  }
};

}

#endif
