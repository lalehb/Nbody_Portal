#ifndef INC_VARIABLE_H
#define INC_VARIABLE_H

#include "IRNode.h"

namespace Nbody {

class Var {
  std::string _name;

public:
  /* Construct a variable with the given name */
  Var(const std::string& n) : _name(n) {}

  /* Construct a variable with an automatically generated name */
  Var() : _name(generate_name('v')) {}

  /* Get the name of the variable */
  const std::string& name() const {
      return _name;
  }

  /* Check if two variable names are the same */
  bool same_as(const Var& other) const {
    return _name == other._name;
  }

  /* Variable can be treated as an expression of type Int(32) */
  operator Expr() {
    return Variable::make(Int(32), name());
  }
};

}

#endif
