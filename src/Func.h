#ifndef INC_FUNC_H
#define INC_FUNC_H

#include "IRNode.h"
#include "IRPass.h"
#include "IROperator.h"
#include "Function.h"
#include "Variable.h"
#include "ReductionWrapper.h"
#include "JITModule.h"
namespace Nbody {

/* Represents Vars or RVars.
 */
struct VarOrRVar {
  const Var var;
  const RVar rvar;
  const bool is_rvar;

  VarOrRVar(const Var& v) : var(v), is_rvar(false) {}
  VarOrRVar(const RVar& r) : rvar(r), is_rvar(true) {}
  VarOrRVar(const RDom& r) : rvar(RVar(r)), is_rvar(true) {}

  const std::string& name() const {
    if (is_rvar) 
      return rvar.name();
    else
      return var.name();
  }
};

class InferArguments : public IRPass {
public:
  std::vector<Argument> arg_types;
  std::vector<const void*> arg_values;

private:
  void visit(const Load* op) {
    std::cerr << "InferArguments Load...\n";
    IRPass::visit(op);

    Data d;
    std::string arg_name;
    if (op->points.defined()) {
      std::cerr << "Found a points argument: " << op->points.name() << '\n';
      d = op->points;
      arg_name = op->points.name();
    }
    else
      return;

    Argument arg(arg_name, true, op->type);
    bool inferred = false;
    for (size_t i = 0; i < arg_types.size(); ++i) {
      if (arg_types[i].name == op->name)
        inferred = true;
    }
    if (!inferred) {
      arg_types.push_back(arg);
      if (op->points.defined()) {
        assert(d.defined() && "Points undefined");
        arg_values.push_back(d.raw_buffer());
      }
      else
        arg_values.push_back(NULL);
    }
  }
};

/* A wrapper around a schedule for common transformations on
a Nbody function */
class Transform {
  Schedule& schedule;
  void set_dim_type(VarOrRVar v, int stride, For::ForType ftype);

public:
  Transform(Schedule& s) : schedule(s) {}

  Transform& vectorize(VarOrRVar v, int stride);
};

/* Inject base-case definition for reduction */
void define_base_case(Function func, const std::vector<Expr>& a, Expr e);

/* Front-end syntactic sugar of the form f(x, y) where x and y
 * are expressions. Used for reduction defintion.
 */
class FuncArgExpr {
  Function func;
  std::vector<Expr> args;

public:
  FuncArgExpr(Function f, const std::vector<Expr>& a) : func(f), args(a) {
    for (size_t i = 0; i < args.size(); ++i)
      args[i] = cast<int>(args[i]);
  }

  FuncArgExpr(Function f, const std::vector<std::string>& a) : func(f) {
    args.resize(a.size());
    for (size_t i = 0; i < a.size(); ++i)
      args[i] = Var(a[i]);
  }

  void operator=(Expr e) {
    assert (func.has_pure_definition() && "Can't add a reduction definition to an undefined function");
    std::vector<Expr> a = args;
    func.define_reduction(a, e);
  }

  void operator+=(Expr e) {
    std::vector<Expr> a = args;
    define_base_case(func, a, cast(e.type(), 0));
    (*this) = Expr(*this) + e;
  }

  operator Expr() const {
    assert(func.has_pure_definition() && "Can't call undefined function");
    return Call::make(func, args);
  }
};

/* Front-end syntactic sugar of the form f(x, y) where x and y
 * are variables.
 */
class FuncArgVar {
  Function func;
  std::vector<std::string> args;

public:
  FuncArgVar(Function f, const std::vector<Var>& a) : func(f) {
    args.resize(a.size());
    for (size_t i = 0; i < a.size(); ++i) {
      args[i] = a[i].name();
      std::cerr << "Func arg name: " << a[i].name() << std::endl;
    }
  }

  void operator=(Expr e) {
    std::vector<std::string> a = args;
    func.define(a, e);
  }

  /* Sum reduction */
  void operator+=(Expr e) {
    FuncArgExpr(func, args) += e;
  }

  operator Expr() const {
    assert(func.has_pure_definition() && "Can't call undefined function");
    std::vector<Expr> expr_args(args.size());
    for (size_t i = 0; i < expr_args.size(); ++i)
      expr_args[i] = Var(args[i]);
    return Call::make(func, expr_args);
  }
};

/** N-body function. This could be a kernel function, expansion,
prune, or approximation logic. */
class Func {
  /* Handle to internal function */
  Function func;

  /* The lowered form of this function */
  Stmt lowered;

  /* A JIT compiled version of the function (used for
  multiple evaluation without re-jitting */
  JITModule compiled_module;

  /* Pointers to inferred arguments used to evaluate this function.
  Only relavent when jitting. */
  std::vector<const void*> arg_values;

public:
  /* Construct a function with an automatically generated name */
  Func() : func(generate_name('f')) {}

  /* Construct a new Func to wrap an existing defined Function object */
  Func (Function f) : func(f) {}

  /* Evaluate this function over the given range and return the
  output */
  Data evaluate(int m = 0, int n = 0);

  /* Evaluate this function */
  void evaluate(Data dst);

  /* Statically compile this function to llvm bitcode with the given
  filename */
  void compile_to_bitcode(const std::string& filename, std::vector<Argument>);

  /* Output the internal representation of the lowered code */
  void compile_to_lowered_form(const std::string& fname);

  /* JIT compile the function to machine code and return a raw function pointer to
  the compiled module */
  void* jit_compile();

  /* Get the name of the variable */
  const std::string& name() const {
      return func.name();
  }

  /* Get the pure arguments */
  std::vector<Var> args() const {
    const std::vector<std::string> arg_names = func.args();
    std::vector<Var> args(arg_names.size());
    for (size_t i = 0; i < arg_names.size(); ++i)
      args[i] = Var(arg_names[i]);
    return args;
  }

  /* Get the right-hand side of the pure definition of this function */
  Expr value() const {
    return func.value();
  }

  /* The dimensionality (number of arguments) of this function */
  size_t dims() const {
    return func.dims();
  }

  /* Check if this function has a pure definition */
  bool defined() const {
    return func.has_pure_definition();
  }

  /* Construct lef-hand size of function definition or function call
  with only variables as aguments. */
  FuncArgVar operator()(Var x) const {
    std::vector<Var> args = vec(x);
    return FuncArgVar(func, args);
  }

  FuncArgVar operator()(Var x, Var y) const {
    std::vector<Var> args = vec(x, y);
    return FuncArgVar(func, args);
  }

  /* Construct left-hand size of function definition with Expr as args */
  FuncArgExpr operator()(Expr x) const {
    std::vector<Expr> args = vec(x);
    return FuncArgExpr(func, args);
  }

  FuncArgExpr operator()(Expr x, Expr y) const {
    std::vector<Expr> args = vec(x, y);
    return FuncArgExpr(func, args);

  }

  /* Vectorize the given dimension */
  Func& vectorize(VarOrRVar v, int stride) {
    Transform(func.schedule()).vectorize(v, stride);
    return *this;
  }

  /* Schedule a function to be computed at a givel Loc */
  Func& compute_at(Schedule::Loc location) {
    func.schedule().compute_loc = location;
    std::cerr << "Inside compute at\n";
  }

  /* Function is computed ahead of time -- no redudant work, poor locality */
  Func& compute_root() {
    std::cerr << "Inside compute root\n";
    return compute_at(Schedule::Loc::root());
  }

  /* Schedule storage at a given Loc */
  Func& store_at(Schedule::Loc location) {
    func.schedule().store_loc = location;
    std::cerr << "Inside store at\n";
  }

  /* Schedules storage outside of the outermost loop */
  Func& store_root() {
    std::cerr << "Inside store root\n";
    return store_at(Schedule::Loc::root());
  }

  /* Return handle on the internal function this represents. */
  Function function() const {
    return func;
  }
};

}

#endif
