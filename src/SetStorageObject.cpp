#include "SetStorageObject.h"


SetStorageObject::SetStorageObject(){
	this->modified = false;
}

/**
SetStorageObject constructor

Reads an input file and converts it into a set storage object
File must of type CSV

Args
	filepath - file to read from
**/
SetStorageObject::SetStorageObject(std::string filepath){
	std::ifstream valFile(filepath);
    std::string line = "";
	int m = 0;
	int n = 0;
	while(getline(valFile , line)){
		std::string word = "";
		int tempn = 0;
		std::stringstream strstr(line);
        while (getline(strstr, word, ',')){
			++tempn;
		}
		if(m == 0){
			n = tempn;
		}
		if(n != tempn){
			std::cerr << "SetStorageObject constructor failed, row length not the same as previous rows @ row " << std::to_string(m+1) << "\n";
			assert(n == tempn);
		}
		++m;
	}
	valFile.close();

	this->internalPoints = Nbody::Points<float>(m , n);

	std::ifstream writeFile(filepath);
	int mi = 0;
	while(getline(writeFile , line)){
		std::string word = "";
		int ni = 0;
		std::stringstream strstr(line);
        while (getline(strstr, word, ',')){
			this->internalPoints(mi , ni) = stof(word);
			++ni;
		}
		++mi;
	}
	writeFile.close();
	this->modified = false;
}

/**
SetStorageObject constructor

Uses input points object

Args
	p - Points object
**/
SetStorageObject::SetStorageObject(Nbody::Points<float> p){
	this->internalPoints = p;
	this->modified = false;
}

/**
SetStorageObject constructor

Uses input data object

Args
	d - Data object
**/
SetStorageObject::SetStorageObject(Nbody::Data d){
	this->modified = false;
	if(d.type().is_float()){
		this->internalPoints = Nbody::Points<float>(d);
		return;
	}
	int m , n;
	Nbody::Points<int> pi;
	Nbody::Points<bool> pb;
	if(d.type().is_bool()){
		pb = Nbody::Points<bool>(d);
	}
	else{
		pi = Nbody::Points<int>(d);
	}
	if(d.type().is_bool()){m = pb.m();}
	else{m = pi.m();}

	if(d.type().is_bool()){n = pb.n();}
	else{n = pi.n();}

	this->internalPoints = Nbody::Points<float>(m , n);

	for (int j = 0; j < m; ++j){
		for (int i = 0; i < n; ++i){
			float val;
			if(d.type().is_bool()){val = (float) pb(j,i);}
			else{val = (float) pi(j,i);}
			internalPoints(j , i) = val;
		}
	}
}

/**
SetStorageObject constructor

Uses input vector of array

Args
	voa - vector_of_array object
**/
SetStorageObject::SetStorageObject(vector_of_array<real_t> voa){
	this->internalPoints = Nbody::Points<float>(voa.size() , voa.array_size());
	for (int j = 0; j < voa.size(); ++j){
		for (int i = 0; i < voa.array_size(); ++i){
			internalPoints(j , i) = (float) voa[j][i];
		}
	}
	this->modified = false;
}

/**
SetStorageObject constructor

Uses vector of floats

Args
	voa - vector of floats
**/
SetStorageObject::SetStorageObject(std::vector<std::vector<float>> v){
	this->internalPoints = Nbody::Points<float>(v.size() , v[0].size());
	for (int j = 0; j < v.size(); ++j){
		for (int i = 0; i < v[0].size(); ++i){
			internalPoints(j , i) = v[j][i];
		}
	}
	this->modified = false;
}

/**
Returns the size of the setStorageObject
**/
int SetStorageObject::size() const{
	return this->internalPoints.m();
}

/**
Returns the stride of the entire object or a point, depending on the dimension

Args
	i - dimension to check (0 or 1)
**/
int SetStorageObject::stride(int i) const{
	return this->internalPoints.stride(i);
}

/**
Returns the size of an individual point
**/
int SetStorageObject::pointSize() const{
	return this->internalPoints.n();
}



/**
Ostream operator for setStorageObject
**/
std::ostream& operator << (std::ostream& outs, const SetStorageObject& store) {
	outs << "[";
	for (int j = 0; j < store.size(); ++j){
		if(j != 0){
			outs << "\n";
		}
		outs << "[";
		for (int i = 0; i < store.pointSize(); ++i){
			if(i != 0){
				outs << ", ";
			}
			outs << store[j][i];
		}
		outs << "]";
	}
	outs << "]";
	return outs;
}


/**
Prints the object
**/
void SetStorageObject::print(){
	std::cout << "[";
	for (int j = 0; j < this->size(); ++j){
		if(j != 0){
			std::cout << "\n";
		}
		std::cout << "[";
		for (int i = 0; i < this->pointSize(); ++i){
			if(i != 0){
				std::cout << ", ";
			}
			std::cout << this[0][j][i];
		}
		std::cout << "]";
	}
	std::cout << "]";
}

void SetStorageObject::toFile(std::string fname){
	std::ofstream fl;
	fl.open (fname);
	fl.precision(17);
	for (int j = 0; j < this->size(); ++j){
		if(j != 0){
			fl << "\n";
		}
		for (int i = 0; i < this->pointSize(); ++i){
			if(i != 0){
				fl << ",";
			}
			fl << std::fixed << this[0][j][i];
		}
	}
	fl.close();
}

/**
Reassigns the setStorageObject with the newSet input

Args
	newSet - Points<float> object
**/
void SetStorageObject::reassign(Nbody::Points<float> newSet){
	this->internalPoints = newSet;
	this->modified = true;
}

/**
Reassigns the setStorageObject with the voa input

Args
	voa - vector_of_array<real_t> object
**/
void SetStorageObject::reassign(vector_of_array<real_t> voa){
	this->internalPoints = Nbody::Points<float>(voa.size() , voa.array_size());
	for (int j = 0; j < this->size(); ++j){
		for (int i = 0; i < this->pointSize(); ++i){
			internalPoints(j , i) = (float) voa[j][i];
		}
	}
	this->modified = true;
}

/**
Returns the internal points object
**/
Nbody::Points<float> SetStorageObject::points() const{
	return this->internalPoints;
}

/**
Returns a vector_of_array representation of the setStorageObject
**/
vector_of_array<real_t> SetStorageObject::getVectorOfArray(){
	vector_of_array<real_t> out(this->size() , this->pointSize());
	for (int j = 0; j < this->size(); ++j){
		for (int i = 0; i < this->pointSize(); ++i){
			out[j][i] = this[0][j][i];
		}
	}
	return out;
}

/**
Equality operator for setStorageObject
**/
bool SetStorageObject::operator==(const SetStorageObject &other) const{
	if(this->size() != other.size()){
		return false;
	}
	if(this->pointSize() != other.pointSize()){
		return false;
	}
	for(int i = 0; i != this->size(); ++i){
		for(int j = 0; j != this->pointSize(); ++j){
			if(this[0][i][j] != other[i][j]){
				return false;
			}
		}
	}
	return true;
}

/**
NEQ operator for setStorageObject
**/
bool SetStorageObject::operator!=(const SetStorageObject &other) const{
	if(this->size() != other.size()){
		return true;
	}
	if(this->pointSize() != other.pointSize()){
		return true;
	}
	for(int i = 0; i != this->size(); ++i){
		for(int j = 0; j != this->pointSize(); ++j){
			if(this[0][i][j] != other[i][j]){
				return true;
			}
		}
	}
	return false;
}

/**
Accessor for setStorageObject
Returns a point, which then can also be accessed using []
**/
const float* SetStorageObject::operator [](const int index) const{
// float* SetStorageObject::operator [](const int index) {
	return &this->internalPoints(index);
}
