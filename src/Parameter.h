#ifndef INC_PARAMETER_H
#define INC_PARAMETER_H

#include <string>
#include "IRNode.h"
#include "Type.h"
#include "Util.h"

/* Internal representation of parameters */

namespace Nbody {

struct ParameterContents {
  mutable RefCount count;
  Type type;
  bool is_scalar;
  std::string name;
  ParameterContents (Type t, const std::string &n) : type(t), name(n) { }
};

class Parameter {
  Ptr<ParameterContents> contents;

public:
  /* Construct a new handle */
  Parameter() : contents(NULL) {}

  /* Constructor for a new parameter of type t  
 * with the given name */
  Parameter(Type t, const std::string &name) : 
    contents(new ParameterContents(t, name)) { }

  /* Get the type of the parameter */
  Type type() const {
    assert (contents.defined());
    return contents.ptr->type;
  }

  /* Get the name of the parameter */
  const std::string &name() const {
    assert (contents.defined());
    return contents.ptr->name;
  }

  /* Test if the handle is not-NULL */
  bool defined() const {
    return contents.defined();
  }
};

template<>
inline RefCount& count<ParameterContents>(const ParameterContents* p) {
  return p->count;
}

/* Free the Parameter memory allocated */
template<>
inline void destroy<ParameterContents>(const ParameterContents* p) {
  delete p;
}
}

#endif
