#ifndef INC_FUNCTION_H
#define INC_FUNCTION_H

#include "Type.h"
#include "Util.h"
#include "Schedule.h"

/** An internal representation of a function */
namespace Nbody {

struct FunctionContents {
  mutable RefCount count;
  std::string name;
  std::vector<std::string> args;
  Expr value;
  Type output_type;
  Schedule schedule;

  std::vector<Expr> rargs;
  Expr rvalue;
  Schedule rschedule;
  ReductionDomain rdom;

  FunctionContents() {}
};

class Function {
  Ptr<FunctionContents> contents;

public:
  /* Construct a new function with no name */
  Function() : contents(new FunctionContents) {}

  /* Add a pure definition to this function */
  void define(const std::vector<std::string>& args, Expr value);

  /* Add a reduction definition to this function. Must already have
  a pure definition */
  void define_reduction(const std::vector<Expr>& args, Expr value);

  /* Construct a new functin with a given name */
  Function(const std::string& n) : contents(new FunctionContents) {
    contents.ptr->name = n;
  }

  /* Get the name of the parameter */
  const std::string &name() const {
    assert (contents.defined());
    return contents.ptr->name;
  }

  /* Get the pure arguments */
  const std::vector<std::string> &args() const {
    assert (contents.defined());
    return contents.ptr->args;
  }

  /* Get the dimensionality */
  size_t dims() const {
    return args().size();
  }

  /* Get the right-hand side of a pure definition */
  Expr value() const {
    assert (contents.defined());
    return contents.ptr->value;
  }

  /* Get the type of the output */
  const Type& output_type() const {
    return contents.ptr->output_type;
  }

  /* Get a modifiable handle to the schedule */
  Schedule &schedule() {
    return contents.ptr->schedule;
  }

  /* Get a read-only handle to the schedule */
  const Schedule &schedule() const {
    return contents.ptr->schedule;
  }

  /* Get the reduction arguments */
  const std::vector<Expr> &reduction_args() const {
    assert (contents.defined());
    return contents.ptr->rargs;
  }

  /* Get the right-hand side of the reduction definition */
  Expr reduction_value() const {
    assert (contents.defined());
    return contents.ptr->rvalue;
  }

  /* Get a mutable handle to the schedule for reduction */
  Schedule &reduction_schedule() {
    return contents.ptr->rschedule;
  }

  /* Get a read-only handle to the schedule for reduction */
  const Schedule &reduction_schedule() const {
    return contents.ptr->rschedule;
  }

  /* Get the reduction domain for the reduction definition */
  ReductionDomain reduction_domain() const {
    return contents.ptr->rdom;
  }

  /* Check if the function has a pure definition */
  bool has_pure_definition() const {
    return value().defined();
  }

  /* Check if the function only has a pure definition */
  bool is_pure() const {
    return (has_pure_definition() && !is_reduction());
  }

  /* Check if the function is a reduction */
  bool is_reduction() const {
    return reduction_value().defined();
  }

  /* Check if two functions are the same */
  bool same_as(const Function& other) const {
    assert (contents.defined());
    return contents.same_as(other.contents);
  }

  /* Test if the handle is not-NULL */
  bool defined() const {
    return contents.defined();
  }
};

template<>
inline RefCount& count<FunctionContents>(const FunctionContents* f) {
  return f->count;
}

template<>
inline void destroy<FunctionContents>(const FunctionContents* f) {
  delete f;
}
}

#endif
