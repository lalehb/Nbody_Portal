#ifndef INC_PARAM_H
#define INC_PARAM_H

#include "IRNode.h"

namespace Nbody {

template<typename T>
class Param {

  /* Handle to internal parameter object */
  Parameter param;

public:
  /* Constructor for a scalar parameter of type T with the
 * given name */
  Param(const std::string& name) : param(type_of<T>(), name) { }

  /* Get the name of the parameter */
  const std::string& name() const {
    return param.name();
  }

  /* Get the type of the parameter */
  Type type() const {
    return type_of<T>();
  }

  /* Parameter can be treated as an expression */
  operator Expr() const {
    return Variable::make(type(), name(), param);
  }

  /* Construct an argument matching this parameter */
  operator Argument() const {
    return Argument(name(), false, type());
  }
};

}

#endif

