#ifndef N_BODY_LOWER_IMPORT
#define N_BODY_LOWER_IMPORT

#include <string>
#include <vector>
#include <utility>
#include <math.h>
#include <map>
#include <tuple>
#include <assert.h>

#include "IRNode.h"
#include "IRPass.h"
#include "StmtCompiler.h"
#include "Type.h"
#include "Argument.h"
#include "buffer_t.h"
#include "Data.h"
#include "JITModule.h"
#include "Lower.h"
#include "Points.h"
#include "NBodyLayer.h"
#include "IROperator.h"


#endif

#ifndef N_BODY_LOWER
#define N_BODY_LOWER

namespace Nbody {

struct Expr;

//class NBodyExpression;

class NBodyLower{
	public:
		NBodyLower();
		~NBodyLower();

		Stmt lower (const std::vector<NBodyLayer> &layerStorage , const std::vector<std::pair<std::string , SetStorageObject>> & additionalStorage); //Change SetStorageObject to pairs

		Stmt makeInitialObject(const std::vector<NBodyLayer> &layerStorage);


		void adjustCounter(const std::vector<NBodyLayer> &layerStorage , int layer ,
			                 int startIndex , int endIndex , int counterIncrement);

		void compile(std::vector<std::pair<std::string , void *>> extFuncts);

		void run();

		Data evaluate();

		Data getOutput();
		void adjustPointApproximateK(int layer , int k);

		Stmt buildStmt(const std::vector<NBodyLayer> &layerStorage , int i , int li ,
			             std::vector<Expr> writeArgs , int writeLevel ,
									 std::vector<std::string> forAccessor , std::vector<std::string> setPointers ,
							     std::vector<std::string> valueFilterStrings , std::vector<std::string> keyFilterStrings ,
									  std::vector<std::string> resultStrings);

		Expr buildCalculation(const std::vector<NBodyLayer> &layerStorage , std::string extFuncName ,
			                    std::vector<std::string> setPointers , int level , Expr result);

		void buildArgumentList(const std::vector<NBodyLayer> &layerStorage ,
			                     const std::vector<std::pair<std::string , SetStorageObject>> & additionalStorage);

		std::vector<Stmt> buildReductionVars(std::string valueFilterString , std::string keyFilterString , NBodyLayer layer);
		std::vector<Stmt> buildReductionFilter(const std::vector<NBodyLayer> &layerStorage,
			                                    int i , std::vector<std::string> valueFilterStrings ,
																					std::string result , std::vector<std::string> forAccessors ,
																					std::vector<std::string> keyFilterStrings,
																				  std::vector<Expr> writeArgs);

		Stmt buildWriteStmts(const std::vector<NBodyLayer> &layerStorage , int i ,
			                   std::vector<Expr> writeArgs , Expr writeValue);

		std::vector<Stmt> buildIRSAssignment(const std::vector<NBodyLayer> &layerStorage ,
			                                   int i , std::vector<std::string> valueFilterStrings ,
																				 std::vector<std::string> keyFilterStrings , std::vector<Expr>  writeArgs);

		Expr getCorrectWriteResult(const std::vector<NBodyLayer> &layerStorage , int i ,
			                         std::vector<std::string> keyFilterStrings , std::vector<std::string> resultStrings);

		Expr flattenArguments(std::vector<Expr> args);

		void updateStorage(int i , const std::vector<NBodyLayer> &layerStorage);
		struct dataQuad{
			Data * first; //Values
			Data * second; //Index (For arg operators)
			Data * third; //Counters
			Data * fourth; //IsReal (Checks if the IRS has been written to for a particular row)
		};

		std::map<std::string , dataQuad> intermediaryResultStorage;

		Stmt pointApproximateLowered;


		StmtCompiler sc;
		Stmt lowered;
		JITModule compiled_module;


		std::vector<Argument> args;
		std::vector<const void*> arg_values;
	private:
		std::map<std::string , std::vector<Expr>> irsArgs;


		std::string outputVarName;

		int dimx;
		int dimy;

		Data * output;

		std::vector<Expr> emptyArgs;
		std::vector<Data*> finalCounter;
		std::vector<int> argumentInputPointStorageLookup;

		std::vector<std::pair<std::string , SetStorageObject>> additionalStorage;
};

}



#endif
