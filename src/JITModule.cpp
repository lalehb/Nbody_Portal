#ifdef _DEBUG
#define DEBUG_MSG(str) do { std::cout << str; } while( false )
#else
#define DEBUG_MSG(str) do { } while ( false )
#endif

#include "Codegen.h"
#include "JITModule.h"



namespace Nbody {

using namespace llvm;



/* Returns the function pointer of a llvm module */
template<typename FPType>
void get_function_pointer(ExecutionEngine* ee, const std::string& fname, FPType* result) {
  assert (ee && "No execution engine defined");

  DEBUG_MSG( "Getting function " << fname << " from module\n");
  llvm::Function* func = ee->FindFunctionNamed(fname.c_str());
  assert (func && "Could not find function in module");

  DEBUG_MSG("Jitting " << fname << '\n');
  void* fp = ee->getPointerToFunction(func);
  assert(fp && "Jitting failed");

  *result = (FPType)fp;
}

void JITModule::compile_module(CodeGenContext* cg, std::unique_ptr<llvm::Module> module, const std::string& function_name) {
  std::string err_string;
  DEBUG_MSG("Inside compile module...\n");
  EngineBuilder engine_builder(std::move(module));

  engine_builder.setErrorStr(&err_string);
  engine_builder.setEngineKind(llvm::EngineKind::JIT);
  engine_builder.setOptLevel(CodeGenOpt::Aggressive);
  ExecutionEngine *ee = engine_builder.create();
 

  if (!ee) {
    std::cerr << "Could not create Execution Engine: " << err_string << '\n';
    assert (false);
  }
  get_function_pointer(ee, function_name, &function);
  DEBUG_MSG( "JIT compiled function pointer 0x" << std::hex << (unsigned long)function << std::dec << '\n');

  get_function_pointer(ee, function_name + "_jit_wrapper", &wrapped_function);
  ee->finalizeObject();
}

void JITModule::compile_module(CodeGenContext* cg, std::unique_ptr<llvm::Module> module, const std::string& function_name , std::vector<std::pair<std::string , void *>> extFuncts , std::map<std::string , llvm::Function*> externFunctions) {
  std::string err_string;
  DEBUG_MSG("Inside compile module...\n");
  
  EngineBuilder engine_builder(std::move(module));
  engine_builder.setErrorStr(&err_string);
  engine_builder.setEngineKind(llvm::EngineKind::JIT);
  engine_builder.setOptLevel(CodeGenOpt::Aggressive);
  ExecutionEngine *ee = engine_builder.create();
  
  
  if (!ee) {
    std::cerr << "Could not create Execution Engine: " << err_string << '\n';
    assert (false);
  }
  
  
  for(int i = 0; i != extFuncts.size(); ++i){
	  if(externFunctions.find(extFuncts[i].first) != externFunctions.end()){
		  ee->addGlobalMapping(externFunctions[extFuncts[i].first], extFuncts[i].second); 
	  }
	  else{
		  DEBUG_MSG("External function " << extFuncts[i].first << " function call doesn't exist.\n");
	  }
  }
  
  
  get_function_pointer(ee, function_name, &function);
  DEBUG_MSG("JIT compiled function pointer 0x" << std::hex << (unsigned long)function << std::dec << '\n');

  get_function_pointer(ee, function_name + "_jit_wrapper", &wrapped_function);
  ee->finalizeObject();

  
  
}


}


