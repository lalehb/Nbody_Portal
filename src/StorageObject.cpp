#include "StorageObject.h"

StorageObject::StorageObject(float v){
	this->val = v;
}

StorageObject::StorageObject(){
	
	
}

float StorageObject::operator()(){
	return this->val;
}

StorageObject& StorageObject::operator [](const int index){
	return *this;
}

float StorageObject::v(){
	return this->val;
}



