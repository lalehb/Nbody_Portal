/**
 *  Defines base class for code generator that uses llvm
 */

#ifndef INC_CODEGEN_H
#define INC_CODEGEN_H


#include <stack>
#include <typeinfo>
#include "LLVM_Headers.h"
#include "IRNode.h"
#include "IRVisitor.h"
#include "IROperator.h"
#include "Argument.h"
#include "Target.h"
#include "Util.h"
#include "JITModule.h"
#include <utility>

namespace llvm {
  class Module;
  class Function;
  class Value;
  class Type;
  class LLVMContext;
  class ExecutionEngine;
  class BasicBlock;
  class raw_fd_ostream;
  class raw_pwrite_stream;
  class IRBuilderDefaultInserter;
  template<typename, typename> class IRBuilder;
}

typedef llvm::raw_pwrite_stream LLVMOStream;

namespace Nbody {

  class CodeGenContext : public IRVisitor {

    public:
      mutable RefCount count;
      CodeGenContext();
	  CodeGenContext(llvm::ExecutionEngine *e);
      virtual ~CodeGenContext();

      /* Compile a Nbody statement to llvm module */
      virtual void compile (Stmt stmt, std::string name, const std::vector<Argument> &args);

      /* Emit llvm bitcode */
      void compile_to_bitcode(const std::string& filename);

      /* Return function pointer to compiled machine code */
      JITModule compile_to_function_pointers();
	  JITModule compile_to_function_pointers(std::vector<std::pair<std::string , void *>> extFuncts);
    protected:
      /* State needed by llvm for code generation */
      std::unique_ptr<llvm::Module> module;
      llvm::Function *function;
      llvm::LLVMContext *context;
	    llvm::ExecutionEngine *engine;
      llvm::IRBuilder<llvm::ConstantFolder, llvm::IRBuilderDefaultInserter> *builder;
      llvm::Value *value;

      /* llvm types */
      llvm::Type *void_ty, *i8, *i32, *f32, *f64;

      llvm::StructType* buffer_t_type;

      /* Initialize the llvm state for the target. */
      void initializeLLVM();

      /* Initialize the CodeGenContext state to compile a new module. */
      void init_module();

      /* Run the selected llvm optimization passes on this module. */
      void optimize_module();

      /* Emit code that evaluates an expression. */
      llvm::Value *codegen (Expr);

      /* Emit code that evaluates a statement */
      void codegen (Stmt);

      /* Construct an llvm output stream for writing to files. */
      std::unique_ptr<llvm::raw_fd_ostream> make_raw_fd_ostream(const std::string &filename);

      /* Add an entry to the symbol table. */
      void sym_push (const std::string &name, llvm::Value *value);

      /* Remove an entry from the symbol table. */
      void sym_pop (const std::string &name);

      /* Check if an element exists in the symbol table. */
      bool sym_exists (const std::string &name);

      /* Return an entry from the symbol table. */
      llvm::Value* sym_get (const std::string &name);

      /* Get the type of buffer_t. */
      llvm::StructType* get_buffer_type();

      /* Take a llvm Value representating a pointer to buffer_t and
       * populate the symbol table with it's memebers. */
      void unpack_buffer (std::string name, llvm::Value* buffer);

      /* Load the value of the members of buffer_t. */
      llvm::Value* buffer_data(llvm::Value* buffer);
      llvm::Value* buffer_begin(llvm::Value* buffer, int i);
      llvm::Value* buffer_end(llvm::Value* buffer, int i);
      llvm::Value* buffer_stride(llvm::Value* buffer, int i);
      llvm::Value* buffer_elem_size(llvm::Value* buffer);

      /* Return a pointer to the appropriate struct element of buffer_t. */
      llvm::Value* buffer_data_ptr(llvm::Value* buffer);
      llvm::Value* buffer_begin_ptr(llvm::Value* buffer, int i);
      llvm::Value* buffer_end_ptr(llvm::Value* buffer, int i);
      llvm::Value* buffer_stride_ptr(llvm::Value* buffer, int i);
      llvm::Value* buffer_elem_size_ptr(llvm::Value* buffer);

      /* Return a pointer into an array at a given index. */
      llvm::Value* codegen_data_ptr(std::string data, Type type, llvm::Value* index);

      /* The name of the function code generated */
      std::string function_name;

      /* Generate code for IR nodes */
      using IRVisitor::visit;

      virtual void visit (const Integer*);
      virtual void visit (const Flt*);
      virtual void visit (const Dbl*);
      virtual void visit (const String*);
      virtual void visit (const Variable*);
      virtual void visit (const Cast*);
      virtual void visit (const Add*);
      virtual void visit (const Sub*);
      virtual void visit (const Mul*);
      virtual void visit (const Div*);
      virtual void visit (const Mod*);
	    virtual void visit (const Min*);
	    virtual void visit (const Max*);
	    virtual void visit (const Select*);
      virtual void visit (const ComparisonOp*);
      virtual void visit (const Call*);
      virtual void visit (const Load*);
      virtual void visit (const Store*);
      virtual void visit (const Vector*);
      virtual void visit (const VectorSet*);
      virtual void visit (const Let*);
      virtual void visit (const LetStmt*);
	    virtual void visit (const Assign*);
      virtual void visit (const For*);
      virtual void visit (const Evaluate*);
      virtual void visit (const Block*);
      virtual void visit (const BlockStmt*);
      virtual void visit (const While*);
      virtual void visit (const OrderedArray_Insert*);


  /* Get the llvm equivalent type */
  llvm::Type *llvm_get_type (Type t);
  llvm::Type *llvm_get_type (llvm::LLVMContext* context, Type t);

private:
  /* Values in scope during code generation. Use
 * sym_push, sym_pop, and sym_get to access. */
  std::map<std::string, llvm::Value*> symbol_table;
  std::map<std::string, llvm::AllocaInst*> NamedValues;
  std::map<std::string , llvm::Function*> externFunctions;

  /* Map for comparison operators */
  enum cop {EQ, NE, LT, GT, LE, GE , AND , OR};
  std::map<std::string, cop> mapcop;

  std::set<std::string> symKey;
  int objAccess;
};

template<>
inline RefCount& count<CodeGenContext>(const CodeGenContext* c) {
  return c->count;
}

template<>
inline void destroy<CodeGenContext>(const CodeGenContext* c) {
  delete c;
}

}

#endif
