#ifndef INC_JITMODULE_H
#define INC_JITMODULE_H

#include <memory>
#include <utility>


#include <llvm/ExecutionEngine/Orc/GlobalMappingLayer.h>

namespace llvm {
class Module;
class ExecutionEngine;
class StringRef;
class Function;
}

namespace Nbody {

class CodeGenContext;

struct JITModule {
  //MappingLayer_t MappingLayer;
  
  
	
  /* Pointer to the raw function */
  void* function;

  /* Wrapper around raw function pointer */
  void (*wrapped_function)(const void**);

  JITModule() : function(NULL), wrapped_function(NULL) {}

  /* Populated the function pointer */
  void compile_module(CodeGenContext* cg, std::unique_ptr<llvm::Module> mod, const std::string& function_name);
  void compile_module(CodeGenContext* cg, std::unique_ptr<llvm::Module> mod, const std::string& function_name , std::vector<std::pair<std::string , void *>> extFuncts , std::map<std::string , llvm::Function*> externFunctions);
  

};

}

#endif

