#ifdef _DEBUG
#define DEBUG_MSG(str) do { std::cout << str; } while( false )
#else
#define DEBUG_MSG(str) do { } while ( false )
#endif

#ifndef INC_IRPASS_H
#define INC_IRPASS_H

#include "IRNode.h"
#include "IRVisitor.h"

/* Defines a base class for passes over the IR to modify it */

namespace Nbody {

class IRPass : public IRVisitor {
protected:
  void visit(const Integer* op)   {DEBUG_MSG("IRPass Int...\n"); expr = op;}
  void visit(const Flt* op)       {DEBUG_MSG("IRPass Flt...\n"); expr = op;}
  void visit(const Dbl* op)       {DEBUG_MSG("IRPass Dbl...\n"); expr = op;}
  void visit(const String* op)    {DEBUG_MSG("IRPass String...\n"); expr = op;}
  void visit(const Variable* op)  {DEBUG_MSG("IRPass Variable..." << op->name << std::endl); expr = op;}

  void visit(const Cast* op) {
    DEBUG_MSG("IRPass Cast...\n");
    Expr value = mutate(op->value);
    if (value.same_as(op->value))
      expr = op;
    else
      expr = Cast::make(op->type, value);
  }

  template<typename T>
  void mutate_binary_operator(IRPass* pass, T* op, Expr* expr) {
    DEBUG_MSG("IRPass BinOp...\n");
    Expr a = mutate(op->a);
    Expr b = mutate(op->b);
    if (a.same_as(op->a) && b.same_as(op->b))
      *expr = op;
    else
      *expr = T::make(a, b);
  }

  void visit(const Add* op) {mutate_binary_operator(this, op, &expr);}
  void visit(const Sub* op) {mutate_binary_operator(this, op, &expr);}
  void visit(const Mul* op) {mutate_binary_operator(this, op, &expr);}
  void visit(const Div* op) {mutate_binary_operator(this, op, &expr);}
  void visit(const Mod* op) {mutate_binary_operator(this, op, &expr);}
  void visit(const Min* op) {mutate_binary_operator(this, op, &expr);}
  void visit(const Max* op) {mutate_binary_operator(this, op, &expr);}

  void visit(const ComparisonOp* op) {
	DEBUG_MSG("IRPass ComparisonOp...\n");
    Expr a = mutate(op->a);
    Expr b = mutate(op->b);
    if (a.same_as(op->a) && b.same_as(op->b))
      expr = op;
    else
      expr = ComparisonOp::make(a , op->op , b);
  }

  void visit(const Call* op) {
    DEBUG_MSG("IRPass Call...\n");
    std::vector<Expr> args(op->args.size());
    bool mod = false;

    for (size_t i = 0; i < op->args.size(); i++) {
      Expr arg = mutate (op->args[i]);
      if (!arg.same_as(op->args[i]))
        mod = true;
      args[i] = arg;
    }

    if (mod)
      expr = Call::make (op->type, op->name, args, op->ctype, op->func, op->points);
    else
      expr = op;
  }

  void visit (const Vector* op) {
    DEBUG_MSG("IRPass Vector...\n");
    Expr base = mutate (op->base);
    Expr stride = mutate (op->stride);
    if (base.same_as(op->base) && stride.same_as(op->stride))
      expr = op;
    else
      expr = Vector::make (base, stride, op->width);
  }

  void visit (const VectorSet* op) {
    DEBUG_MSG("IRPass VectorSet...\n");
    Expr value = mutate (op->value);
    if (value.same_as(op->value))
      expr = op;
    else
      expr = VectorSet::make (value, op->width);
  }

  void visit (const Select* op) {
	  DEBUG_MSG("SELECT STUFF IN PASS \n");
    Expr cond = mutate(op->cond);
    Expr tval = mutate(op->true_value);
    Expr fval = mutate(op->false_value);
	DEBUG_MSG("SELECT STUFF IN PASS TOOK THE" << (cond.same_as(op->cond) && tval.same_as(op->true_value) && fval.same_as(op->false_value)) << "\n");
    if (cond.same_as(op->cond) &&
        tval.same_as(op->true_value) &&
        fval.same_as(op->false_value))
      expr = op;
    else
      expr = Select::make(cond, tval, fval);
  }

  void visit (const Load* op) {
    DEBUG_MSG("IRPass Load...\n");
    Expr index = mutate (op->index);
    if (index.same_as(op->index))
      expr = op;
    else
      expr = Load::make (op->type, op->name, index, op->stride, op->points);
  }

  void visit (const Store* op) {
    DEBUG_MSG("IRPass Store...\n");
    Expr value = mutate(op->value);
    Expr index = mutate(op->index);

    if (value.same_as(op->value) && index.same_as(op->index))
      stmt = op;
    else
      stmt = Store::make(op->name, value, index, op->stride);
  }

  void visit (const MultiStore* op) {
    DEBUG_MSG("IRPass MultiStore...\n");
    std::vector<Expr> args(op->args.size());
    bool mod = false;

    for (size_t i = 0; i < op->args.size(); ++i) {
      Expr arg = mutate(op->args[i]);
      if(!arg.same_as(op->args[i]))
        mod = true;
      args[i] = arg;
    }
    Expr value = mutate(op->value);

    if (!mod && value.same_as(op->value))
      stmt = op;
    else
      stmt = MultiStore::make(op->name, value, args);
  }

  void visit (const For* op) {
    DEBUG_MSG("IRPass For... " << op->ftype << "\n");
    Expr begin = mutate(op->begin);
    Expr end = mutate(op->end);
    Expr stride = mutate(op->stride);
    Stmt body = mutate(op->body);

    if (begin.same_as(op->begin) &&
        end.same_as(op->end) &&
        stride.same_as(op->stride) &&
        body.same_as(op->body))
      stmt = op;
    else
      stmt = For::make (op->name, begin, end, stride, op->ftype, body);
  }


  void visit (const While* op) {
    DEBUG_MSG("IRPass While... \n");
    Expr condition = mutate(op->condition);
    Stmt body = mutate(op->body);

    if (condition.same_as(op->condition) &&
        body.same_as(op->body))
      stmt = op;
    else
      stmt = While::make (op->name, condition, body);
  }

  void visit (const Assign* op) {
    DEBUG_MSG("IRPass Assign..." << op->name << std::endl);
    Expr value = mutate(op->value);
    if (value.same_as(op->value))
      stmt = op;
    else
      stmt = Assign::make (op->name, value);
  }

  void visit (const Let* op) {
    DEBUG_MSG("IRPass Let...\n");
    Expr value = mutate(op->value);
    Expr body = mutate(op->body);
    if (value.same_as(op->value) &&
        body.same_as(op->body))
      expr = op;
    else
      expr = Let::make (op->name, value, body);
  }

  void visit (const LetStmt* op) {
    DEBUG_MSG("IRPass LetStmt..." << op->name << std::endl);
    Expr value = mutate(op->value);
    Stmt body = mutate(op->body);
    if (value.same_as(op->value) &&
        body.same_as(op->body))
      stmt = op;
    else
      stmt = LetStmt::make (op->name, value, body);
  }

  void visit(const Realize* op) {
    DEBUG_MSG("IRPass Realize...\n");
    Region new_bounds(op->bounds.size());
    bool bounds_changed = false;

    // Mutate the bounds
    for (size_t i = 0; i < op->bounds.size(); ++i) {
      Expr old_min = op->bounds[i].min;
      Expr old_max = op->bounds[i].max;
      Expr new_min = mutate(old_min);
      Expr new_max = mutate(old_max);
      if (!new_min.same_as(old_min))
        bounds_changed = true;
      if (!new_max.same_as(old_max))
        bounds_changed = true;
      new_bounds[i] = Range(new_min, new_max);
    }

    Stmt body = mutate(op->body);
    if (!bounds_changed && body.same_as(op->body))
      stmt = op;
    else
      stmt = Realize::make(op->name, op->type, new_bounds, body);
  }

  void visit (const Evaluate* op) {
    DEBUG_MSG("IRPass Evaluate...\n");
    Expr value = mutate(op->value);
    if (value.same_as(op->value))
      stmt = op;
    else
      stmt = Evaluate::make (value);
  }

  void visit (const Block* op) {
    std::vector<Expr> stmts(op->stmts.size());
    bool mod = false;

    for (size_t i = 0; i < op->stmts.size(); i++) {
      Expr s = mutate (op->stmts[i]);
      if (!s.same_as(op->stmts[i]))
        mod = true;
      stmts[i] = s;
    }

    if (mod)
      stmt = Block::make (stmts);
    else
      stmt = op;
  }

  void visit (const BlockStmt* op) {
    std::vector<Stmt> stmts(op->stmts.size());
    bool mod = false;

    for (size_t i = 0; i < op->stmts.size(); i++) {
      Stmt s = mutate (op->stmts[i]);
      if (!s.same_as(op->stmts[i]))
        mod = true;
      stmts[i] = s;
    }

    if (mod)
      stmt = BlockStmt::make (stmts);
    else
      stmt = op;
  }
  void visit (const OrderedArray_Insert* op) {
     DEBUG_MSG("OrderedArray_Insert Evaluate...\n");
     Expr value = mutate(op->value);
     Expr index = mutate(op->index);
       if (value.same_as(op->value) &&
       index.same_as(op->index))
         stmt = op;
       else
         stmt = OrderedArray_Insert::make(op->arrayName ,  op->k , value , index);
  }



  // // void visit (const Transpose* op) {
  // //
  // // }
  // void visit (const Determinant* op) {
  //  // expr = Flt::make(1);
  //  expr = Determinant::make(op->vals);
  //
  // }
  // // void visit (const Inverse* op) {
  // //
  // // }

public:
  Expr mutate(Expr e) {
    e.accept(this);
    return expr;
  }

  Stmt mutate(Stmt s) {
    s.accept(this);
    return stmt;
  }

protected:
  Expr expr;
  Stmt stmt;

};

}

#endif
