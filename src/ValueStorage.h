#include "StorageObject.h"

template <typename T>
class ValueStorage : public StorageObject{
	public:
		/*
		ValueStorage is the generic data storage used within Portal.
		*/
		ValueStorage();
		ValueStorage(T item);

		/*
		Returns the object of type T associated with this object. 
		*/
		T value(); //template <typename T>
	private:
		T storedItem;
};