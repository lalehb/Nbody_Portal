#ifndef INC_REDUCTION_WRAPPER_H
#define INC_REDUCTION_WRAPPER_H

#include "IRNode.h"
#include "IROperator.h"
#include "Reduction.h"

/* Front-end syntax for reduction domain and variables */

namespace Nbody {

/* A reduction variable (RVar) is a single dimension of the reduction
 * domain (RDom). Use to RDom::operator[] to access RVar.
 */
class RVar {
  std::string _name;
  int _index;
  ReductionDomain _domain;

  const ReductionVariable& _var() const {
    return _domain.domain().at(_index);
  }

public:
  /* Construct a reduction variable with an automatically generated name */
  RVar() : _name(generate_name('r')) {}

  /* Construct a reduction variable with the given name */
  RVar(const std::string& n) : _name(n) {}

  /* Construct a reduction variable with the given bounds */
  RVar(ReductionDomain domain, int index) : _domain(domain), _index(index) {}

  /* Minimum value of this reduction variable */
  Expr begin() const {
    if (_domain.defined())
      return _var().begin;
    return Expr();
  }

  /* Maximum value of this reduction variable */
  Expr end() const {
    if (_domain.defined())
      return _var().end;
    return Expr();
  }

  /* Return the domain of this reduction variable */
  ReductionDomain domain() const {
    return _domain;
  }

  /* Return the name of this reduction variable */
  const std::string& name() const {
    if (_domain.defined())
      return _var().name;
    return _name;
  }

  /* Reduction variable as expressions */
  operator Expr() const {
    assert ((begin().defined() || end().defined()) && "Undefined reduction variable");
    return Variable::make(Int(32), name(), domain());
  }
};

template <int N>
ReductionDomain build_domain(ReductionVariable (&vars)[N]) {
  std::vector<ReductionVariable> rv(&vars[0], &vars[N]);
  ReductionDomain dom(rv);
  return dom;
}

/* A multi-dimensional reduction */
class RDom {
  ReductionDomain _dom;

  void init_vars(std::string name) {
    static std::string var_names[] = {"x", "y"};
    const std::vector<ReductionVariable>& rvars = _dom.domain();
    RVar* vars[] = {&x, &y};

    for (size_t i = 0; i < sizeof(vars)/sizeof(vars[0]); ++i) {
      if (i < rvars.size())
        *(vars[i]) = RVar(_dom, i);
      else
        *(vars[i]) = RVar(name + "." + var_names[i]);
    }
  }

public:
  /* Construct a reduction that wraps the reduction domain internal object */
  RDom(ReductionDomain d) : _dom(d) {
    init_vars("");
  }

  /* Construct a 1-D reduction with the given name */
  RDom (Expr begin, Expr end, std::string name = "") {
    if (name == "")
      name = generate_name('r');

    ReductionVariable rvars[] = {
      { name + ".x$r", cast<int>(begin), cast<int>(end) },
    };
    _dom = build_domain(rvars);
    init_vars(name);
  }

  /* Construct a 2-D reduction with the given name */
  RDom (Expr begin0, Expr end0, Expr begin1, Expr end1, std::string name = "") {
    if (name == "")
      name = generate_name('r');

    ReductionVariable rvars[] = {
      { name + ".x$r", cast<int>(begin0), cast<int>(end0) },
      { name + ".y$r", cast<int>(begin1), cast<int>(end1) },
    };
    _dom = build_domain(rvars);
    init_vars(name);
  }

  /* Construct a reduction that iterates over all points in a buffer */
  RDom (Data b) {
    static std::string var_names[] = {"x$r", "y$r"};
    std::vector<ReductionVariable> rvars;
    for (int i = 0; i < b.dims(); ++i) {
      ReductionVariable rv = {b.name() + "." + var_names[i], b.begin(i), b.size(i)};
      rvars.push_back(rv);
    }
    _dom = ReductionDomain(rvars);
    init_vars(b.name());
  }

  /* Return the internal reduction domain object */
  ReductionDomain domain() const { return _dom; }

  /* Check if this reduction is non-NULL */
  bool defined() const { return _dom.defined(); }

  /* Check if two variable names are the same */
  bool same_as(const RDom& other) const {
    return _dom.same_as(other._dom);
  }

  /* Get the dimensionality of the reduction */
  size_t dims() const {
    return _dom.domain().size();
  }

  /* Return the reduction variable i */
  RVar operator[](unsigned int i) const {
    if (i == 0) return x;
    if (i == 1) return y;
    if (i < dims())
      return RVar(_dom, i);
    assert ("Reduction index out of bounds");
  }

  /* 1D reductions can be cast directly into an expression */
  operator Expr() const {
    assert (dims() == 1 && "Can't cast multiD reductions to a reduction variable");
    return Expr(x);
  }

  /* 1D reductions can be cast directly into a reduction variable */
  operator RVar() const {
    assert (dims() == 1 && "Can't cast multiD reductions to a reduction variable");
    return x;
  }

  /* Direct access to the first two dimensions od the reduction */
  RVar x, y;
};

/* Emit RVar in human-readable form */
std::ostream& operator<<(std::ostream& stream, RVar r);

/* Emit RDom in human-readable form */
std::ostream& operator<<(std::ostream& stream, RDom d);
}

#endif
