#ifndef INC_NODE_H
#define INC_NODE_H

#include <iostream>
#include "Data.h"
#include "Variable.h"
#include "IROperator.h"

namespace Nbody {

/* Base class for Points */
class PointsBase {
protected:
  /* The internal memory object */
  Data dataObj;

  void* origin;
  int stride_0, stride_1, dim;

public:
  /* Node default constructor */
  PointsBase() : origin(NULL), stride_0(0), stride_1(0), dim(0) {}

  /* Allocate an object with the given name */
  PointsBase(Type t, int m, int n, const std::string& name = "") :
    dataObj(Data(t, m, n, NULL, name)) {
    if (dataObj.defined()) {
      origin = dataObj.data_ptr();
      stride_0 = dataObj.stride(0);
      stride_1 = dataObj.stride(1);
      dim = dataObj.dims();
    }
    else {
      origin = NULL;
      stride_0 = 0; stride_1 = 0;
      dim = 0;
    }
  }

  /* Wrap the data in a PointsBase object */
  PointsBase(Type t, const Data& d) : dataObj(d) {
    assert(t == dataObj.type() && "Type mismatch between Points and Data");
    if (dataObj.defined()) {
      origin = dataObj.data_ptr();
      stride_0 = dataObj.stride(0);
      stride_1 = dataObj.stride(1);
      dim = dataObj.dims();
    }
    else {
      origin = NULL;
      stride_0 = 0; stride_1 = 0;
      dim = 0;
    }
  }

  /* Check if node points to actual data */
  bool defined() const {
    return dataObj.defined();
  }

  /* Get the number of dimensions of the node */
  int dims() const {
    return dim;
  }

  /* Get the number of elements in each dimension */
  int size(int dim) const {
    return dataObj.size(dim);
  }

  int stride(int dim) const {
    return dataObj.stride(dim);
  }

  int m() const {
    return size(0);
  }

  int n() const {
    return size(1);
  }

  /* Get the name of the node */
  const std::string& name() const {
    return dataObj.name();
  }

  /* Get a pointer to the raw buffer_t that holds this node */
  buffer_t *raw_buffer() const {
    return dataObj.raw_buffer();
  }

  /* Construct an expression which loads from this point. */
  Expr operator()() const {
    std::vector<Expr> args;
    return Call::make(dataObj, args);
  }

  Expr operator()(Expr x) const {
    std::vector<Expr> args;
    args.push_back(x);
    return Call::make(dataObj, args);
  }

  Expr operator()(Expr x, Expr y) const {
    std::vector<Expr> args;
    args.push_back(x);
    args.push_back(y);
    return Call::make(dataObj, args);
  }
};

/* Reference-counted handle on a dense multi-dimensonal array
containing scalar values of type T. May have upto 2 dimensions
*/
template<typename T>
class Points : public PointsBase {
public:
  Points() : PointsBase() {}

  /* Allocate an array with the given dimensions */
  Points(int x, int y, const std::string& name = "") :
    PointsBase(type_of<T>(), x, y, name) {}

  Points(int x, const std::string& name = "") :
    PointsBase(type_of<T>(), x, 0, name) {}

  /* Wrap data in a Points object, so that we can directly
  access its elements */
  Points(const Data& dataObj) : PointsBase(type_of<T>(), dataObj) {}

  /* Get a pointer to the data */
  T* data() const {
    assert(defined() && "Undefined points array");
    return (T*)dataObj.data_ptr();
  }

  using PointsBase::operator();

  /* Get the value of the array at position i */
  const T& operator()(int i) const {
    //return ((T*)origin)[i];
	return ((T*)origin)[i * stride_0];
  }

  /* Get the value of the array at position (i, j) */
  const T& operator()(int i, int j) const {
    //return ((T*)origin)[i + j*stride_1];
	return ((T*)origin)[i*stride_0 + j];
  }

  /* Get the reference of the array at position i */
  T& operator()(int i) {
    //return ((T*)origin)[i];
	return ((T*)origin)[i * stride_0];
  }

  /* Get the reference of the array at position (i, j) */
  T& operator()(int i, int j) {
    //return ((T*)origin)[i + j*stride_1];
	return ((T*)origin)[i*stride_0 + j];
  }


  /* Get a handle on the data this array holds */
  operator Data() const {
    return dataObj;
  }

  /* Convert this array to an argument */
  operator Argument() const {
    return Argument(dataObj);
  }
};

}

#endif
