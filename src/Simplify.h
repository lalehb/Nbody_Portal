#ifndef INC_SIMPLIFY_H
#define INC_SIMPLIFY_H

#include "IRNode.h"
#include "IRPass.h"
#include "IROperator.h"

/* Performs simplification of expressions and statements such as
constant folding */

namespace Nbody {

class Simplify : public IRPass {

  bool const_int(Expr e, int* i) {
    assert(e.defined() && "Undefined expression");
    const Integer* ci = e.ir<Integer>();
    if (ci) {
      *i = ci->value;
      return true;
    }
    return false;
  }

  bool const_float(Expr e, float* f) {
    assert(e.defined() && "Undefined expression");
    const Flt* cf = e.ir<Flt>();
    if (cf) {
      *f = cf->value;
      return true;
    }
    return false;
  }

  bool const_double(Expr e, double* d) {
    assert(e.defined() && "Undefined expression");
    const Dbl* cd = e.ir<Dbl>();
    if (cd) {
      *d = cd->value;
      return true;
    }
    return false;
  }

/*
  void visit(const BinOpBasic* op) {
    std::cerr << "Simplify BinOp... " << op->op << "\n";
    int ilhs = 0, irhs = 0;
    float flhs = 0.0f, frhs = 0.0f;
    double dlhs = 0.0, drhs = 0.0;

    Expr lhs = mutate(op->lhs);
    Expr rhs = mutate(op->rhs);

    const BinOpBasic* binop_lhs = lhs.ir<BinOpBasic>();
    const BinOpBasic* binop_rhs = rhs.ir<BinOpBasic>();

    switch (op->op) {
      case PLUS:
      {
        // Rearrange lhs and rhs when lhs is constant
        if (is_const(lhs) && !is_const(rhs))
          std::swap(lhs, rhs);

        // If both lhs and rhs are constants, fold
        if (const_int(lhs, &ilhs) &&
            const_int(rhs, &irhs)) {
          expr = ilhs + irhs;
        }
        else if (const_float(lhs, &flhs) &&
                const_float(rhs, &frhs)) {
          expr = frhs + frhs;
        }
        else if (const_double(lhs, &dlhs) &&
                const_double(rhs, &drhs)) {
          expr = drhs + drhs;
        }
        else if (is_zero(lhs)) {
          expr = rhs;
        }
        else if (is_zero(rhs)) {
          expr = lhs;
        }
        // In nested associate expressions, pull constants outside
        else if (binop_lhs && is_const(binop_lhs->rhs) && is_const(rhs)) {
          expr = mutate(binop_lhs->lhs + (binop_lhs->rhs + rhs));
        }
        else if (lhs.same_as(op->lhs) && rhs.same_as(op->rhs)) {
          expr = op;
        }
        else {
          expr = BinOpBasic::make(lhs, op->op, rhs);
        }
        break;
      }
      case MINUS:
      case MUL:
      case DIV:
      case REM:
      {
        if (lhs.same_as(op->lhs) && rhs.same_as(op->rhs))
          expr = op;
        else
          expr = BinOpBasic::make(lhs, op->op, rhs);
        break;
      }
    }
  }
  */
};

Expr simplify(Expr e) {
  return Simplify().mutate(e);
}

Stmt simplify(Stmt s) {
  return Simplify().mutate(s);
}

}

#endif
