#ifndef INC_OPERATOR_H
#define INC_OPERATOR_H

#include "IRNode.h"
#include "Clock.hpp"
// #include "Binary_tree.h"
#include "Metric.h"

using namespace std;
namespace Nbody {

class Op {
private:
  std::string op_name;

public:

  /* Construct a variable with an automatically generated name */
  Op() : op_name("Forall") {}

  /* Get the name of the variable */
  const std::string name() const {
      return op_name;
  }

  /* Check if two variable names are the same */
  bool same_as(const Op& other) const {
    return op_name == other.op_name;
  }
  /* checking if a operator is compartive or not, the default is not!*/
  virtual bool is_comparative() = 0;

  virtual bool reverse(real_t a, real_t b) = 0;
  /* overrding the operators for a vector of data points */
  auto  operator() (vector<real_t> data_points) {}

  /* Variable can be treated as an expression of type Int(32) */
 /* operator Expr() {
    return Variable::make(Int(32), name());
  }
 */
};


class Op_Forall : public Op 

{

private:
  std::string op_name = "Forall";
  vector<real_t> result; 
public:
 
  /* check if the operator is compartive or not */
  bool is_comparative() {
    return false;
  }

  auto operator() (vector<real_t> input) {
    for (size_t i = 0; i < input.size() ; i++ ) {
      result[i] = input[i];
    }
  }
  bool reverse(real_t a, real_t b) {
    return false;
  }

};

class Op_Min : public Op 
{
private:
  std::string op_name = "Min";
  real_t result;
public:
  /* In constructor we set teh min value as the max of double for now, but I will change it later  */
  Op_Min() {
    result = DBL_MAX;
  } 
  /* check if the operator is compartive or not */
  bool is_comparative() {
    return true;
  }
  /* override this operator for doing the computation if Op on a vector of data */
  auto operator() (vector<real_t> input) {
    real_t result = input[0];
    for (size_t i = 0; i < input.size() ; i++ ) {
      if (input[i] < result)
        result = input[i];
    }
  }

  bool reverse(real_t a, real_t b) {
    return (a > b);
  }

};


class Op_Max : public Op 
{
private:
  std::string op_name = "Max";
  real_t result;
public:
  Op_Max() {
    result = DBL_MIN;
  }
  bool is_comparative() {
    return true;
  }

  auto operator() (vector<real_t> input) {
    real_t result = input[0];
    for (size_t i = 0; i < input.size() ; i++ ) {
      if (input[i] > result) 
        result = input[i];
    }
  }

  bool reverse(real_t a, real_t b) {
    return (a < b);
  }

};

class Op_Mul : public Op 
{
private:
  std::string op_name = "Mul";
  real_t result;
public:
 
  bool is_comparative() {
    return false;
  }

  auto operator() (vector<real_t> input) {
    real_t result = 1;
    for (size_t i = 0; i < input.size() ; i++ ) {
      result *= input[i];
    }
  }
  bool reverse(real_t a, real_t b) {
    return false;
  }

};


class Op_Sum : public Op 
{
private:
  std::string op_name = "Sum";
  real_t result;  
public:
 
  bool is_comparative() {
    return false;
  }

  auto operator() (vector<real_t> input) {
    real_t result = 0;
    for (size_t i = 0; i < input.size() ; i++ ) { 
        result += input[i];
    }
  }
  bool reverse(real_t a, real_t b) {
    return false;
  }

};


class  Op_ArgMax : public Op 
{
private:
  std::string op_name = "ArgMax";
  real_t result;  
public:
 
  bool is_comparative() {
    return true;
  }

  auto operator() (vector<real_t> input) {
    real_t temp = input[0];
   size_t Arg = 0;
    for (size_t i = 0; i < input.size() ; i++ ) {
      if (input[i] > temp) 
        temp = input[i];
        Arg = i;
    }
    result = Arg;
  }

  bool reverse(real_t a, real_t b) {
    return (a < b);
  }

};


class Op_ArgMin : public Op 
{
private:
  std::string op_name = "ArgMin";
  real_t result;
public:
 
  bool is_comparative() {
    return true;
  }

  auto operator() (vector<real_t> input) {
    real_t temp = input[0];
    size_t Arg  = 0;
    for (size_t i = 0; i < input.size() ; i++ ) {
      if (input[i] > temp) { 
        temp = input[i];
        Arg = i;
      }
    }
    result = Arg;
  }

  bool reverse(real_t a, real_t b) {
    return (a > b);
  }

};

class Op_Union : public Op 
{
private:
  std::string op_name = "Union";
  vector<real_t> result; 
public:
 
  bool is_comparative() {
    return false;
  }

  auto operator() (vector<real_t> input) {
    for (size_t i = 0; i < input.size() ; i++ ) {
      result[i] = input[i];
    }
  }
  bool reverse(real_t a, real_t b) {
    return false;
  }

};

}
#endif
