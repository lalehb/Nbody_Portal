#ifdef _DEBUG
#define DEBUG_MSG(str) do { std::cout << str; } while( false )
#else
#define DEBUG_MSG(str) do { } while ( false )
#endif

#ifndef INC_SCHEDULEFUNCTION_H
#define INC_SCHEDULEFUNCTION_H

#include "IRNode.h"
#include "Variable.h"

namespace Nbody {

/* Inject the allocation and evaluation statements into an
existing loop nest using its schedule */
class InjectStmt : public IRPass {
public:
  const Function& func;
  bool found_store_loc, found_compute_loc;

  InjectStmt(const Function& f) : func(f), found_store_loc(false), found_compute_loc(false) {}

private:

  Stmt build_pipeline(Stmt s) {
    std::vector<Stmt> stmts;
    stmts.push_back(build_produce(func));
    stmts.push_back(build_update(func));

    return BlockStmt::make(stmts);
  }

  Stmt build_realize(Stmt s) {
    Region bounds;
    std::string name = func.name();
    for (size_t i = 0; i < func.dims(); ++i) {
      std::string arg = func.args()[i];
      Expr begin = Variable::make(Int(32), name + "." + arg + ".begin_realized");
      Expr end = Variable::make(Int(32), name + "." + arg + ".end_realized");
      bounds.push_back(Range(begin, end));
    }
    s = Realize::make(name, func.output_type(), bounds, s);
  }

  void visit(const For* op) {
    DEBUG_MSG("InjectStmt of " << func.name() << " over loop " << op->name << "\n");

    const Schedule::Loc& compute_loc = func.schedule().compute_loc;
    const Schedule::Loc& store_loc = func.schedule().store_loc;
    
    Stmt body = op->body;

    // Dig through LetStmts
    std::vector<std::pair<std::string, Expr> > lets;
    while (const LetStmt* let = body.ir<LetStmt>()) {
      lets.push_back(make_pair(let->name, let->value));
      body = let->body;
    }

    body = mutate(body);
  
    if (compute_loc.match(op->name)) { // todo: incomplete
      DEBUG_MSG("Compute location found!\n");
      found_compute_loc = true;
    }

    if (store_loc.match(op->name)) { //todo: incomplete
      DEBUG_MSG("Store location found!\n");
      found_store_loc = true;
    }

    // Re-inject the LetStmt's
    for (size_t i = lets.size(); i > 0; --i) 
      body = LetStmt::make(lets[i-1].first, lets[i-1].second, body);

    if(body.same_as(op->body))
      stmt = op;
    else
      stmt = For::make(op->name, op->begin, op->end, op->stride, op->ftype, body);
  }

  void visit(const MultiStore* op) {
    DEBUG_MSG("Mutating multistore injector...\n");
    DEBUG_MSG(op->name << std::endl << func.name() << std::endl);
    if (op->name != func.name() && !func.is_pure() && func.schedule().compute_loc.do_inline()) {
      stmt = build_realize(build_pipeline(op));
      found_store_loc = found_compute_loc = true;
    }
    else
      stmt = op;
  }

  void visit(const BlockStmt* op) {
    DEBUG_MSG("Mutating BlockStmt injector...\n");
    std::vector<Stmt> stmts(op->stmts.size());
    bool mod = false;

    for (size_t i = 0; i < op->stmts.size(); ++i) {
      Stmt s = mutate (op->stmts[i]);
      if (!s.same_as(op->stmts[i]))
        mod = true;
      stmts[i] = s;
    }

    if (mod)
      stmt = BlockStmt::make(stmts);
    else
      stmt = op;
  }
};

Stmt schedule_function(Stmt s, const std::vector<std::string> &order, const std::map<std::string, Function> &env) {
  /* Create a loop over root to give us a scheduling point */
  std::string root_var = Schedule::Loc::root().func + "." + Schedule::Loc::root().var;
  s = For::make(root_var, 0, 1, 1, For::Serial, s);

  Function f = env.find(order[0])->second;

  if (f.is_pure() && f.schedule().compute_loc.do_inline()) {
    DEBUG_MSG("Inlining function \n");
    s = inline_function(s, f);
  } 
  else {
    DEBUG_MSG("Injecting realization \n");
    InjectStmt injector(f);
    s = injector.mutate(s);
    assert(injector.found_store_loc && injector.found_compute_loc);
  }

  /* Remove the loop over root */
  const For* root_loop = s.ir<For>();
  assert(root_loop);
  return root_loop->body;
}

}

#endif
