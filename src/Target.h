#ifndef INC_TARGET_H
#define INC_TARGET_H

#include <memory>

namespace llvm {
class Module;
class LLVMContext;
class Triple;
}

namespace Nbody {

std::unique_ptr<llvm::Module> get_target_module (llvm::LLVMContext* c);
llvm::Triple get_target_triple();

}

#endif
