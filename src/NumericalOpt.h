#ifndef INC_NUMERICAL_OPT_H
#define INC_NUMERICAL_OPT_H

#include "IRPass.h"


/* Defines a pass that applies the Numerical Optimizations */
namespace Nbody {

class NumericalOpt : public IRPass {

 
   void visit (const Call* op) {

    if ( (op->name == "sqrt_f64" || op->name == "sqrt_f32") ) {
      expr = op->args[0];
    }
   
  }

};

inline Stmt Numerical_Optimizations(Stmt s) {
  return NumericalOpt().mutate(s);
}

}
#endif
