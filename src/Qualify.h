#ifdef _DEBUG
#define DEBUG_MSG(str) do { std::cout << str; } while( false )
#else
#define DEBUG_MSG(str) do { } while ( false )
#endif

#ifndef INC_QUALIFY_H
#define INC_QUALIFY_H

#include "IRNode.h"

/* Prefix names in an expression with some string */

namespace Nbody {

class QualifyExpr : public IRPass {
  const std::string& prefix;
public:
  QualifyExpr(const std::string& p) : prefix(p) {}

private:
  void visit(const Variable* v) {
    if (v->param.defined())
      expr = v;
    else {
      DEBUG_MSG("Variable name in qualify: " << prefix + v->name << std::endl);
      expr = Variable::make(v->type, prefix + v->name, v->rdom);
    }
  }

  void visit(const Let* op) {
    Expr value = mutate(op->value);
    Expr body = mutate(op->body);
    expr = Let::make(prefix + op->name, value, body);
  }
};

Expr qualify(const std::string& prefix, Expr value) {
  QualifyExpr q(prefix);
  return q.mutate(value);
}

}

#endif
