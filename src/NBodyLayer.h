#ifndef N_BODY_LAYER_IMPORT
#define N_BODY_LAYER_IMPORT

#include "StorageObject.h"
#include "SetStorageObject.h"
#include "KeyValueStorage.h"
#include "PortalFunction.h"
#include "PortalOperator.h"
#include "Points.h"
#include <string> 
#include <sstream>
#include <fstream>
#include <utility>
#include <assert.h> 


#endif

#ifndef N_BODY_LAYER
#define N_BODY_LAYER

namespace Nbody {


class NBodyLayer{
	public:
		PortalOperator op;
		SetStorageObject set;
		std::string setAccessor;
		//userFunction func;
		void * userFunct;
		std::string userFunctName;
		std::string pointAccessor;
		//Points<float> p;
		int n;
		
		int startIndex;
		int endIndex;
		PortalFunction func;
		
		
		
		int * finalCounter;

};

}



#endif
