#include <iostream>
#include <vector>
#include <string>
#include <assert.h>
#include "Target.h"
#include "LLVM_Headers.h"

namespace Nbody {

using std::vector;
using std::string;

// #define DECLARE_INITMOD(mod) \
//   extern "C"  unsigned char initmod_##mod[];  \
//   extern "C"  int initmod_##mod##_length;  \
//   std::unique_ptr<llvm::Module> get_initmod_##mod(llvm::LLVMContext* context) {  \
//     llvm::StringRef s = llvm::StringRef((const char*) initmod_##mod, initmod_##mod##_length); \
//     llvm::MemoryBufferRef buf = llvm::MemoryBufferRef(s, #mod);  \
//     auto return_value = llvm::parseBitcodeFile(buf, *context); \
//     std::unique_ptr<llvm::Module> module(std::move(*return_value)); \
//     module->setModuleIdentifier(#mod); \
//     return module; \
//   }



#define DECLARE_INITMOD(mod) \
  extern "C"  unsigned char initmod_##mod[];  \
  extern "C"  int initmod_##mod##_length;  \
  std::unique_ptr<llvm::Module> get_initmod_##mod(llvm::LLVMContext* context) {  \
    llvm::StringRef s = llvm::StringRef((const char*) initmod_##mod, initmod_##mod##_length); \
    llvm::MemoryBufferRef buf = llvm::MemoryBufferRef(s, #mod);  \
    auto return_value = llvm::parseBitcodeFile(buf, *context); \
    std::unique_ptr<llvm::Module> module(std::move(*return_value)); \
    module->setModuleIdentifier(#mod); \
    return module; \
  }


#define INITMOD(mod) \
  DECLARE_INITMOD(mod)

INITMOD(cpu)
INITMOD(math)

/*extern "C"  unsigned char initmod_cpu[];
extern "C"  int initmod_cpu_length;
llvm::Module* get_initmod_cpu(llvm::LLVMContext* context) {
  llvm::StringRef s = llvm::StringRef((const char*) initmod_cpu, initmod_cpu_length);
  llvm::MemoryBuffer* buf = llvm::MemoryBuffer::getMemBuffer(s);
  llvm::Module* module = llvm::ParseBitcodeFile(buf, *context);
  delete buf;
  return module;
}
*/

/* Link all the modules together into m[0] */
void link_modules (vector<std::unique_ptr<llvm::Module>> &m) {
  for (size_t i = 1; i < m.size(); i++) {
    string err;
    bool f = llvm::Linker::linkModules (*m[0], std::move(m[1]));
    if (f) {
      std::cerr << "Failed to link modules: " << err << std::endl;
      assert (false);
    }
  }
}

std::unique_ptr<llvm::Module> get_target_module (llvm::LLVMContext* c) {
  vector<std::unique_ptr<llvm::Module>> m;

  m.push_back(get_initmod_cpu(c));
  m.push_back(get_initmod_math(c));
  link_modules(m);

  return std::move(m[0]);
}

llvm::Triple get_target_triple() {
  llvm::Triple triple;

  triple.setArch(llvm::Triple::x86_64);

  #ifdef __linux__
  triple.setOS(llvm::Triple::Linux);
  triple.setEnvironment(llvm::Triple::GNU);
  #endif
  #ifdef __APPLE__
  triple.setVendor(llvm::Triple::Apple);
  triple.setOS(llvm::Triple::MacOSX);
  #endif
  #ifdef _MSC_VER
  triple.setVendor(llvm::Triple::PC);
  triple.setOS(llvm::Triple::Win32);
  #endif

  return triple;
}

}
