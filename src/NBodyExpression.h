#ifndef N_BODY_EXPRESSION_IMPORT
#define N_BODY_EXPRESSION_IMPORT

#include <functional>
#include <map>
#include <string>
#include <sstream>
#include <fstream>
#include <utility>
#include <assert.h>

#include "StorageObject.h"
#include "SetStorageObject.h"
#include "ValueStorage.h"
#include "KeyValueStorage.h"
#include "PortalFunction.h"
#include "PortalOperator.h"
#include "Lower.h"
#include "IRNode.h"
#include "Data.h"
#include "Points.h"
#include "IRPrinter.h"
#include "StmtCompiler.h"
#include "NBodyLayer.h"
#include "Binary_tree.h"
#include "Dual_tree_traversal_Portal.h"
#include "Multi_tree_traversal_Portal.h"
#include "NBodyLower.h"

#endif

#ifndef N_BODY_EXPRESSION
#define N_BODY_EXPRESSION

namespace Nbody {

#if !defined (USE_FLOAT)
/** \brief Floating-point type for a real number. */
typedef double real_t;
#else
/** \brief Floating-point type for a real number. */
typedef float real_t;
#endif

typedef fixed_vector<real_t> Vec;

class NBodyExpression{
	public:

		struct StoragePair{
			std::string key;
			StorageObject value;
		};

		std::vector<NBodyLayer> layerStorage;
		std::vector<std::pair<std::string , void *>> extFuncts;

		NBodyExpression();
		~NBodyExpression();

		NBodyExpression& addLayer(PortalOperator op , SetStorageObject set);
		NBodyExpression& addLayer(PortalOperator op , SetStorageObject set , PortalFunction funct);
		NBodyExpression& addLayer(PortalOperator op , SetStorageObject set , void * funct);
		NBodyExpression& addLayer(PortalOperator op , std::string inputAccessorString , SetStorageObject set);
		NBodyExpression& addLayer(PortalOperator op , std::string inputAccessorString , SetStorageObject set , void * funct);
		NBodyExpression& addLayer(PortalOperator op , std::string inputAccessorString , SetStorageObject set , PortalFunction funct);
		NBodyExpression& addLayer(PortalOperator op , std::string inputAccessorString , SetStorageObject set , Expr funct);
		NBodyExpression& addLayer(PortalOperator op , Expr setAccessor , SetStorageObject set , Expr funct);

		void attachStorage(std::string name , SetStorageObject obj);
		/*
		Imposes an iteration bound on the generated For loop, so the problem can be run on specific parts of the input set

		layer - decides the layer, where layer 0 is the first layer added
		start - start set index
		end - end set index
		*/

		void adjustSetIterationBounds(int layer , int start , int end);
		void adjustPartitionCounter(int layer , int startIndex , int endIndex , int counterIncrement);
		void insertApproximatePoint(int layer , int startIndex , int endIndex , Vec point);
		void insertApproximatePoint(int targetStartIndex , int targetEndIndex , Vec targetPoint , int sourceStartIndex , int sourceEndIndex , Vec sourcePoint);

		void verifyStructuralValidity();
		bool verifyOutputValidity();

		void execute();
                void executeTraverse();
		void compile_to_lowered_form (const std::string& fname);
		void clearData();
		void compile();
		SetStorageObject getOutput();


		void buildTree();
		template<typename Box> void base_case(Box& , Box&);
		template<typename Box> void base_case(Box& , int);
		double get_temp();
		double get_temp(int a, int b);
		double get_temp(int);
		template<typename Box> double get_temp(Box&);
		double point_calc(Vec& s, Vec& t);

		NBodyLower * lwr;


	private:
		Stmt lowered;
		bool modifiedBody;
		bool modifiedArgs;
		bool loweredExists;

		std::vector<std::pair<std::string , SetStorageObject>> additionalStorage;

		std::vector<std::vector<std::pair<int , int>>> boundStructure;

		void addLayer(PortalOperator op , std::string inputAccessorString  , std::string pointAccessorString , SetStorageObject set ,  PortalFunction funct , void * Extfunct);

		float** saveAndReplaceToPoints(int layer , int startVal , int endVal , Vec pnt);
		void loadToPoints(int layer , int startVal , int endVal , float** saved);

};

}

#endif
