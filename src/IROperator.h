#ifndef INC_IROPERATOR_H
#define INC_IROPERATOR_H

/* Defines operator overloades of the IR nodes */


#include "IRNode.h"

namespace Nbody {

#define PLUS  '+'
#define MINUS '-'
#define MUL '*'
#define DIV '/'
#define REM '%'

/* Cast an expression to type T */
template<typename T>
inline Expr cast (Expr x) {
  return Cast::make (type_of<T>(), x);
}

/* Cast an expression to a new type */
inline Expr cast (Type t, Expr x) {
  assert(x.defined() && "cast of undefined expression");
  return Cast::make(t, x);
}

/* Match the types of two expressions. For eg, if one
 * is a vector type and the other is a scalar type, the
 * scalar is convered into a vector to match types. */
inline void match_types (Expr& a, Expr& b) {
  if (a.type() == b.type())
    return;

  if (a.type().is_scalar() && b.type().is_vector())
    a = VectorSet::make(a, b.type().nelem);
  else if (a.type().is_vector() && b.type().is_scalar())
    b = VectorSet::make(b, a.type().nelem);
  else
    assert (a.type().nelem == b.type().nelem && "Can't type promote two vectors");

  Type ta = a.type();
  Type tb = b.type();

  /* int(a) (op) float(b) -> float(b) */
  if (!ta.is_float() && tb.is_float())
    a = cast(tb, a);
  else if (ta.is_float() && !tb.is_float())
    b = cast(ta, b);
  /* float(a) (op) float(b) -> float(max_bits(a, b)) */
  else if (ta.is_float() && tb.is_float()) {
    if (ta.bits > tb.bits)
      b = cast(ta, b);
    else
      a = cast(tb, a);
  }
  else
    assert (false && "Could not promote types");
}

inline Expr make_const(Type t, int value) {
  if (t == Int(32)) return value;
  if (t == Float(32)) return (float)value;
  if (t == Float(64)) return (double)value;
  if (t.is_vector())
    return VectorSet::make(make_const(t.element_type(), value), t.nelem);
  return Cast::make(t, value);
}

/* Construct a zero expression with the given type t */
inline Expr make_zero (Type t) {
  return make_const(t, 0);
}

inline Expr make_one (Type t) {
  return make_const(t, 1);
}


/** Construct the constant boolean true. May also be a vector of
 * trues, if a width argument is given. */
inline Expr const_true(int w = 1) {
  return make_one(UInt(1, w));
}

/** Construct the constant boolean false. May also be a vector of
 * falses, if a width argument is given. */
inline Expr const_false(int w = 1) {
  return make_zero(UInt(1, w));
}


/* Check if the expression is equal to zero (in all lanes, if it's a
 * vector expression). */
inline bool is_zero (Expr e) {
  if (const Integer* i = e.ir<Integer>())
    return i->value == 0;
  if (const Flt* f = e.ir<Flt>())
    return f->value == 0.0f;
  if (const Dbl* d = e.ir<Dbl>())
    return d->value == 0.0;
  if (const Cast* c = e.ir<Cast>())
    return is_zero(c->value);
  if (const VectorSet* v = e.ir<VectorSet>())
    return is_zero(v->value);
  return false;
}

/* Check if the expression is equal to one (in all lanes, if it's a
 * vector expression). */
inline bool is_one (Expr e) {
  if (const Integer* i = e.ir<Integer>()) {
    //std::cout << "integer... " << i->value <<"\n";
    return i->value == 1;
  }
  if (const Flt* f = e.ir<Flt>()) {
    //std::cout << "float...\n";
    return f->value == 1.0f;
  }
  if (const Dbl* d = e.ir<Dbl>()) {
    //std::cerr << "double... " << d->value <<"\n";
    return d->value == 1.0;
  }
  if (const Cast* c = e.ir<Cast>()) {
    //std::cerr << "cast... \n";
    return is_one(c->value);
  }
  if (const VectorSet* v = e.ir<VectorSet>()) {
    //std::cerr << "vector...\n";
    return is_one(v->value);
  }
  return false;
}

/* Check if the expression is a constant (in all lanes, if it's a
 * vector expression */
inline bool is_const (Expr e) {
  if (e.ir<Integer>()) return true;
  if (e.ir<Flt>()) return true;
  if (e.ir<Dbl>()) return true;
  if (const Cast* c = e.ir<Cast>())
    return is_const(c->value);
  if (const Vector* v = e.ir<Vector>())
    return is_const(v->base) && is_const(v->stride);
  if (const VectorSet* v = e.ir<VectorSet>())
    return is_const(v->value);
  return false;
}

/** If an expression is an Integer, return a pointer to its
 * value. Otherwise returns NULL. */
inline const int *as_const_int(Expr e) {
  const Integer* i = e.ir<Integer>();
  return i ? &(i->value) : NULL;
}

inline bool is_positive_const (Expr e) {
  if (const Integer* i = e.ir<Integer>()) return i->value > 0;
  if (const Flt* f = e.ir<Flt>()) return f->value > 0.0f;
  if (const Dbl* d = e.ir<Dbl>()) return d->value > 0.0;
  if (const Cast* c = e.ir<Cast>())
    return is_positive_const(c->value);
  if (const Vector* v = e.ir<Vector>())
    return is_positive_const(v->base) && is_positive_const(v->stride);
  if (const VectorSet* v = e.ir<VectorSet>())
    return is_positive_const(v->value);
  return false;
}

inline bool is_negative_const (Expr e) {
  if (const Integer* i = e.ir<Integer>()) return i->value < 0;
  if (const Flt* f = e.ir<Flt>()) return f->value < 0.0f;
  if (const Dbl* d = e.ir<Dbl>()) return d->value < 0.0;
  if (const Cast* c = e.ir<Cast>())
    return is_negative_const(c->value);
  if (const Vector* v = e.ir<Vector>())
    return is_negative_const(v->base) && is_negative_const(v->stride);
  if (const VectorSet* v = e.ir<VectorSet>())
    return is_negative_const(v->value);
  return false;
}



/* Return the sum of two expressions */
inline Expr operator+(Expr a, Expr b) {
  assert (a.defined() && b.defined() && "operator '+' undefined");
  match_types(a, b);
  return Add::make(a, b);
}

/* Modify and return Expr a as the sum of two expressions */
inline Expr& operator+=(Expr& a, Expr b) {
  assert (a.defined() && b.defined() && "operator '+' undefined");
  a = Add::make(a, b);
  return a;
}

/* Return the difference of two expressions */
inline Expr operator-(Expr a, Expr b) {
  assert (a.defined() && b.defined() && "operator '-' undefined");
  match_types(a, b);
  return Sub::make(a, b);
}

/* Return the negative of the argument */
inline Expr operator-(Expr a) {
  assert (a.defined() && "operator '-' of undefined Expr");
  return Sub::make(make_zero(a.type()), a);
}

/* Return the product of two expressions */
inline Expr operator*(Expr a, Expr b) {
  assert (a.defined() && b.defined() && "operator '*' undefined");
  match_types(a, b);
  return Mul::make(a, b);
}

/* Return the ratio of two expressions */
inline Expr operator/(Expr a, Expr b) {
  assert (a.defined() && b.defined() && "operator '/' undefined");
  match_types(a, b);
  if (is_zero(b))
    return a;
  else
    return Div::make(a, b);
}

/* Return the mod of two expressions */
inline Expr operator%(Expr a, Expr b) {
  assert (a.defined() && b.defined() && "operator '%' undefined");
  match_types(a, b);
  return Mod::make(a, b);
}

/* Tests whether two expressions are equal and returns a boolean
 * result */
inline Expr operator==(Expr a, Expr b) {
  assert (a.defined() && b.defined() && "operator '==' undefined");
  match_types(a, b);
  return ComparisonOp::make(a, "==", b);
}

/* Tests whether two expressions are not equal and returns a
 * boolean result */
inline Expr operator!=(Expr a, Expr b) {
  assert (a.defined() && b.defined() && "operator '!=' undefined");
  match_types(a, b);

  return ComparisonOp::make(a, "!=", b);
}


/* Tests whether the first expression is greater than the second
 * expression and returns a boolean result */
inline Expr operator>(Expr a, Expr b) {
  assert (a.defined() && b.defined() && "operator '>' undefined");
  match_types(a, b);
  return ComparisonOp::make(a, ">", b);
}

/* Tests whether the first expression is less than the second
 * expression and returns a boolean result */
inline Expr operator<(Expr a, Expr b) {
  // assert (a.defined() && b.defined() && "operator '<' undefined");
  assert (a.defined() && "operator '<' undefined");
  assert ( b.defined() && "operator '<' undefined");
  match_types(a, b);
  return ComparisonOp::make(a, "<", b);
}

/* Tests whether the first expression is greater than or equal to
 * the second expression and returns a boolean result */
inline Expr operator>=(Expr a, Expr b) {
  assert (a.defined() && b.defined() && "operator '>=' undefined");
  match_types(a, b);
  return ComparisonOp::make(a, ">=", b);
}

/* Tests whether the first expression is less than or equal to
 * the second expression and returns a boolean result */
inline Expr operator<=(Expr a, Expr b) {
  assert (a.defined() && b.defined() && "operator '<=' undefined");
  match_types(a, b);
  return ComparisonOp::make(a, "<=", b);
}

/* Returns the logical and of two arguments */
inline Expr operator&&(Expr a, Expr b) {
  assert (a.defined() && b.defined() && "operator '&&' of undefined Expr");
  match_types(a, b);
  return ComparisonOp::make(a, "&&", b);
}

/* Returns the logical or of two arguments */
inline Expr operator||(Expr a, Expr b) {
  assert (a.defined() && b.defined() && "operator '||' of undefined Expr");
  match_types(a, b);
  return ComparisonOp::make(a, "||", b);
}

/* Returns the logical not of an argument */
inline Expr operator!(Expr a) {
  assert (a.defined() && "operator '!' of undefined Expr");
  return ComparisonOp::make(a, "!");
}

/* Returns min of the two arguments */
inline Expr min(Expr a, Expr b) {
  assert (a.defined() && b.defined() && "min of undefined Expr");
  match_types(a, b);
  return Min::make(a, b);
}

/* Returns max of the two arguments */
inline Expr max(Expr a, Expr b) {
  assert (a.defined() && b.defined() && "max of undefined Expr");
  match_types(a, b);
  return Max::make(a, b);
}

/* Power node - one floating point expression raised to
 * the power of another. The type of the result is given
 * by the type of the first argument. */
inline Expr pow (Expr x, Expr y) {
  assert (x.defined() && y.defined() && "pow() undefined");

  // Raise to the power of a floating point type
  if (x.type() == Float(32)) {
    y = cast<float>(y);
    return Call::make (Float(32), "pow_f32", vec(x,y), Call::Extern);
  }
  else if (x.type() == Float(64)) {
    y = cast<double>(y);
    return Call::make (Float(64), "pow_f64", vec(x,y), Call::Extern);
  }
  else {
    x = cast<float>(x);
    y = cast<float>(y);
    return Call::make (Float(32), "pow_f32", vec(x,y), Call::Extern);
  }
}

inline Expr pow(int x , int y){
	return pow(Flt::make(x) , Flt::make(y));
}

inline Expr pow(Expr x , float y){
  assert (x.defined() && "pow() undefined");
	return pow(x , Flt::make(y));
}

inline Expr pow(Expr x , double y){
  assert (x.defined() && "pow() undefined");
	return pow(x , Flt::make(y));
}

inline Expr exp(Expr x){
  assert (x.defined() && "pow() undefined");
	return pow(Flt::make(2.71), x);
}

inline Expr pow(Expr x , int y){
  assert (x.defined() && "pow() undefined");
	int threshold = 5; //Decide when to switch between unrolling pow to series of muls to using ext power function
	if(y > threshold){
		return pow(x , Flt::make(y));
	}
	if(y == 0){
		return Flt::make(1);
	}
	Expr out = x;
	int to = y;
	if(to < 0) to = -to;
	for(int i = 1; i != to; ++i){
		out = out * x;
	}
	if(y < 0) out = Flt::make(1)/out;
	return out;

}

/* Square root node - computes square root of a floating
 * point expression */
inline Expr sqrt (Expr x) {
  assert (x.defined() && "sqrt() undefined");

  if (x.type() == Float(32))
    return Call::make (Float(32), "sqrt_f32", vec(x), Call::Extern);
  else if (x.type() == Float(64))
    return Call::make (Float(64), "sqrt_f64", vec(x), Call::Extern);
  else
    return Call::make (Float(32), "sqrt_f32", vec(cast<float>(x)), Call::Extern);
}

/* Reciprocal square root node - computes the reciprocal square
 * root of a floating point expression */
inline Expr rsqrt (Expr x) {
  assert (x.defined() && "rsqrt() undefined");
  return Div::make (Expr(1.0), sqrt(x));
}

inline Expr Det(Expr x) {
  assert (x.defined() && "Det() undefined");
  int dim = 2;
  std::vector<Expr> d;
  d.push_back(x);
  // d.push_back(Load::make(Float(32) , "covariance" , x , true , dim));
  d.push_back(Integer::make(dim));
  return Call::make(Float(32), "Determinant", d, Call::Extern);
}

inline Expr Inv(Expr x, Expr y) {
  assert (x.defined() && "Inv() undefined");
  int dim = 2;
  std::vector<Expr> d;
  d.push_back(x);
  d.push_back(y);

  d.push_back(Integer::make(dim));
  return Call::make(Float(32), "Inverse", d, Call::Extern);
}

inline Expr Transp(Expr x, Expr y) {
  assert (x.defined() && "Transp() undefined");
  int dim = 2;
  std::vector<Expr> d;
  d.push_back(x);
  d.push_back(y);

  d.push_back(Integer::make(dim));
  return Call::make(Float(32), "Transpose", d, Call::Extern);
}


inline Expr MatrixMul(Expr x, Expr y,  Expr z) {
  assert (x.defined() && y.defined() && "MatrixMul of undefined Expr");
  int dim = 2;

  std::vector<Expr> d;
  d.push_back(x);
  d.push_back(y);
  d.push_back(z);

  d.push_back(Integer::make(dim));
  d.push_back(Integer::make(dim));
  d.push_back(Integer::make(dim));
  return Call::make(Float(32), "MatrixMul2", d, Call::Extern);


}

inline Expr MatrixMul(Expr x, Expr y, Expr z,  Expr w) {
  assert (x.defined() && y.defined() &&  z.defined() && "MatrixMul of undefined Expr");
  int dim = 2;
  std::vector<Expr> d;
  d.push_back(x);
  d.push_back(y);
  d.push_back(z);
  d.push_back(w);


  d.push_back(Integer::make(dim));
  d.push_back(Integer::make(dim));
  d.push_back(Integer::make(dim));
  return Call::make(Float(32), "MatrixMul3", d, Call::Extern);
}

inline Expr Sum (Expr a) {


}

/* Return the greatest whole number less than or equal to a
 * floating-point expression. If the argument is not floating-point,
 * it is cast to Float(32). */
inline Expr floor (Expr x) {
    assert(x.defined() && "floor() undefined");
    if (x.type() == Float(32))
        return Call::make(Float(32), "floor_f32", vec(x), Call::Extern);
    else if (x.type() == Float(64))
        return Call::make(Float(64), "floor_f64", vec(x), Call::Extern);
    else
        return Call::make(Float(32), "floor_f32", vec(cast<float>(x)), Call::Extern);
}

/** Return the least whole number greater than or equal to a
 * floating-point expression. If the argument is not floating-point,
 * it is cast to Float(32). */
inline Expr ceil (Expr x) {
    assert(x.defined() && "ceil() undefined");
    if (x.type() == Float(32))
        return Call::make(Float(32), "ceil_f32", vec(x), Call::Extern);
    else if (x.type() == Float(64))
        return Call::make(Float(64), "ceil_f64", vec(x), Call::Extern);
    else
        return Call::make(Float(32), "ceil_f32", vec(cast<float>(x)), Call::Extern);
}


/** Returns an expression similar to the ternary operator in C, except
 * that it always evaluates all arguments. If the first argument is
 * true, then return the second, else return the third. Typically
 * vectorizes cleanly, but benefits from SSE41 or newer on x86. */
inline Expr select(Expr condition, Expr true_value, Expr false_value) {
  assert (condition.defined() && true_value.defined() &&  false_value.defined() && "Select() of undefined Expr");
  if (as_const_int(condition)) {
    // Why are you doing this? We'll preserve the select node until constant folding for you.
    condition = cast(Bool(), condition);
  }
  // Coerce int literals to the type of the other argument
  if (as_const_int(true_value)) {
    true_value = cast(false_value.type(), true_value);
  }
  if (as_const_int(false_value)) {
    false_value = cast(true_value.type(), false_value);
  }
  return Select::make(condition, true_value, false_value);
}

/** A multi-way variant of select similar to a switch statement in C,
 * which can accept multiple conditions and values in pairs. Evaluates
 * to the first value for which the condition is true. Returns the
 * final value if all conditions are false.
 */
// @{
inline Expr select(Expr c1, Expr v1,
                   Expr c2, Expr v2,
                   Expr default_val) {
    assert (c1.defined() && v1.defined() &&
            c2.defined() && v2.defined() && default_val.defined() && "Select() of undefined Expr");
    return select(c1, v1,
                  select(c2, v2, default_val));
}

/* Returns the absolute value

This didn't work! The current work around is doing a select
*/
inline Expr abs(Expr a) {
  assert(a.defined() && "abs of undefined Expr");
  Type t = a.type();
  if (t.is_int())
    t.t == Type::UInt;
//return Call::make(t, Call::abs, vec(a), Call::Intrinsic);
  return select(a > -a , a , -a);
}

}
#endif
