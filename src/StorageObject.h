#ifndef STORAGE_OBJECT
#define STORAGE_OBJECT

#include <iostream>

class StorageObject{
	public:
		/*
		StorageObject is the generic data storage used within Portal.
		*/
		StorageObject();
		StorageObject(float v);
		
		virtual StorageObject & operator [] (int index);
		
		float operator()();
		
		float v();
	private:
		float val;
};
#endif



