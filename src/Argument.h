#ifndef INC_ARGUMENT_H
#define INC_ARGUMENT_H

#include "Type.h"

namespace Nbody {

struct Argument {
  /* Name of the argument passed */
  std::string name;

  /* Boolean which says whether the argument
 * is a data pointer or scalar type
 */
  bool is_ptr;

  /* Type of the argument if it's scalar */
  Type type;

  Argument() : is_ptr(false) {}
  Argument(const std::string& name_, bool is_ptr_, Type type_) :
    name(name_), is_ptr(is_ptr_), type(type_) {}
};

}

#endif
