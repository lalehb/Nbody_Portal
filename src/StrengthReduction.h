#ifndef INC_STRENGTH_REDUCTION_H
#define INC_STRENGTH_REDUCTION_H

#include "IRPass.h"


/* Defines a pass that does Newton-Raphson optimization (essentially
a form of strength reduction) */
namespace Nbody {

class StrengthReduction : public IRPass {

  void visit (const Mod* op) {
    Expr a = mutate(op->a);
    Expr b = mutate(op->b);
      
    if (a.same_as(op->a) && b.same_as(op->b))
      expr = op;
    else
      expr = Mod::make(a, b);
  }
  
  void visit (const Div* op) {
    //std::cerr << "StrengthReduction of Div...\n";
    Expr a = mutate(op->a);
    Expr b = mutate(op->b);

    /* Reciprocal square-root */
    if (is_one(a) && op->type == Float(64, 2)) {
      /* Newton-Raphson square-root algorithm */
      const Call* c = b.ir<Call>();
      Type s = Float(32, op->type.nelem * 2);
      Expr d2s = Call::make (s, "sse2.cvtpd2ps", c->args, Call::Intrinsic);
      Expr r = Call::make (s, "sse.rsqrt.ps", vec(d2s), Call::Intrinsic);
      Expr s2d = Call::make (op->type, "sse2.cvtps2pd", vec(r), Call::Intrinsic);
      expr = (1.5 - (s2d * s2d * (0.5 * c->args[0]))) * s2d;
    }
    else if (is_one(a) && op->type == Float(32, 4)) {
      const Call* c = b.ir<Call>();
      expr = Call::make (op->type, "sse.rsqrt.ps", c->args, Call::Intrinsic) ;
    }
    else {
      if (a.same_as(op->a) && b.same_as(op->b))
        expr = op;
      else
        expr = Div::make(a, b);
    }
  }
};

inline Stmt strength_reduction(Stmt s) {
  return StrengthReduction().mutate(s);
}

}
#endif
