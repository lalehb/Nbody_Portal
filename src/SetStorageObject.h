#ifndef SET_STORAGE_OBJECT
#define SET_STORAGE_OBJECT


#include "Nbody.h"
#include "StorageObject.h"
#include "Points.h"


#include <iostream>
#include <vector>
#include <iostream>
#include <sstream>
#include <string>
#include <fstream>
#include <vector>
#include <cfloat>
#include <assert.h>

#include "utils.h"
#include "vector_of_array.hpp"

#if !defined (USE_FLOAT)
/** \brief Floating-point type for a real number. */
typedef double real_t;
#else
/** \brief Floating-point type for a real number. */
typedef float real_t;
#endif


class SetStorageObject{
	public:
		/*
		SetStorageObject is the set data storage used within Portal.

		It can be used to load data from external sources into the program and to save data from the nBodyExpression object.
		Arguments
			filepath - path to where file can be found
		*/
		SetStorageObject();
		SetStorageObject(std::string filepath);
		SetStorageObject(Nbody::Points<float> p);
		SetStorageObject(Nbody::Data d);
		SetStorageObject(vector_of_array<real_t> voa);
		SetStorageObject(std::vector<std::vector<float>> v);


		// float * operator [] (int index);
		const float * operator [] (int index) const;
		//const float * operator()(int index) const;
		bool operator==(const SetStorageObject &other) const;
		bool operator!=(const SetStorageObject &other) const;
		friend std::ostream& operator << (std::ostream& outs, const SetStorageObject& store);



		int size() const;
		int pointSize() const;
		int stride(int i) const;
		Nbody::Points<float> points() const;
		void reassign(Nbody::Points<float> newSet);
		void reassign(vector_of_array<real_t> voa);
		vector_of_array<real_t> getVectorOfArray();
		void print();

		void toFile(std::string fname);

		bool modified;
	private:

		Nbody::Points<float> internalPoints;





};

class sso : public SetStorageObject{};
#endif
