#ifndef INC_IRVISITOR_H
#define INC_IRVISITOR_H

#include <iostream>
#include <set>

namespace Nbody {
struct Expr;
struct Stmt;
struct IRNode;
struct Integer;
struct Flt;
struct Dbl;
struct String;
struct Cast;
struct Variable;
struct Add;
struct Sub;
struct Mul;
struct Div;
struct Mod;
struct Min;
struct Max;
struct ComparisonOp;
struct Call;
struct Vector;
struct VectorSet;
struct Assign;
struct Let;
struct LetStmt;
struct Select;
struct Load;
struct Store;
struct MultiStore;
struct For;
struct Realize;
struct Evaluate;
struct Block;
struct BlockStmt;
struct SSO;
struct While;
struct OrderedArray_Insert;
struct Determinant;


class IRVisitor {
public:
  virtual ~IRVisitor();
  virtual void visit (const Integer*);
  virtual void visit (const Flt*);
  virtual void visit (const Dbl*);
  virtual void visit (const String*);
  virtual void visit (const Cast*);
  virtual void visit (const Variable*);
  virtual void visit (const Add*);
  virtual void visit (const Sub*);
  virtual void visit (const Mul*);
  virtual void visit (const Div*);
  virtual void visit (const Mod*);
  virtual void visit (const Min*);
  virtual void visit (const Max*);
  virtual void visit (const ComparisonOp*);
  virtual void visit (const Call*);
  virtual void visit (const Vector*);
  virtual void visit (const VectorSet*);
  virtual void visit (const Assign*);
  virtual void visit (const Let* );
  virtual void visit (const LetStmt* );
  virtual void visit (const Select* );
  virtual void visit (const Load*);
  virtual void visit (const Store*);
  virtual void visit (const MultiStore*);
  virtual void visit (const For*);
  virtual void visit (const Realize*);
  virtual void visit (const Evaluate*);
  virtual void visit (const Block*);
  virtual void visit (const BlockStmt*);
  virtual void visit (const SSO*);
  virtual void visit (const While*);
  virtual void visit (const OrderedArray_Insert*);
  virtual void visit(const Determinant*);
};

/* A base class for algorithms that recursively walk over the IR without re-visiting nodes.
This is for passes capable of interpreting the IR as a DAG instead of a tree */
class IRGraphVisitor : public IRVisitor {
protected:
  /* By default, these two methods add the node to the visited set. */
  virtual void include (const Expr&);
  virtual void include (const Stmt&);

  /* The nodes visited so far */
  std::set<const IRNode*> visited;

public:
  /* These methods should call 'include' only if they haven't already been visited */
  virtual void visit (const Integer*);
  virtual void visit (const Flt*);
  virtual void visit (const Dbl*);
  virtual void visit (const String*);
  virtual void visit (const Cast*);
  virtual void visit (const Variable*);
  virtual void visit (const Add*);
  virtual void visit (const Sub*);
  virtual void visit (const Mul*);
  virtual void visit (const Div*);
  virtual void visit (const Mod*);
  virtual void visit (const Min*);
  virtual void visit (const Max*);
  virtual void visit (const ComparisonOp*);
  virtual void visit (const Call*);
  virtual void visit (const Vector*);
  virtual void visit (const VectorSet*);
  virtual void visit (const Assign*);
  virtual void visit (const Let*);
  virtual void visit (const LetStmt*);
  virtual void visit (const Select*);
  virtual void visit (const Load*);
  virtual void visit (const Store*);
  virtual void visit (const MultiStore*);
  virtual void visit (const For*);
  virtual void visit (const Realize*);
  virtual void visit (const Evaluate*);
  virtual void visit (const Block*);
  virtual void visit (const BlockStmt*);
  virtual void visit (const While*);
  virtual void visit (const OrderedArray_Insert*);
  virtual void visit (const SSO*);
  virtual void visit(const Determinant*);
};
}

#endif
