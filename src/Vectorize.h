#ifdef _DEBUG
#define DEBUG_MSG(str) do { std::cout << str; } while( false )
#else
#define DEBUG_MSG(str) do { } while ( false )
#endif


#ifndef INC_VECTORIZE_H
#define INC_VECTORIZE_H

#include "IRPass.h"


/* Defines a pass that vectorizes loops */
namespace Nbody {

class Vectorization : public IRPass {

  class Substitute : public IRPass {
    std::string var;
    int width;
    std::vector<std::string> scope;

    /* Extend the expression e to contain n elements */
    Expr extend (Expr e, int n) {
      if (e.type().nelem == n)
        return e;
      else if (e.type().nelem == 1) {
        DEBUG_MSG("Vectorize VectorSet...\n");
        return VectorSet::make (e, n);
      }
      else
        assert (false && "Mismatched vector length");
    }

    void visit (const Cast* op) {
      DEBUG_MSG("Vectorize Cast...\n");
      Expr value = mutate(op->value);
      if (value.same_as(op->value))
        expr = op;
      else {
        Type t = op->type.make_vector(value.type().nelem);
        expr = Cast::make(t, value);
      }
    }

    void visit (const Variable* op) {
    DEBUG_MSG("Vectorize Variable...\n");
      if (op->name == var)
        expr = Vector::make(op, 1, width);
      else
        expr = op;
    }

    template<typename T>
    void mutate_binary_operator(const T* op) {
      DEBUG_MSG("Vectorize BinOp...\n");
      Expr a = mutate(op->a);
      Expr b = mutate(op->b);

      if (a.same_as(op->a) && b.same_as(op->b))
        expr = op;
      else {
        int n = std::max(a.type().nelem, b.type().nelem);
        expr = T::make (extend(a, n), extend(b, n));
      }
    }

    void visit (const Add* op) {mutate_binary_operator(op);}
    void visit (const Sub* op) {mutate_binary_operator(op);}
    void visit (const Mul* op) {mutate_binary_operator(op);}
    void visit (const Div* op) {mutate_binary_operator(op);}
    void visit (const Mod* op) {mutate_binary_operator(op);}

    void visit (const Load* op) {
    DEBUG_MSG("Vectorize Load...\n");
      Expr index = mutate(op->index);

      if (std::find(scope.begin(), scope.end(), op->name) != scope.end())
        index = extend(index, width);

      if (index.same_as(op->index))
        expr = op;
      else
        expr = Load::make (op->type.make_vector(width), op->name, index, op->stride, op->points);
    }

    void visit (const Call* op) {
    DEBUG_MSG("Vectorize Call...\n");
      std::vector<Expr> args(op->args.size());
      int n = 0;
      std::string name;
      bool mod = false;

      for (size_t i = 0; i < op->args.size(); i++) {
        Expr arg = mutate (op->args[i]);
        if (!arg.same_as(op->args[i]))
          mod = true;
        args[i] = arg;
        n = std::max(arg.type().nelem, n);
      }

      if (mod) {
        for (size_t i = 0; i < args.size(); i++)
          args[i] = extend(args[i], n);

        Type t = op->type.make_vector(n);
        if (op->name == "sqrt_f64" && t == Float(64, 2))
          name = "avx.sqrt.pd";
        else if (op->name == "sqrt_f32" && t == Float(32, 4))
          name = "avx.sqrt.ps";
        else
          name = op->name;
        expr = Call::make (t, name, args, Call::Intrinsic);
      }
      else
        expr = op;
    }

    void visit (const Store* op) {
    DEBUG_MSG("Vectorize Store...\n");
      Expr value = mutate(op->value);
      Expr index = mutate(op->index);

      if (find(scope.begin(), scope.end(), op->name) != scope.end())
        index = extend(index, width);

      if (value.same_as(op->value) && index.same_as(op->index))
        stmt = op;
      else {
        int n = std::max(value.type().nelem, index.type().nelem);
        stmt = Store::make(op->name, extend(value, n), extend(index, n), op->stride);
      }
    }

  public:
    Substitute(std::string v, int n) : var(v), width(n) {}
  };

  void visit (const For* op) {
    DEBUG_MSG("Vectorize For... " << op->ftype << "\n");
    if (op->ftype == For::Vectorized) {
      const Integer *stride = op->stride.ir<Integer>();
      Stmt body = Substitute(op->name, stride->value).mutate(op->body);
      stmt = For::make (op->name, op->begin, op->end, op->stride, For::Serial, body);
    }
    else
      IRPass::visit(op);
  }
};

inline Stmt vectorize(Stmt s) {
  return Vectorization().mutate(s);
}

}
#endif
