#include "ReductionWrapper.h"

namespace Nbody {

/* Emit RVar in human-readable form */
std::ostream& operator<<(std::ostream& stream, RVar r) {
  stream << r.name(); // << "(" << r.begin() << ", " << r.end() << ")";
  return stream;
}

/* Emit RDom in human-readable form */
std::ostream& operator<<(std::ostream& stream, RDom d) {
  stream << "RDom(\n";
  for (int i = 0; i < d.dims(); ++i)
    stream << " " << d[i] << "\n";
  stream << ")\n";
  return stream;
}

}
