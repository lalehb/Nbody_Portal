#ifndef _SINGLE_TREE_TRAVERSAL_H_
#define _SINGLE_TREE_TRAVERSAL_H_
#define counter_type unsigned long long


#ifdef _DEBUG_PAR
#define DEBUG_CMD(cmd) do { cmd} while( false )
#else
#define DEBUG_CMD(cmd) do { } while ( false )
#endif

#include <ratio>
#include <numeric>
#include <chrono>
#include <cfloat>
#include <vector>
#include <omp.h>
#include "Clock.hpp"
#include <time.h>

/*
* the flag _DEBUG_NO_REDUC is used for the algorithms such as EM which the operator can be
* parallelized using the inner tree too, for these group of algorithm since the calculation is a forall
* we can parallelized over any of trees, Note to add the flag -D _DEBUG_NO_REDUC in compiling these
* group of problems while using the single tree traversal
*/
template <typename Tree, typename Rule>
class SingleTreeTraversal {
  public:
    typedef typename Tree::NodeTree Box;
    Tree& tree;
    Rule& rules;
    counter_type num_prunes = 0;

    #ifdef _DEBUG_PAR
      counter_type num_basecases = 0;
      counter_type num_basecase[32];
      counter_type num_pruneEl[32];
    #endif


    /* Convenience constructor */
    SingleTreeTraversal(Tree& t, Rule& r, counter_type& num) : tree(t), rules(r), num_prunes(num) {

      #ifdef _DEBUG_PAR
          for (size_t i= 0; i < 32; i++ ) {
            num_basecase[i] = 0;
            num_pruneEl[i] = 0;
          }
      #endif

    }

    /* Traverse function for calculation of N-body */
    void traverse (Box& k, int t);
    void printVal();
};

/**
 * Traverse the tree while discarding regions of space
 * with hypersphere-hyperrectange intersections.
 */



template <typename Tree, typename Rule>
void
SingleTreeTraversal<Tree, Rule>::traverse (Box& s, int t) {

  int tree_size =  SingleTreeTraversal<Tree, Rule>::tree.data.size();
  if (!rules.prune_subtree(s, t)) {
    /* Evaluate the leaf node */
    if (s.is_leaf()) {
      rules.base_case(s, t);
      #ifdef _DEBUG_PAR
        num_basecase[omp_get_thread_num()] += s.size();
      #endif
    }
    else {
      vector<int> visit_order = rules.visit(s, t);
      int child_id = 0;
      for (int i = 0; i < visit_order.size(); i++) {
        child_id = s.child + visit_order[i];
        /* For algorithm which do not include reduction in their operators such as EM */
        #ifdef _DEBUG_NO_REDUC
          if (s.size() > (tree_size / omp_get_num_threads())){
            #pragma omp task  default(shared)
              traverse (tree.node_data[child_id], t);
          } else {
            traverse (tree.node_data[child_id], t);
          }
          #pragma omp taskwait
        #endif
          /* For algorithm which include reduction in their operators such as KNN */
        #ifndef _DEBUG_NO_REDUC
           traverse (tree.node_data[child_id], t);
        #endif
      }
    }
  }
  /* puting the center for the pruned branch */
  else {
    rules.centroid_case(s, t);
    #ifdef _DEBUG_PAR
        num_pruneEl[omp_get_thread_num()] += s.size();
    #endif
  }
}

template <typename Tree, typename Rule>
void
SingleTreeTraversal<Tree, Rule>::printVal () {

  #ifdef _DEBUG_PAR
    for (size_t i = 0; i < 32; i++) {
       num_basecases += num_basecase[i];
       cout  << "basecase " << i << " : " << (num_basecase[i]) << "\n" ;
    }
    for (size_t i = 0; i < 32; i++) {
        num_prunes += num_pruneEl[i];
        cout  << "pruneNodes " << i << " : " << (num_pruneEl[i]) << "\n" ;
    }
    cout << "Total base cases: " << num_basecases << "\n";
    cout << "Total prunes: " << num_prunes << "\n";
  #endif
}


#endif
