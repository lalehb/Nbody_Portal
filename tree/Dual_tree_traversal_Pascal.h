#ifndef _DUAL_TREE_TRAVERSAL_H_
#define _DUAL_TREE_TRAVERSAL_H_
#define counter_type unsigned long long


#ifdef _DEBUG_PAR
#define DEBUG_CMD(cmd) do { cmd} while( false )
#else
#define DEBUG_CMD(cmd) do { } while ( false )
#endif

#include <cfloat>
#include <omp.h>
#include <limits.h>
#include <cilk/cilk.h>
#include <cilk/cilk_api.h>

#include "Clock.hpp"
#include "timer.c"



template <typename Tree, typename Rule, typename pruneGenerator>
class DualTreeTraversal {
 public:
  typedef typename Tree::NodeTree Box;
  Tree& src_tree;
  Tree& trg_tree;

  Rule& rules;
  pruneGenerator prune;
  int task_level;
  counter_type num_prunes;
  counter_type num_basecases;


  #ifdef _DEBUG_PAR
    counter_type original_basecases = 0;
    counter_type num_basecase[32];
    counter_type num_pruneEl[32];
  #endif


  /* Convenience constructor */
  DualTreeTraversal(Tree& st, Tree& tt, Rule& r, pruneGenerator& pg, counter_type& num, int level = 4)
      : src_tree(st), trg_tree(tt), rules(r), prune(pg), num_prunes(num), task_level(level) {
        #ifdef _DEBUG_PAR
          for (size_t i= 0; i < 32; i++ ) {
            num_basecase[i] = 0;
            num_pruneEl[i] = 0;
          }
        #endif
        num_prunes = 0;
        num_basecases = 0;

        trg_tree.centroids(trg_tree.root());

  }

  /* Traverse the tree checking hypersphere-hyperrectangle intersections to discard regions of the space */
  void traverse (Box& s, Box& t) ;

 private:
  /* Implementation helper function */
  void traverse_impl(Box& s, Box& t, unsigned level) ;
};

/**
 * Traverse the tree while discarding regions of space
 * with hypersphere-hyperrectange (bounds) intersections.
 */
template <typename Tree, typename Rule, typename pruneGenerator>
void
DualTreeTraversal<Tree, Rule, pruneGenerator>::traverse(Box& s, Box& t)  {

    int NUMA_SIZE = 2;
    int ii = atoi(getenv("OMP_NUM_THREADS"))/NUMA_SIZE;
    omp_set_nested(1);
    omp_set_dynamic(0);

    NUMA_SIZE = min (NUMA_SIZE, atoi(getenv("OMP_NUM_THREADS")));
    /* to enable nested task parallelism in the tree travrsal computation*/
    #pragma omp parallel num_threads(NUMA_SIZE)
    {
      int i = omp_get_thread_num();
      #pragma omp parallel num_threads(ii)
      {
        #pragma omp single nowait
        {
            traverse_impl(s, trg_tree.node_data[NUMA_SIZE-1+i], 0);
        }
      }
    }

  #ifdef _DEBUG_PAR
    for (size_t i = 0; i < 32; i++) {
       num_basecases += num_basecase[i];
       // cout  << "basecase " << i << " : " << (num_basecase[i]) << "\n" ;
    }
    for (size_t i = 0; i < 32; i++) {
        num_prunes += num_pruneEl[i];
        // cout  << "pruneNodes " << i << " : " << (num_pruneEl[i]) << "\n" ;
    }
    cout << "Total base cases: " << num_basecases << "\n";
    cout << "Total prunes: " << num_prunes << "\n";
    cout << "Total : " << (num_basecases + num_prunes) << "\n";

    // cout << "Total base cases (from rules): " << rules.num_base << "\n";
    // cout << "Total prunes (from rules): " << rules.num_prunes << "\n";

  #endif

}



template <typename Tree, typename Rule, typename pruneGenerator>
void
DualTreeTraversal<Tree, Rule, pruneGenerator>::traverse_impl(Box& s, Box& t,
                                             unsigned level )  {


if (!rules.prune_subtree(s, t)) {
    /* If both source and target nodes are leaves, evaluate the base case */
    if (s.is_leaf() && t.is_leaf()) {
      rules.base_case (s, t);
      #ifdef _DEBUG_PAR
        counter_type sb_size = s.size(); // sourse size for base counting
        counter_type tb_size = t.size(); // target size for base counting
        num_basecase[omp_get_thread_num()] += sb_size * tb_size;
      #endif
    }
    /* Recurse down the target node. Recursion order does not matter. */
    else if ( (s.is_leaf() && !t.is_leaf())
          // ||((t.size() > 3 * s.size()) && !t.is_leaf() && !s.is_leaf())
          // ||((t.size() >= 3 * s.size()) && !t.is_leaf())
     ) {
      int child_id ;
       #ifdef TARGET_VISIT
        /* Note we do visit (t,s) because we are dividing on t and not s*/
        std::vector<int> visit_order = rules.visit(t,s);
       #endif
      for (size_t i = 0; i < t.num_child(); i++) {
         child_id = t.child + i;
         #ifdef TARGET_VISIT
            child_id = t.child + visit_order[i];
         #endif
        traverse_impl(s, trg_tree.node_data[child_id], level+1);
      }
    }

    /* Recurse down the source node. Recursion order does matter. */
    else if (!s.is_leaf() && t.is_leaf()) {
      /* Decide the visiting order of branches */
      std::vector<int> visit_order = rules.visit(s, t);
      int child_id ;
      for (size_t i = 0; i < visit_order.size(); i++ ) {
        child_id = s.child + visit_order[i];
        traverse_impl(src_tree.node_data[child_id], t, level+1);
      }
    }
    /* We have to recurse down both source and target nodes. */
    else {
        #ifdef TARGET_VISIT
          vector<int> visit_order_t = rules.visit(t,s);
        #endif

        for (int i = 0; i < t.num_child(); i++) {
          if ((level < task_level )){
            #pragma omp task shared(s,t) untied
            {
              int trg_child_id = t.child + i;
              vector<int> visit_order = rules.visit(s,
                                            trg_tree.node_data[trg_child_id]);
              for (int j = 0; j < visit_order.size(); j++) {
                int src_child_id = s.child + visit_order[j];
                traverse_impl(src_tree.node_data[src_child_id],
                                        trg_tree.node_data[trg_child_id], level+1);
              }
            }
          }else {

            int trg_child_id = t.child + i;
            #ifdef TARGET_VISIT
              trg_child_id = t.child + visit_order_t[i];
            #endif
            vector<int> visit_order = rules.visit(s,
                                          trg_tree.node_data[trg_child_id]);
            for (int j = 0; j < visit_order.size(); j++) {
              int src_child_id = s.child + visit_order[j];
              traverse_impl(src_tree.node_data[src_child_id],
                                      trg_tree.node_data[trg_child_id], level+1);
            }
          }
        }
     #pragma omp taskwait
    }
} else {
      rules.centroid_case(s, t);
      #ifdef _DEBUG_PAR
        counter_type sp_size = s.size();  // source  size for prune counting
        counter_type tp_size = t.size();  // target size for prune counting
        num_pruneEl[omp_get_thread_num()] += sp_size * tp_size;
      #endif
}
}

#endif
