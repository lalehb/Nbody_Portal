#!/usr/bin/env python

import sys
import csv
import time
sys.path.append("/usr/lib64/python3.4/site-packages/")
import numpy as np
import sklearn
from sklearn.naive_bayes import GaussianNB



def main():

    fileNameTrain = sys.argv[1]
    fileNameTest = sys.argv[2]

    # assign the predicator and  target variables
    X = np.array(list(csv.reader(open(fileNameTrain, "r"), delimiter=","))).astype("float")

    np.random.seed(0)
    Y = np.random.random((len(X), 1))
    for i in range(0,len(Y)):
        if Y[i] < 0.5 :
            Y[i] = 1
        else:
            Y[i] = 2

    Y = np.ravel(Y)

    Z = np.array(list(csv.reader(open(fileNameTest, "r"), delimiter=","))).astype("float")

    start_time = time.time()
    #Create a Gaussian Classifier
    model = GaussianNB()

    # Train the model using the training Set
    model.fit(X,Y)

    #predict output

    predicted = model.predict(X)

    print("--- %s seconds ---" % (time.time() - start_time))
    print predicted

if __name__ == "__main__":
  main()
