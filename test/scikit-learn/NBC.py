#!/usr/bin/env python

import sys
import csv
import time
sys.path.append("/usr/lib64/python3.4/site-packages/")
import numpy as np
import sklearn
from sklearn.naive_bayes import GaussianNB

# fileNameTrain = "/home/real_datasets/G2-D2-1000000.csv"
# fileNameVar = "/home/real_datasets/G2-D2-1000000.csv"
# fileNameTest = "/home/real_datasets/G2-D2-1000000.csv"


def main():


    fileNameTrain = sys.argv[1]
    fileNameVar = sys.argv[2]
    fileNameTest = sys.argv[3]
    
    start_time = time.time()
    # assign the predicator and  target variables
    X = np.array(list(csv.reader(open(fileNameTrain, "r"), delimiter=","))).astype("float")
   # Y = np.array(list(csv.reader(open(fileNameVar, "r"), delimiter=","))).astype("float")
    Z = np.array(list(csv.reader(open(fileNameTest, "r"), delimiter=","))).astype("float")
    Y = np.ones(1000000);

    #Create a Gaussian Classifier
    model = GaussianNB()

    # Train the model using the training Set
    model.fit(X,Y)

    #predict output

    predicted = model.predict(Z)

    print("--- %s seconds ---" % (time.time() - start_time))
    print predicted

if __name__ == "__main__":
  main()
