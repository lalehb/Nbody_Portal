#include <iostream>
#include "Nbody.h"
#include "NBodyExpression.h"
#include "PortalOperator.h"
#include "SetStorageObject.h"
#include "Data.h"
#include "Clock.hpp"

using namespace Nbody;



/*

Making a model for Niave Bayes Classification considering the features are sampled from a Gaussian distrbution
with diagonal covariance.
 We have two main set of data, one is tarin set, which each row presens features for each data points
 and the last one shows the lable for that data point, the implementation include a couple of steps:

 a) calculating the Naive Bayes model parameters (using train data):
    1- means
    2- covariance
    3- probablities
b) Classify the test data using info from step (a)
    1- calculate the likelihood
    2- compute the argmax to assign the class

*/

static inline
void
usage__ (const char* use)
{
  fprintf (stderr, "usage: %s <filenameTrain> <filenameTest> <#class> \n", use);
}

ComputeModelParammeters(SetStorageObject& mean_s, SetStorageObject& standard_deviation_s,
                        SetStorageObject& probablity_s, int num_class, SetStorageObject file_obj_train){

    int label = file_obj_train.pointSize();
    int data_size = file_obj_train.size();
    int dim = file_obj_train.pointSize() - 1;
    vector<int> class_counter(num_class);
    vector_of_array<double> mean;
    vector_of_array<double> standard_deviation;
    vector_of_array<double> probablity;

    mean = mean_s.getVectorOfArray();
    standard_deviation = standard_deviation_s.getVectorOfArray();
    probablity = probablity_s.getVectorOfArray();


    for (int i = 0; i < num_class; i++) {
      for (int j = 0; j < dim ; j++){
        mean[i][j] = 0;
        standard_deviation[i][j] = 0;
        probablity[i][j] = 0;
      }
    }

    for (int i = 0; i < data_size; i++) {
      for (int j = 0 ; j < dim ; i++) {
        // mean[(int)file_obj_train[i][label-1]][j] += file_obj_train[i][j];
        mean[0][j] += file_obj_train[i][j];
      }
      // class_counter[file_obj_train[i][label-1]]++;
      class_counter[0]++;
    }
    for (int i = 0; i < label; i++) {
      for (int j = 0 ; j < dim ; i++){
        mean[i][j] /= class_counter[i];
      }
    }
    for (int i = 0; i < data_size; i++) {
      for (int j = 0 ; j < dim; i++)
       standard_deviation[0][j] += (file_obj_train[i][j] - mean[0][j])
                                                          * (file_obj_train[i][j] - mean[0][j]);
      // standard_deviation[(int)file_obj_train[i][label-1]][j] += (file_obj_train[i][j] - mean[file_obj_train[i][label-1]][j])
      //                                                    * (file_obj_train[i][j] - mean[file_obj_train[i][label-1]][j]);
    }
    for (int i = 0; i < label; i++) {
      for (int j = 0 ; j < dim; i++){
        standard_deviation[i][j] = sqrt(standard_deviation[i][j] / class_counter[i]);
      }
      probablity[i][0] = class_counter[i] / data_size;
    }

    // mean_s = SetStorageObject(mean);
    // standard_deviation_s = SetStorageObject(standard_deviation);
    // probablity_s = SetStorageObject(probablity);


}

double accuracy_test( SetStorageObject output, SetStorageObject file_obj_test){
  double accuracy;
  double tolerance = 1;
  int dim = file_obj_test.pointSize()-1;
  int correct_counter = 0;
  for (int i = 0; i < output.size(); i++) {
    if (output[i][0] - file_obj_test[i][dim] < tolerance)
      correct_counter++;
  }
  accuracy = correct_counter * 100 / output.size();
  return accuracy;

}


int main (int argc, char** argv){

       if (argc != 4) {
            usage__ (argv[0]);
            return -1;
        }

        /* input the train dataset */
        std::string file_path_train = argv[1];
        SetStorageObject file_obj_train(file_path_train);

        /* input the test dataset */
        std::string file_path_test = argv[2];
        SetStorageObject file_obj_test(file_path_test);

        int num_class = atoi(argv[3]);

        /* just generates the set storage object for each of the needed file  */

        SetStorageObject mean(file_path_train);
        SetStorageObject standard_deviation(file_path_train);
        SetStorageObject probablity(file_path_train);

        /* computes the parammeters of the Naive Bayes Calculation based on the traning data */
        // ComputeModelParammeters(mean, standard_deviation, probablity, num_class, file_obj_train);


        Clock timer;
        float e = 2.71;
        float pi = 3.14;


        /* defining the kernel funcion */
        Expr a   = Variable::make("dataset");
        Expr b   = Variable::make("mean");
        SSO p    = SSO::make("prior_probablity");
        SSO sd  =  SSO::make("standard_deviation");


        Expr GaussianNB = (p[0][0] * sqrt((1/(2* pi))) * sd[0][0] * pow (e, ((-0.5 * ((a[0]-b[0])*(a[0]-b[0])))/(sd[0][0] * sd[0][0]))))
                          * (p[1][0] * sqrt((1/(2* pi))) * sd[1][0] * pow (e, ((-0.5 * ((a[1]-b[1])*(a[1]-b[1])))/(sd[1][0] * sd[1][0]))));

        // Expr GaussianNB = a[0] + b[0];

        /* defining the N-body expression using portal IRs */
        NBodyExpression exprTrain;
        exprTrain.attachStorage( "prior_probablity", probablity);
        exprTrain.attachStorage( "standard_deviation", standard_deviation);
        exprTrain.addLayer(PortalOperator(PortalOperator::OP::FORALL) , "dataset", file_obj_test);
        exprTrain.addLayer(PortalOperator(PortalOperator::OP::ARGMAX) , "mean" , mean, GaussianNB);

        const char* result_file = "test/NBC.stmt";
        exprTrain.compile_to_lowered_form(result_file);


        /* N-body caculation with portal IRs using the tree traversal */
        timer.start();
        exprTrain.executeTraverse();
        SetStorageObject output = exprTrain.getOutput();
        cout << "Total time: " << timer.seconds() << endl;


        double accuracy = accuracy_test(output, file_obj_test);

        cout << "Passed!   \n ";

	return 0;
}
