#include <iostream>
#include "Nbody.h"
#include "NBodyExpression.h"
#include "PortalOperator.h"
#include "SetStorageObject.h"
#include "Data.h"
#include "Clock.hpp"

using namespace Nbody;


static inline
void
usage__ (const char* use)
{
  fprintf (stderr, "usage: %s <filenameT> <filenameS> <min> <max> \n", use);
}

/* this function just compares the result of two different version of computation such as bruteforce vs traversal*/
bool validate(SetStorageObject result, SetStorageObject compute) {
        bool valid = true;
        for (int i = 0; i < result.size(); i++) {
          for(int j = 0; j < result.pointSize(); j++) {
             if (result[i][j] != compute[i][j] )
                 return false;
          }
        }
        return valid;
}


/* compares the result with the C++ computation of RangeSearch */
bool validateCBruteForce (SetStorageObject result, SetStorageObject outter, SetStorageObject inner , int h1, int h2){

  bool error = false;
  /* Set tolerance value for validation */
  const real_t tolerance = 1e-2;

  /* Allocate memory for exhaustive calculation of near neighbors */
  std::vector<double> nearest;

  /* Evaluate test/target point one by one */
  for (int i = 0; i < outter.size() ; i++) {
    /* Calculate brute-force nearest neighbor for validation */
    for (int j = 0; j < inner.size() ; j++) {
      real_t dist = 0.0;
      for (int d = 0; d < inner.pointSize(); d++)
        dist += (outter[j][d]-inner[i][d]) * (outter[j][d]-inner[i][d]);
      if ((dist > h1)&& (dist < h2))
        nearest.push_back(dist);
    }

    /* Sort the source points by its distance to the target points */
    sort (nearest.begin(), nearest.end());
    /* Sort the neighbor vector in the increasing order of distance */
    std::vector<double> kneighbors;
      for (int d = 0; d < inner.pointSize(); d++){
        kneighbors.push_back(result[i][d]);
      }
    sort (kneighbors.begin(), kneighbors.end());
    for (int j = 0; j < nearest.size(); j++) {
      if (fabs(result[i][j] - sqrt(nearest[j])) >= tolerance) {
        error = true;
        break;
      }
    }
    nearest.clear();
  }
  return error;


}




int main (int argc, char** argv){

       if (argc != 5) {
            usage__ (argv[0]);
            return -1;
        }

        /* input the target dataset */
        std::string filePathT = argv[1];
        SetStorageObject fileObjT(filePathT);

        /* input the source dataset */
        std::string filePathS = argv[2];
        SetStorageObject fileObjS(filePathS);

        int h1 = atoi(argv[3]);
        int h2 = atoi(argv[4]);

        Clock timer;

        /* defining the kernel funcion */
        Expr a = Variable::make("outterSet");
        Expr b = Variable::make("innerSet");
        Expr q1 = Integer::make(h1);
        Expr q2 = Integer::make(h2);
        Expr UnionEuclidDist = (((sqrt(((a[0] - b[0]) * (a[0] - b[0])) + ((a[1] - b[1]) * (a[1] - b[1]))))> q1)
                               && (sqrt( ((a[0] - b[0]) * (a[0] - b[0])) + ((a[1] - b[1]) * (a[1] - b[1])))< q2));

        /* defining the N-body expression using portal IRs*/
        NBodyExpression expr;
        expr.addLayer(PortalOperator(PortalOperator::OP::FORALL) , "outterSet", fileObjT);
        expr.addLayer(PortalOperator(PortalOperator::OP::UNIONARG) , "innerSet" , fileObjS , UnionEuclidDist);

        const char* result_file = "test/RangeSearch.stmt";
        expr.compile_to_lowered_form(result_file);


        /* N-body caculation with portal IRs using the tree traversal */
        timer.start();
        expr.executeTraverse();
        SetStorageObject output1 = expr.getOutput();
        cout << "Total time: " << timer.seconds() << endl;

        bool valid = validateCBruteForce(output1, fileObjT, fileObjS, h1, h2);

        if (valid) {
          cout << "Passed!" << endl;
        }
        else {
          cout << "Failed!" << endl;
        }

	return 0;
}
