#include <iostream>
#include <fstream>

#include "Nbody.h"
#include "NBodyExpression.h"
#include "PortalOperator.h"
#include "SetStorageObject.h"
#include "Data.h"
#include "Clock.hpp"
#include "PortalFunction.h"
#include "EM_kdtree.h"

#include <Eigen/Dense>       // for using the determinant and inverse of matrix in Gaussian
#include <Eigen/Eigenvalues> // for eigen values and eigen vectors
// #include <mkl.h>

using namespace Eigen;
using namespace Nbody;

static inline
void
usage__ (const char* use)
{
  fprintf (stderr, "usage: %s <filenameT> <filenameS> <filenameMean> <filenameCov> <#source> <#target> <#dim> <Threashold>\n", use);
}

/* this function just compares the result of two different version of computation such as bruteforce vs traversal*/
bool validate(SetStorageObject result, SetStorageObject compute) {
    bool valid = true;
    int failedPoints = 0;
    for (int i = 0; i < result.size(); i++) {
      bool validPoint = true;
      for(int j = 0; j < result.pointSize(); j++) {
         if (result[i][j] != compute[i][j] ) {
            cout <<  "correct and computed values: " << i << ", " << j << ":" <<result[i][j] << " , " << compute[i][j] << endl;
            valid =  false;
            validPoint = false;
         }
      if (!validPoint)
        failedPoints++;

      }
    }
    cout << "Point Failed: " << failedPoints << endl;
    return valid;
}


/* compares the result with the C++ computation of EM */
bool validateCBruteForce(SetStorageObject fileObjT, SetStorageObject fileObjMean, SetStorageObject fileObjCoef , SetStorageObject fileObjCov, int dim){

     bool error = false;
     double tol = 0.01;
     vector_of_array<double> source;
     source = fileObjT.getVectorOfArray();

     EMBase emTest(fileObjT.size(), fileObjMean.size(), dim, 10, tol, source);
     emTest.IterationEM();
     int i,j,l;

     MeanSigma a;
     a = emTest.Result();
     for (i = 0; i < fileObjT.size(); ++i) {
       for (j = 0; j < dim; ++j) {
         if (std::isnan(fileObjMean[i][j])) {
           cerr << i << "|" << j << ": " << fileObjMean[i][j] << " , ";
           cout << "please re-run the test, there is NAN in the mean of a Gaussian" << endl;
           error = true;
           exit(0);
         }
         else if (a.mean[i][j] - fileObjMean[i][j] >= tol){
           cerr << "Gaussian  " << i << " doesn't match at dimension " << j << endl;
           cerr << "Correct version: " << a.mean[i][j] << endl;
           cerr << "Computed version: " << fileObjMean[i][j] << endl;
           error = true;
           exit(0);
         }
         for (l = 0; l < dim; ++l) {
           if (std::isnan(fileObjCov[i][(j*dim)+l])) {
             cerr << i << "|" << j << "|" << l << ": " << fileObjCov[i][(j*dim)+l];
             cout << "please re-run the test, there is NAN in the covariance of a Gaussian" << endl;
             error = true;
             exit(0);
         }
         else if (a.sigma[i][j][l] - fileObjCov[i][(j*dim)+l] >= tol){
             cerr << "Gaussian " << i  << " dosn't match at Cov on " << j  << ", "<< l<< endl;
             cerr << "Correct version: " << a.sigma[i][j][l] << endl;
             cerr << "Computed version: " << fileObjCov[i][(j*dim)+l] << endl;
             error = true;
             exit(0);
           }
         }
       }
     }
     return error;


}


int main (int argc, char** argv){

    if (argc != 9) {
        usage__ (argv[0]);
        return -1;
    }


    /* input the target dataset */
    std::string filePathT = argv[1];
    SetStorageObject fileObjT(filePathT);

    /* input the source dataset */
    std::string filePathS = argv[2];
    SetStorageObject fileObjS(filePathS);

    std::string filePathCoef = argv[3];
    SetStorageObject fileObjCoef(filePathCoef);

    std::string filePathCov = argv[4];
    SetStorageObject fileObjCov(filePathCov);

    int sourse_size = atoi(argv[5]);
    int target_size = atoi(argv[6]);
    int dim  = atoi(argv[7]);
    double Threashold = atof(argv[8]);
    SetStorageObject output2_ll;

    float e = 2.71;
    float pi = 3.14;
    Clock timer;
    double oldLL = -INFINITY;
    double newLL = 0;
    int iter = 0;

    timer.start();
  /* the convergence condition checking the Log likelihood*/
  while (abs(oldLL - newLL) >  Threashold) {

    /* E-step of EM calculation */

    /* defining the kernel funcion */
    Expr a   = Variable::make("outterSet");
    Expr b   = Variable::make("innerSet");
    SSO p   = SSO::make("coefficinet");
    Expr cov =  Variable::make("covariance");



    Expr d = Det(a);
    Expr tras = Transp(a,b);
    Expr inve = Inv(cov,a);
    Expr matMul = MatrixMul(b, a, b, a);
    Expr Gaussian = (p[0][0] * sqrt((1/(2* pi))) * sqrt(d))
                                                * pow (e, (-0.5 * a[0]));
    // Expr Gaussian = (p[0][0] * sqrt((1/(2* pi))) * sqrt(Det(a)))
                                          // * pow (e, (-0.5 * MatrixMul(Transp(a-b), Inv(cov), (a-b))));


    /* defining the N-body expression using portal IRs*/
    NBodyExpression exprE;
    exprE.attachStorage( "coefficinet", filePathCoef);
    exprE.attachStorage( "covariance", filePathCov);
    exprE.addLayer(PortalOperator(PortalOperator::OP::FORALL) , "outterSet", fileObjT);
    exprE.addLayer(PortalOperator(PortalOperator::OP::FORALL) , "innerSet" , fileObjS , Gaussian);


    const char* result_file = "test/EM_E.stmt";
    exprE.compile_to_lowered_form(result_file);


    /* N-body caculation for computing the denuminator of E-step for calculating the responsibility function*/
    exprE.execute();
    // exprE.executeTraverse();
    SetStorageObject output2 = exprE.getOutput();

    Expr f   = Variable::make("innerSum");
    Expr Responsibility = 1/ f[0];
    NBodyExpression exprDenum;

    exprDenum.addLayer(PortalOperator(PortalOperator::OP::FORALL) , "outterSet", fileObjT);
    exprDenum.addLayer(PortalOperator(PortalOperator::OP::SUM) , "innerSum" , output2 , Responsibility);

    exprDenum.execute();
    // exprDenum.executeTraverse();
    SetStorageObject output3 = exprDenum.getOutput();


/* M-step of EM calculation */

    /* write back the coefficent to be used for the next iteration */

    double nk[target_size];
    for (int i = 0; i < output2.size(); i++) {
      double temp = 0;
      for(int j = 0; j < output2.pointSize(); j++) {
        temp += output2[i][j];
      }
      temp /= output2.pointSize();
      nk[i] = temp;
      filePathCoef[i] = temp;
    }

    /* write back the the mean (b) */
    double* temp[dim];
    for (int i = 0; i < output2.size(); i++) {
      temp[i] = new double[dim];
    }

    for (int i = 0; i < output2.size(); i++) {
      for (int j = 0; j < dim; j++) {
         temp[i][j] = fileObjT[i][j];
      }
    }
    for (int i = 0; i < output2.size(); i++) {
      double temp2[dim];
      for(int j = 0; j < output2.pointSize(); j++) {
        for (int k = 0; k < dim; k++)
          temp2[k] += output2[i][j] * temp[i][j];
      }
      for (int k = 0; k < dim; k++) {
          temp2[k] /= nk[i];
          filePathS[k] = temp2[k];
      }
    }


    /* write back the the Covariance (cov) to be used for the next iteration */

   double eps = std::pow(10, -16);
   for (int i = 0; i < target_size; ++i) {
     MatrixXd sigTemp(dim,dim);
     for (int j = 0; j < sourse_size; ++j) {
       VectorXd x(dim);
       VectorXd m(dim);
       VectorXd temp3(dim);
       for (int f = 0; f < dim ; ++f) {
         x(f) = output2[j][f];
         m(f) = temp[i][f];
         temp3(f) = x(f)-m(f);
       }
       sigTemp = sigTemp + (output2[j][i]*(temp3 * temp3.transpose()));
     }
     if (nk[i] == 0)
       nk[i] = eps;
     sigTemp = sigTemp /nk[i];

     // Adding Epsilon to the Zero places in the eigen values to make the Sigma positive-semi-definite
     real_t eps = std::pow(10, -6);

     SelfAdjointEigenSolver<MatrixXd> es(sigTemp);
     MatrixXd Di = es.eigenvalues().asDiagonal();
     MatrixXd V = es.eigenvectors();
     for (int i = 0; i < dim; ++i) {
       if (Di(i,i) <= 0)
         Di(i,i) = eps ;
     }

     sigTemp = V * Di * V.inverse();
     /* sigma assigning */
     for (int j = 0; j < dim; ++j) {
       for(int f = 0; f < dim; ++f)
           filePathT[j] = sigTemp(j,f) ;
     }
   }

/* LL computation for EM */

    /* defining variables and the  kernel funcion */
    Expr a_ll    = Variable::make("outterSetll");
    Expr b_ll    = Variable::make("innerSetll");
    SSO p_ll   = SSO::make("coefficinetll");
    Expr cov_ll  =  Variable::make("covariancell");

    Expr d_ll = Det(a_ll);
    Expr tras_ll = Transp(a_ll,b_ll);
    Expr inve_ll = Inv(cov_ll,a_ll);
    Expr matMul_ll = MatrixMul(b_ll, a_ll, b_ll, a_ll);
    Expr LL = (p_ll[0][0] * sqrt((1/(2* pi))) * sqrt(d_ll))
                                                * pow (e, (-0.5 * a_ll[0]));

    // Expr LL =  p_ll[0][0] * sqrt((1/(2* pi))) * sqrt(Det(a_ll))
    //                                        * pow (e, (-0.5 * MatrixMul(Transp(a_ll-b_ll), Inv(cov_ll), (a_ll-b_ll))));

    /* defining the N-body expression using portal IRs*/
    NBodyExpression exprLL;
    exprLL.attachStorage( "coefficinetll", filePathCoef);
    exprLL.attachStorage( "covariancell", filePathCov);
    exprLL.addLayer(PortalOperator(PortalOperator::OP::SUM) , "outterSetll", fileObjT);
    exprLL.addLayer(PortalOperator(PortalOperator::OP::LOGSUM) , "innerSetll" , fileObjS , LL);


    const char* result_file_ll = "test/EM_ll.stmt";
    exprLL.compile_to_lowered_form(result_file_ll);


    /* N-body caculation with portal */
    exprLL.execute();
    // exprLL.executeTraverse();
    SetStorageObject output2_ll = exprLL.getOutput();

    oldLL = newLL;
    newLL = output2_ll[0][0];

 }

 cout << "Total time:" << timer.seconds() << endl;
 SetStorageObject fileObjT_(filePathT);
 SetStorageObject fileObjMean_(filePathS);
 SetStorageObject fileObjCoef_(filePathCoef);
 SetStorageObject fileObjCov_(filePathCov);
 bool valid =  !validateCBruteForce(fileObjT_, fileObjMean_,fileObjCoef_, fileObjCov_ , dim);


 if (valid) {
   cout << "Passed!" << endl;
 }
 else {
   cout << "Failed!" << endl;
 }

 return 0;
}
