#include <iostream>
#include "Nbody.h"
#include "NBodyExpression.h"
#include "PortalOperator.h"
#include "SetStorageObject.h"
#include "Data.h"
#include "JITModule.h"
#include <math.h>
#include "Clock.hpp"

//using namespace std;
using namespace Nbody;
// typedef GaussianKernel KernelType;

static inline
void
usage__ (const char* use)
{
  fprintf (stderr, "usage: %s <filenameT> <filenameS> <threshold>\n", use);
}


/* this function just compares the result of two different version of computation such as bruteforce vs traversal*/
bool validate(SetStorageObject result, SetStorageObject compute) {
      bool valid = true;
      int failedPoints = 0;
      double tolerance = 0.00001;
      for (int i = 0; i < result.size(); i++) {
        bool validPoint = true;
        for(int j = 0; j < result.pointSize(); j++) {
           if (abs(result[i][j] - compute[i][j]) >= tolerance ) {
              valid =  false;
              validPoint = false;
           }
        if (!validPoint)
          failedPoints++;

        }
      }
      return valid;
}


bool  validateCBruteForce(SetStorageObject result_forces, SetStorageObject outter,
                         SetStorageObject inner, real_t epsilon, int d) {

             vector_of_array<real_t> outter_;
             vector_of_array<real_t> inner_;

             outter_ = outter.getVectorOfArray();
             inner_  = inner.getVectorOfArray();
             Clock timer;
             real_t gravitational_constant = 6.674e-11;
             timer.start();

             bool error = false;
             real_t tolerance = epsilon;
             real_t regulizer = 10e-14;
             real_t dist;

             std::vector<real_t> results;
             for (int i = 0 ; i < outter_.size(); i++) {
               real_t temp = 0;
               for (int j = 0; j < inner_.size(); j++) {
                   dist = 0;
                   for(int w = 0; w < 3 ; w++)
                     dist += (outter_[i][w]-inner_[j][w]) * (outter_[i][w]-inner_[j][w]);
                   dist = sqrt(dist)+ regulizer;
                   temp +=  (outter_[i][3] * inner_[j][3]) * gravitational_constant / dist;
                 }
               results.push_back(temp);
             }
             std::cout << "Naive C++ time:" << timer.seconds() << "\n";
             for (int i = 0 ; i < outter_.size(); i++) {
                    if (abs(results[i] - result_forces[i][0]) > tolerance) {
                      error = true;
                      cout << "At ["<< i << "]--> expected: " << results[i] << ", computed: " << result_forces[i][0] << "\n";
                      break;
                    }
             }
         return error;
}



int main (int argc, char** argv){

	if (argc != 4) {
            usage__ (argv[0]);
            return -1;
    }

    std::string filePathT = argv[1];
    SetStorageObject fileObjT(filePathT);


    std::string filePathS = argv[2];
    SetStorageObject fileObjS(filePathS);

    real_t threshold = atof(argv[3]);
    Clock timer;


    Expr a = Variable::make("outterSet");
    Expr b = Variable::make("innerSet");
    Expr gravitational_constant = Flt::make(6.67e-11);
    Expr regulizer = Flt::make(6.67e-14);


    /* computing Potential instead of 3 dimenstion of forces */
    Expr denum = sqrt(pow(a[0] - b[0] , 2) + pow(a[1] - b[1] , 2) + pow(a[2] - b[2] , 2)) + regulizer ;
    Expr force = gravitational_constant * (a[3] * b[3])/denum;


    NBodyExpression expr;
    expr.addLayer(PortalOperator(PortalOperator::OP::FORALL) , "outterSet", fileObjT);
    expr.addLayer(PortalOperator(PortalOperator::OP::SUM) , "innerSet" , fileObjS , force);


    const char* result_file = "test/BH.stmt";
    expr.compile_to_lowered_form(result_file);

    timer.start();
    expr.executeTraverse();
    SetStorageObject output = expr.getOutput();
    cout << "Total time: " << timer.seconds() << endl;

    bool error = validateCBruteForce(output, fileObjT, fileObjS, threshold, 0);


    if (!(error)) {
      cout << "Passed!" << endl;
    }
    else {
      cout << "Failed!" << endl;
    }


	return 0;
}
