#include <iostream>
#include "Nbody.h"
#include "NBodyExpression.h"
#include "PortalOperator.h"
#include "SetStorageObject.h"
#include "Data.h"
#include "JITModule.h"
#include <math.h>
#include "Clock.hpp"
// #include "Gaussian_kernel.h"


//using namespace std;
using namespace Nbody;
// typedef GaussianKernel KernelType;

static inline
void
usage__ (const char* use)
{
  fprintf (stderr, "usage: %s <filenameT> <filenameS> <kernelBandwidth>\n", use);
}


/* this function just compares the result of two different version of computation such as bruteforce vs traversal*/
bool validate(SetStorageObject result, SetStorageObject compute) {
      bool valid = true;
      int failedPoints = 0;
      double tolerance = 0.00001;
      for (int i = 0; i < result.size(); i++) {
        bool validPoint = true;
        for(int j = 0; j < result.pointSize(); j++) {
           if ((result[i][j] - compute[i][j]) >= tolerance ) {
              valid =  false;
              validPoint = false;
           }
        if (!validPoint)
          failedPoints++;

        }
      }
      return valid;
}

// bool validateCBruteForce(int num_pts, int dim, KernelType& kernel, real_t atol, real_t rtol, Points_t& sources, Points_t& targets, Vec& result) {
//   bool error = false;
  // real_t norm = kernel.norm(dim);
  //
  // /* Allocate memory for exhaustive calculation of kernel density */
  // Vec density(num_pts);
  //
  // /* Evaluate test/target point one by one */
  // for (int i = 0; i < num_pts; i++) {
  //   /* Calculate brute-force nearest neighbor for validation */
  //   real_t kval = 0.0;
  //   for (int j = 0; j < num_pts; j++) {
  //     // kval += kernel.compute(targets[i], sources[j]);
  //   }
  //   density[i] = kval * norm;
  //
	//   /* Set tolerance value for validation */
  //   real_t tolerance = (atol == 0.0 && rtol == 0.0) ? 1e-2 : atol + rtol * density[i];
  //   if (fabs(density[i] - result[i]) >= tolerance) {
  //     cerr << "Target point: " << i << endl;
  //     cerr << "Correct density: " << density[i] << endl;
  //     cerr << "Computed density: " << result[i] << endl;
  //     error = true;
  //     exit(0);
  //   }
  // }
//   return error;
// }


int main (int argc, char** argv){

	if (argc != 4) {
            usage__ (argv[0]);
            return -1;
    }

    std::string filePathT = argv[1];
    SetStorageObject fileObjT(filePathT);


    std::string filePathS = argv[2];
    SetStorageObject fileObjS(filePathS);

    int kernelBandwidth = atoi(argv[3]);

    int dim = 2;
    Clock timer;


    Expr a = Variable::make("outterSet");
    Expr b = Variable::make("innerSet");

    Expr norm = 1.0 / pow((sqrt(2.0 * M_PI) * kernelBandwidth), dim);
    Expr EuclidDist = exp(-0.5 * pow(kernelBandwidth, -2.0) * (sqrt(pow(a[0] - b[0] , 2) + pow(a[1] - b[1] , 2))))  / pow(2 , 2);

    Expr q = Integer::make(fileObjS.size());
    Expr outer =  1 / q;

    NBodyExpression expr;
    expr.addLayer(PortalOperator(PortalOperator::OP::FORALL) , "outterSet", fileObjT, outer);
    // expr.addLayer(PortalOperator(PortalOperator::OP::FORALL) , "outterSet", fileObjT);
    expr.addLayer(PortalOperator(PortalOperator::OP::SUM) , "innerSet" , fileObjS , EuclidDist);


    const char* result_file = "test/KDE.stmt";
    expr.compile_to_lowered_form(result_file);


    timer.start();
    expr.executeTraverse();
    SetStorageObject output1 = expr.getOutput();
    cout << "Total time: " << timer.seconds() << endl;


    expr.execute();
    SetStorageObject output2 = expr.getOutput();


    bool valid = validate(output1, output2);

    if (!valid) {
      cout << "Passed!" << endl;
    }
    else {
      cout << "Failed!" << endl;
    }

	return 0;
}
