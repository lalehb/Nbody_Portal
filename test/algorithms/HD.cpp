#include <iostream>
#include "Nbody.h"
#include "NBodyExpression.h"
#include "PortalOperator.h"
#include "SetStorageObject.h"
#include "Data.h"
#include "Clock.hpp"

using namespace Nbody;

static inline
void
usage__ (const char* use)
{
  fprintf (stderr, "usage: %s <filenameT> <filenameS> \n", use);
}

/* Comparison of two different version of computation such as bruteforce vs traversal*/
bool validateCompare(SetStorageObject result, SetStorageObject compute) {
    bool valid = true;
    for (int i = 0; i < result.size(); i++) {
      for(int j = 0; j < result.pointSize(); j++) {
         if (result[i][j] != compute[i][j] )
             return false;
      }
    }
    return valid;
}

/* comparison of the result with the C++ computation of HD */
bool validateCBruteForce(SetStorageObject result, SetStorageObject outter, SetStorageObject inner ) {

  bool valid = true;
  double threshold = 1;
  vector_of_array<real_t> outter_;
  vector_of_array<real_t> inner_;

  outter_ = outter.getVectorOfArray();
  inner_  = inner.getVectorOfArray();
  Clock timer;
  timer.start();
  real_t maxmin = -1;
  /* Evaluate test/target point one by one */
  for (int i = 0; i < outter_.size(); i++) {
    real_t  min = DBL_MAX;
    /* Calculate brute-force  for validation */
    for (int j = 0; j < inner_.size(); j++) {
      real_t dist = 0.0;
      for (int d = 0; d < inner.pointSize() ; d++)
        dist += (outter_[i][d]-inner_[j][d]) * (outter_[i][d]-inner_[j][d]);
      dist = sqrt(dist);
      if (( dist < min))
        min = dist;
    }
    if (min > maxmin)
       maxmin = min;
 }
 cout << "C++ Total time: " << timer.seconds() << endl;
 if (abs(maxmin - result[0][0]) > threshold) {
    valid =  false;
 }
cout << maxmin << " , " << result[0][0] << "\n";
return valid;
}



int main (int argc, char** argv){

    if (argc != 3) {
        usage__ (argv[0]);
        return -1;
    }

    /* input the target dataset */
    std::string filePathT = argv[1];
    SetStorageObject fileObjT(filePathT);

    /* input the source dataset */
    std::string filePathS = argv[2];
    SetStorageObject fileObjS(filePathS);

    Clock timer;

    /* defining the kernel funcion */
    Expr a = Variable::make("outterSet");
    Expr b = Variable::make("innerSet");
    Expr EuclidDist = sqrt(((a[0] - b[0]) * (a[0] - b[0])) + ((a[1] - b[1]) * (a[1] - b[1])));

    /* defining the N-body expression using portal IRs*/
    NBodyExpression expr;
    expr.addLayer(PortalOperator(PortalOperator::OP::MAX) , "outterSet", fileObjT);
    expr.addLayer(PortalOperator(PortalOperator::OP::MIN) , "innerSet" , fileObjS , EuclidDist);

    const char* result_file = "test/HD.stmt";
    expr.compile_to_lowered_form(result_file);

    /* N-body caculation with portal IRs using the tree traversal */
    timer.start();
    expr.executeTraverse();
    SetStorageObject output1 = expr.getOutput();
    cout << "Total time: " << timer.seconds() << endl;


    bool valid = validateCBruteForce(output1, fileObjT, fileObjS);

    if (valid) {
      cout << "Passed!" << endl;
    }
    else {
      cout << "Failed!" << endl;
    }

	  return 0;
}
