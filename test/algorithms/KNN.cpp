#include <iostream>
#include "Nbody.h"
#include "NBodyExpression.h"
#include "PortalOperator.h"
#include "SetStorageObject.h"
#include "Data.h"
#include "Clock.hpp"

using namespace Nbody;

static inline
void
usage__ (const char* use)
{
  fprintf (stderr, "usage: %s <K> <filenameT> <filenameS> \n", use);
}

/* this function just compares the result of two different version of computation such as bruteforce vs traversal in portal*/
bool validate(SetStorageObject result, SetStorageObject compute) {
    bool valid = true;
    int failedPoints = 0;

    for (int i = 0; i < result.size(); i++) {
      bool validPoint = true;
      for(int j = 0; j < result.pointSize(); j++) {
         if (result[i][j] != compute[i][j] )
         {
            cout <<  "correct and computed values: " << i << ", " << j << ":" <<result[i][j] << " , " << compute[i][j] << endl;
            valid =  false;
            validPoint = false;
            exit;
         }
      if (!validPoint)
        failedPoints++;

      }
    }
    cout << "Point Failed: " << failedPoints << endl;
    return valid;
}



class kNN_distance {
  public:
    real_t squared_distance;  /* Squared distance of the referenced element to an implicit point */
    int index;  /* Index of the feature vector in the data set */

    /* Default and convenience constructors */
    kNN_distance() : squared_distance(DBL_MAX), index(-1) {}
    kNN_distance(int i, real_t sq_dist) : squared_distance(sq_dist), index(i) {}

    /* Distance comparison operator for kNN_distances. Allows kNN_distance objects to be used as STL comparison functors */
    bool operator< (const kNN_distance &v) const {
      return squared_distance < v.squared_distance;
    }
};

typedef vector<kNN_distance> kNeighbors;
typedef vector<kNeighbors> resultNeighbors;


/* compares the result with the C++ computation of HD */
bool validateCBruteForce(SetStorageObject result, SetStorageObject outter, SetStorageObject inner, int K ){

  bool error = false;
  /* Set tolerance value for validation */
  const real_t tolerance = 1e-2;


  vector_of_array<real_t> outter_;
  vector_of_array<real_t> inner_;

  outter_ = outter.getVectorOfArray();
  inner_  = inner.getVectorOfArray();

  /* Allocate memory for exhaustive calculation of near neighbors */
  kNN_distance* nearest = new kNN_distance[outter_.size()];

  /* Evaluate test/target point one by one */
  for (int i = 0; i < outter_.size() ; i++) {
    /* Calculate brute-force nearest neighbor for validation */
    for (int j = 0; j < inner_.size() ; j++) {
      real_t dist = 0.0;
      for (int d = 0; d < inner.pointSize(); d++)
        dist += (outter_[j][d]-inner_[i][d]) * (outter_[j][d]-inner_[i][d]);
      dist = sqrt(dist);
      nearest[j] = kNN_distance(j, dist);
    }

    /* Sort the source points by its distance to the target points */
    sort (nearest, nearest + inner_.size());

    for (int j = 0; j < K; j++) {
      if (fabs(result[i][j] - (nearest[j].squared_distance)) >= tolerance)
      {
        cout <<  "correct and computed values: " << i << ", " << j << " --> " <<result[i][j]
             << " , " << (nearest[j].squared_distance) << endl;
        error = true;
        
        return error;
        exit;
        break;
      }
    }
  }
  return error;

}




int main (int argc, char** argv){

    if (argc != 4) {
        usage__ (argv[0]);
        return -1;
    }

    /* K represents the number of neighbors */
    int K  = atoi(argv[1]);

    /* input the target dataset */
    std::string filePathT = argv[2];
    SetStorageObject fileObjT(filePathT);

    /* input the target dataset */
    std::string filePathS = argv[3];
    SetStorageObject fileObjS(filePathS);

    Clock timer;

    /* defining the kernel funcion */
    Expr a = Variable::make("outterSet");
    Expr b = Variable::make("innerSet");
    Expr EuclidDist = sqrt(((a[0] - b[0]) * (a[0] - b[0])) + ((a[1] - b[1]) * (a[1] - b[1])));

    /* defining the N-body expression using portal IRs*/
    NBodyExpression expr;
    expr.addLayer(PortalOperator(PortalOperator::OP::FORALL) , "outterSet", fileObjT);
    expr.addLayer(PortalOperator(PortalOperator::OP::KMIN, K) , "innerSet" , fileObjS , EuclidDist);

    const char* result_file = "test/KNN.stmt";

    expr.compile_to_lowered_form(result_file);


    /* N-body caculation with portal IRs using the tree traversal */
    timer.start();
    expr.executeTraverse();
    SetStorageObject output1 = expr.getOutput();
    cout << "Total time: " << timer.seconds() << endl;


    bool valid = validateCBruteForce(output1,fileObjT,fileObjS,K);

    if (!valid) {
      cout << "Passed!" << endl;
    }
    else {
      cout << "Failed!" << endl;
    }

    return 0;
}
