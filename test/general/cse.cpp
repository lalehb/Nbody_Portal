#include <iostream>
#include "Nbody.h"

using namespace std;
using namespace Nbody;

int main (int argc, char** argv)
{
  Var x;
  Func kernel;

  /* Compiler does the CSE during optimization pass */
  kernel(x) = x*x + x*x;

  cout << "Computing function to lowered form...\n";
  const char* result_file = "cse.stmt";
  kernel.compile_to_lowered_form(result_file);

  cout << "Evaluating function...\n";
  Points<int32_t> output = kernel.evaluate(5);

  /* Check output */
  for (int i = 0; i < 5; ++i)
    cout << i << ": " << output(i) << '\n';

  cout << "Passed." << endl;
  return 0;
}
