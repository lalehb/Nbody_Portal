#include <iostream>
#include "Nbody.h"

using namespace std;
using namespace Nbody;

int main (int argc, char** argv)
{
  Var x;
  Func kernel;

  kernel(x) = 3+(x+4);

  cout << "Computing function to lowered form...\n";
  const char* result_file = "simplify.stmt";
  kernel.compile_to_lowered_form(result_file);

  cout << "Passed." << endl;
  return 0;
}
