#include <iostream>
#include "Nbody.h"

using namespace std;
using namespace Nbody;

int main (int argc, char** argv)
{
  Var x;
  Func kernel;

  cout << "Sqrt function...\n";
  kernel(x) = 1/sqrt(x);

  cout << "Vectorizing outer loop...\n";
  kernel.vectorize(x,4);

  cout << "Computing function to lowered form...\n";
  const char* result_file = "extern_lowered.stmt";
  kernel.compile_to_lowered_form(result_file);

  cout << "Evaluating function...\n";
  Points<float> output = kernel.evaluate(8);

  /* Check output */
  for (int i = 0; i < 8; ++i)
    cout << i << ": " << output(i) << '\n';


  cout << "Passed." << endl;
  return 0;
}
