#include <iostream>
#include "Nbody.h"
#include "utils.h"

using namespace std;
using namespace Nbody;

static inline
  void
usage__ (const char* use)
{
  fprintf (stderr, "usage: %s <N> <dim> \n", use);
}

int main (int argc, char** argv)
{
  unsigned int num_pts;
  unsigned int dim;

  if (argc != 3) {
    usage__ (argv[0]);
    return -1;
  }

  /** Command line input */
  num_pts = atoi (argv[1]);
  dim = atoi (argv[2]);

  /* Initialize seed */
  util::default_generator.seed(1337);

  cout << "Generating input points...\n";
  Points<float> input(num_pts, dim);
  for (int i = 0; i < input.m(); ++i)
    for (int j = 0; j < input.n(); ++j)
      input(i,j) = util::random<float>::get();

  Var x, y;
  Func kernel;

  cout << "Sqrt function...\n";
  kernel(x,y) = input(x,y);

  cout << "Vectorizing outer loop...\n";
  kernel.vectorize(x,4);

  cout << "Computing function to lowered form...\n";
  const char* result_file = "input_lowered.stmt";
  kernel.compile_to_lowered_form(result_file);

  cout << "Evaluating function...\n";
  Points<float> output = kernel.evaluate(input.m(), input.n());

  /* Check output */
  for (int j = 0; j < output.n(); ++j)
    for (int i = 0; i < output.m(); ++i)
      cout << i << ", " << j << ": " << output(i,j) << " " << input(i,j) << '\n';


  cout << "Passed." << endl;
  return 0;
}
