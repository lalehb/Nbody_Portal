#include <iostream>
#include "Nbody.h"

using namespace std;
using namespace Nbody;

int main (int argc, char** argv)
{
  Var x;
  Func kernel;

  cout << "Lowering trivial function...\n";
  kernel(x) = x;

  cout << "Computing function to lowered form...\n";
  const char* result_file = "lowered.stmt";
  kernel.compile_to_lowered_form(result_file);

  cout << "Passed." << endl;
  return 0;
}
