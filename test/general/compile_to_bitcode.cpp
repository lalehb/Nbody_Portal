#include <iostream>
#include "Nbody.h"

using namespace std;
using namespace Nbody;

int main (int argc, char** argv)
{
  Var x,y;
  Func kernel;

  cout << "Compiling to bitcode...\n";
  kernel(x,y) = x+y;

  const char* result_file = "compile_to_bitcode.bc";
  std::vector<Argument> empty_args;
  kernel.compile_to_bitcode(result_file, empty_args);

  cout << "Passed." << endl;
  return 0;
}
