#ifndef _SIMD_HELPER_H_
#define _SIMD_HELPER_H_

#if !defined (IDEAL_ALIGNMENT)
/** Default alignment, in bytes. */
#  define IDEAL_ALIGNMENT 32
#endif

#if !defined (BEST_ALIGNMENT_ATTR)
#  define BEST_ALIGNMENT_ATTR __attribute__ ((aligned (64)))
#else
#  define BEST_ALIGNMENT_ATTR
#endif

/** \brief No. of real_t words / SIMD register. */
#define SIMD_LEN (IDEAL_ALIGNMENT / sizeof (real_t))

#if !defined (USE_FLOAT)
#  if !defined (__SSE2__)
#    error "*** SSE2 is not available. ***"
#  endif
#  include <emmintrin.h>
/** \name SIMD instructions */
/*@{*/
#  define SIMD_REG      __m256d
#  define SIMD_VAL      m256d_f64
#  define SIMD_LOAD     _mm_load_pd
#  define SIMD_STORE    _mm_store_pd
#  define SIMD_LOAD_U   _mm256_loadu_pd
#  define SIMD_STORE_U  _mm_storeu_pd
#  define SIMD_STORE_S  _mm256_store_pd
#  define SIMD_LOAD1    _mm_load1_pd
#  define SIMD_SET      _mm256_set_pd
#  define SIMD_SET1     _mm256_set1_pd
#  define SIMD_ZERO     SIMD_SET1 (0.0)
#  define SIMD_SUB      _mm256_sub_pd
#  define SIMD_ADD      _mm256_add_pd
#  define SIMD_MUL      _mm256_mul_pd
#  define SIMD_DIV      _mm_div_pd
#  define SIMD_HADD     _mm256_hadd_pd
#  define SIMD_XOR      _mm_xor_pd
#  define SIMD_ANDNOT   _mm_andnot_pd
#  define SIMD_SHUFFLE  _mm_shuffle_pd
#  define SIMD_SQRT     _mm_sqrt_pd
#  define SIMD_INV(x)   SIMD_DIV (SIMD_SET1 (1.0), (x))
#  define SIMD_CMPEQ    _mm_cmpeq_pd
#  define SIMD_CVTD_S   _mm_cvtpd_ps
#  define SIMD_CVTS_D   _mm_cvtps_pd
#  define SIMD_RSQRT_S  _mm_rsqrt_ps
#  define SIMD_INV_SQRT_S(x) SIMD_CVTS_D (SIMD_RSQRT_S (SIMD_CVTD_S(x)))
#  define SIMD_INV_SQRT(x)   SIMD_INV(SIMD_SQRT(x))

#  define SIMD_SET_1    SIMD_SET (1.0, -1.0)
#  define SHUFFLE_0  _MM_SHUFFLE2 (0, 0)
#  define SHUFFLE_1  _MM_SHUFFLE2 (1, 1)
#  define SHUFFLE_2  _MM_SHUFFLE2 (0, 1)
/*@}*/
/** FFT functions */
#  define FFT_COMPLEX   fftw_complex
#  define FFT_PLAN    fftw_plan

#  define FFT_CREATE  fftw_plan_many_dft_r2c
#  define FFT_RE_EXECUTE fftw_execute_dft_r2c
#  define FFT_EXECUTE fftw_execute

#  define IFFT_CREATE  fftw_plan_many_dft_c2r
#  define IFFT_EXECUTE fftw_execute
#  define IFFT_RE_EXECUTE fftw_execute_dft_c2r
#  define FFT_DESTROY  fftw_destroy_plan

/** BLAS functions */
#  define _GESVD  DGESVD
#  define _AXPY  DAXPY
#  define _GEMV  DGEMV
#  define _GEMM  DGEMM

#else
#  include <xmmintrin.h>
/** \name SIMD instructions */
/*@{*/
#  define SIMD_REG      __m256
#  define SIMD_VAL      m256d_f32
#  define SIMD_LOAD     _mm_load_ps
#  define SIMD_STORE    _mm_store_ps
#  define SIMD_STORE_S  _mm_store_ss
#  define SIMD_LOAD_U   _mm_loadu_ps
#  define SIMD_STORE_U  _mm_storeu_ps
#  define SIMD_LOAD1    _mm_load1_ps
#  define SIMD_SET      _mm256_set_ps
#  define SIMD_SET1     _mm256_set1_ps
#  define SIMD_ZERO     SIMD_SET1 (0.0)
#  define SIMD_SUB      _mm_sub_ps
#  define SIMD_ADD      _mm_add_ps
#  define SIMD_MUL      _mm_mul_ps
#  define SIMD_DIV      _mm_div_ps
#  define SIMD_HADD     _mm_hadd_ps
#  define SIMD_SQRT     _mm_sqrt_ps
#  define SIMD_XOR      _mm_xor_ps
#  define SIMD_ANDNOT   _mm_andnot_ps
#  define SIMD_SHUFFLE  _mm_shuffle_ps
#  define SIMD_INV_SQRT _mm_rsqrt_ps
#  define SIMD_INV      _mm_rcp_ps
#  define SIMD_CMPEQ    _mm_cmpeq_ps

#  define SIMD_SET_1    SIMD_SET (1.0, -1.0, 1.0, -1.0)
#  define SHUFFLE_0  _MM_SHUFFLE (2, 2, 0, 0)
#  define SHUFFLE_1  _MM_SHUFFLE (3, 3, 1, 1)
#  define SHUFFLE_2  _MM_SHUFFLE (2, 3, 0, 1)
/*@}*/
/** FFT functions */
#  define FFT_COMPLEX   fftwf_complex
#  define FFT_PLAN    fftwf_plan

#  define FFT_CREATE  fftwf_plan_many_dft_r2c
#  define FFT_RE_EXECUTE  fftwf_execute_dft_r2c
#  define FFT_EXECUTE fftwf_execute

#  define IFFT_CREATE  fftwf_plan_many_dft_c2r
#  define IFFT_EXECUTE fftwf_execute
#  define IFFT_RE_EXECUTE fftwf_execute_dft_c2r
#  define FFT_DESTROY  fftwf_destroy_plan

/** BLAS functions */
#  define _GESVD  SGESVD
#  define _AXPY  SAXPY
#  define _GEMV  SGEMV
#  define _GEMM  SGEMM
#endif

#endif
