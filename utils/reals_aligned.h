/**
 *  \file reals_aligned.h
 *
 *  \brief Implements some utility routines related to allocating
 *  aligned vectors of real numbers.
 */

#if !defined (INC_REALS_ALIGNED_H)
#define INC_REALS_ALIGNED_H /*!< reals_aligned.h included. */

#include "reals.h"

#if !defined (IDEAL_ALIGNMENT)
/** Default alignment, in bytes. */
#  define IDEAL_ALIGNMENT 16
#endif

  /**
   *  \brief Returns a newly allocated array of 'n' real numbers,
   *  aligned on some appropriate boundary for vectorization.
   */
  real_t* reals_alloc__aligned (size_t n);

  /** Initializes 'n' array elements to zero. */
  void reals_zero__aligned (size_t n, real_t* x);

  /**
   *  \brief Copies elements from one real array to another.
   *  \note Both 'src' and 'dest' arrays MUST be aligned.
   */
  void reals_copy__aligned (size_t n, const real_t* restrict src, real_t* restrict dest);

  /**
   *  \brief Copies elements from one real array to another.
   *  \note Only 'src' is aligned.
   */
  void reals_copy__alignedS (size_t n, const real_t* restrict src, real_t* restrict dest);

  /**
   *  \brief Copies elements from one real array to another.
   *  \note Only 'dest' is aligned.
   */
  void reals_copy__alignedD (size_t n, const real_t* restrict src, real_t* restrict dest);

  /**
   *  \brief Frees a previously allocated aligned array of real numbers.
   */
  void reals_free__aligned (real_t* x);

#endif /* !defined (INC_REALS_ALIGNED_H) */

#include "reals_aligned.cpp"
/* eof */
