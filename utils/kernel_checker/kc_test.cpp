#include "KernelChecker.hpp"

#include "Nbody.h"
#include "IRPrinter.h"
/*
#include "Func.h"
#include "Function.h"
#include "IRPrinter.h"
*/

#include <ginac/ginac.h>
#include <ginac/flags.h>

#include <iostream>
#include <string>
#include <vector>

using namespace Nbody;

int main(){
	Func kernel;
	Var x,y;

	kernel(x,y) = x + y - 15;

	KernelChecker kc(kernel);

	kc.test();
}
