#include <iostream>
#include <string>
#include <vector>
#include <ginac/ginac.h>
#include <ginac/flags.h>
#include "Nbody.h"
using namespace std;
using namespace GiNaC;

// globals
const int bigNum = 27494;
const ex threshold = 0.0001;

// check if input ex is within threashold
// input diff should be a numerical value and should be checked before calling this function
bool checkInThreshold(ex diff, ex thresh = 0.0001) {
	if (diff >= 0) {
		if (diff < thresh)
			return true;
	}
	else if (-1 * diff < thresh) {
		return true;
	}
	return false;
}

// simple test to determine if the 2nd deriv is a constant. This allows quicker checking for
// monotonic in/decrease
bool analyze_deriv(ex func, symbol x, symbol y) {
	if (x != y)
		func = func.subs(y == x);
	ex deriv = func.diff(x, 1);
	//cout << deriv << endl;
	if (is_a<numeric>(deriv)) {
		if (deriv.info(info_flags::positive)) {
			printf("[Analyze_Deriv] Function is always increasing!\n");
			return true;
		}
		else if (deriv.info(info_flags::negative)) {
			printf("[Analyze_Deriv] Function is always decreasing!\n");
			return true;
		}
	}
	printf("[Analyze_Deriv] Function derivative isn't constant, conducting more tests...\n");
	return false;
}

// checks to see if limit of a given function aproaches a constant value or infinity
// currently only works with 2 variables
bool analyze_limit(ex func, symbol x, symbol y, ex thresh, int start, int end, int step) {
	ex prev;
	ex diff;
	bool constantLim = false;
	bool posOrNeg = true;
	bool derivChange = false;
	int i = start;

	ex inf_0;

	ex temp = 0;

	// set x to be y if they're different vars
	if (x != y)
		func = func.subs(y == x);

	// unwrapped 1st 2 loop iterations
	prev = evalf(func.subs(x == i*bigNum));

	if (is_a<numeric>(prev)) {
		i+=step;
		inf_0 = evalf(func.subs(x == i*bigNum));

		diff = inf_0 - prev;
		if (diff > 0)
			posOrNeg = true;
		else if (diff < 0)
			posOrNeg = false;
		if (checkInThreshold(diff, thresh))
			constantLim = true;
		else
			constantLim = false;
		prev = inf_0;
	}
	else {
		cout << "[ERROR][Anaylze_Limit] Error with input. Input evaluated as " << prev << endl;
		return false;
	}
	// end unwrapped loop
	// main loop
	for (; i < end; i+=step) {
		// evaluate at a big number
		inf_0 = evalf(func.subs(x == i*bigNum));

		if (is_a<numeric>(inf_0)) {
			diff = inf_0 - prev;

			if (diff > 0) {
				if (!posOrNeg){
//					cout << "Deriv sign change found at " << i <<endl;
					derivChange = true;
				}
				posOrNeg = true;
			}
			else if (diff < 0) {
				if (posOrNeg){
//					cout << "Deriv sign change found at " << i <<endl;
					derivChange = true;
				}
				posOrNeg = false;
			}
			if (checkInThreshold(diff, thresh)) {
				constantLim = true;
			}
			else {
//				cout << "non_constant\n";
				constantLim = false;
			}
			prev = inf_0;
		}
		else {
			cout << "[ERROR][Anaylze_Limit] Error with limit to infinity." << endl;
			return false;
		}
	}
	// end main loop

	cout << "[Anaylze_Limit] last value calculated: " << inf_0 << endl;
	bool retVal = constantLim || (!derivChange);
	if (retVal)
		cout << "[Analyze_Limit] Function has constant limit or derivative thats always pos/neg." << endl;
	else
		cout << "[Analyze_Limit] Function doesn't have constant limit or pos / neg derivative. Conducting more tests..." << endl;
	return retVal;
}

// analyzes the square integral of a function. 
// Returns true if approaches a constant value
// Returns false if not.
bool analyze_integral(ex func, symbol x, symbol y, ex thresh, int start, int end, int step) {
	ex prev, curr;
	ex diff;
	bool constantIntegral = false;
	func = pow(func,2);
	ex integral;
	if (x != y)
		func = func.subs(y == x);

	for (int i = start; i < end; i += step) {
		integral = adaptivesimpson(x, 1, bigNum * i, func, 5);

		if (is_a<numeric>(integral)) {
			if (i == 1) {
				prev = integral;
			}
			diff = prev - integral;

			if (checkInThreshold(diff, thresh))
				constantIntegral = true;
			else {
				constantIntegral = false;
			}
			prev = integral;
		}
		else {
			cout << "[ERROR][Analyze_Integral] Error with integral to infinity" << endl;
			return false;
		}
	}
	if(constantIntegral)
		cout << "[Analyze_Integral] Function has constant integral." << endl;
	else
		cout << "[Analyze_Integral] Function doesn't have constant integral. No tests left..." << endl;

	return constantIntegral;

}

// main test function. give it a GiNaC ex and symbols and it'll do its work. currently only works with 2 vars but easily changed
bool test(ex func, symbol x, symbol y, ex thresh = threshold, int start = 1, int end = 200, int step = 1) {

	cout << "-----------------\n";
	cout << func << endl;

	analyze_deriv(func, x, y);

	if(analyze_limit(func, x, y, thresh, start, end, step)){
		cout << "[TEST] PASSES\n\n";
		return true;
	}

	if(analyze_integral(func, x, y, thresh, start, end, step)){
		cout << "[TEST] PASSES\n\n";
		return true;
	}

	//cout << "integral: " << integral << endl;

	//integral = adaptivesimpson( x , 1 , bigNum * 2 , func, 5);

	cout << "[TEST] FAILS \n\n";
	return false;
}

static ex ret_contents(const ex &x){
	return x;
}

static ex sqrt_(const ex &x){
	return pow(x,0.5);
}

static ex rsqrt_(const ex &x){
	return pow(x,-0.5);
}

static ex pow_(const ex &x,const ex &x2){
	return pow(x,x2);
}

// Macro defs for user defined GiNaC functions
// Format is DECLARE_FUNCTION_<N>P(<func name>) where N = # params
// float_32 transform
DECLARE_FUNCTION_1P(float32);
REGISTER_FUNCTION(float32, eval_func(ret_contents).evalf_func(ret_contents));

// float_64 transform
DECLARE_FUNCTION_1P(float64);
REGISTER_FUNCTION(float64, eval_func(ret_contents).evalf_func(ret_contents));
	
// sqrt_f32 transform
DECLARE_FUNCTION_1P(sqrt_f32);
REGISTER_FUNCTION(sqrt_f32, eval_func(sqrt_).evalf_func(sqrt_));

// sqrt_f64 transform
DECLARE_FUNCTION_1P(sqrt_f64);
REGISTER_FUNCTION(sqrt_f64, eval_func(sqrt_).evalf_func(sqrt_));

// pow_f32 transform
DECLARE_FUNCTION_2P(pow_f32);
REGISTER_FUNCTION(pow_f32, eval_func(pow_).evalf_func(pow_));

// pow_f64 transform
DECLARE_FUNCTION_2P(pow_f64);
REGISTER_FUNCTION(pow_f64, eval_func(pow_).evalf_func(pow_));

// rqrt transform
DECLARE_FUNCTION_1P(rsqrt);
REGISTER_FUNCTION(rsqrt, eval_func(rsqrt_).evalf_func(rsqrt_));

// This function takes the Func given and translates it into a GiNaC symbol table
// currently untested due to zelda server not having cln & GiNaC installed
/*
void getArgs(Nbody::Func kernel, symtab table, symbol x){
	Nbody::Function func = kernel.function();
	for(int i = 0; i < func.dims(); i++){
		table[func.args[i]] = x;
	}
}
*/


// This here is code segment that I wrote while using compile_to_bitcode.cpp to test how to parse
// more of a note to figure out how getArgs() works. Should help with translation if not implemented yet
/*
	// Var setup
	Var x;
	
	// good ol' test function
	Func testy;
	testy(x) = pow(x+11,rsqrt(x))*x + 1/x;
	
	// grab the internal Function of Func
	Function func = testy.function();

	std::cout << "printing args\n";
	// loop through func to find names of args(Var x)
	for(int i = 0;i < func.args().size(); i++)
		std::cout << func.args().at(i) << std::endl;
	std::cout << "done\n";
	
	// this is the exp held by func. value() gets the Exp representation
	Expr exp = func.value();
	
	// setting up an ostringstream because thats what Exp said to use
	std::ostringstream stream;
	// moving exp into an ostringstream
	stream << exp << std::endl;
	
	// turn the altered exp into a string for GiNaC parser use
	std::cout << stream.str();
*/

int main(int argc, char **argv)
{
    symbol x("x"), y("y");
	
    ex func;
	ex euclidian_dist = pow(pow(x, 2) + pow(y, 2),0.5);

	func = 2*x;
	test(func, x, x);

	cout << "Exponential" << " kernel\n";
	func = exp(-euclidian_dist);
	test(func,x,y);

	cout << "Multiquadric" << " kernel\n";
	func = pow(pow(euclidian_dist,2), 0.5);
	test(func, x, y);

	cout << "Log" << " kernel\n";
	func = log(pow(euclidian_dist, 2));
	test(func, x, y);
	
	cout << "Oscilating" << " kernel\n";
	func = sin(euclidian_dist);
	test(func, x, y);
	
	
	// Parser testing
	string test_string = "((pow_f32(float32((v0+11)), float32((1/sqrt_f32(float32(v0)))))*float32(v0))+float32((1/v0))) + 2%5";
	
	symbol z("z");
	
	symtab table;
	table["v0"] = z;
	
	parser reader(table);
	ex e = reader(test_string);
	
	cout << e << endl;
	cout << evalf(e.subs(z == 5)) << endl;
	
	test(e,z,z);
	
  
    return 0;
}
