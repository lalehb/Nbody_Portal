
#include <math.h>
#include <cfloat>
#include "PortalFunction.h"
#include "PortalOperator.h"
#include "fixed_vector.h"
#include <NBodyLower.h>
#include "Metric.h"

using namespace Nbody;
using namespace std;


    template <typename Tree , typename NBodyExpression>
    prune_generator<Tree, NBodyExpression>::prune_generator(vector<PortalOperator*>& OPs, vector<Tree*>&  tree_set,
                                          int size, double tresh):threshold(tresh),
                                          treeSet(tree_set), ttree(*tree_set[0]), stree(*tree_set[1]) {

         dim = tree_set[0]->data[0].size();
         multi_level = OPs.size();
         // threshold = 0.1;

         for(int i = 0 ; i < OPs.size(); i++)
           OP.push_back(*OPs[i]);

         prune_approx_compute();

         for(size_t i = 0; i < multi_level; i++) {
           NodeSet.push_back(&tree_set[i]->root());
         }
         for(int i = 0; i < size+1; i++) {
           temp_compare.push_back(temp_c);
         }
    };

    template <typename Tree , typename NBodyExpression>
    prune_generator<Tree, NBodyExpression>::prune_generator(vector<PortalOperator> OPs,  NBodyExpression& N, vector<Tree*>&  tree_set):
                                            Nexpr(N),treeSet(tree_set), ttree(*tree_set[0]), stree(*tree_set[1]) {

         dim = tree_set[0]->data[0].size();
         multi_level = OPs.size();
         // threshold = 0.1;
         int size = tree_set[0]->root().size();

         for(int i = 0 ; i < OPs.size(); i++)
           OP.push_back(OPs[i]);

         prune_approx_compute();

         for(size_t i = 0; i < multi_level; i++) {
           NodeSet.push_back(&tree_set[i]->root());
         }
         for(int i = 0; i < size+1; i++) {
           temp_compare.push_back(temp_c);
         }
    };


    template <typename Tree , typename NBodyExpression>
    prune_generator<Tree, NBodyExpression>::prune_generator(vector<PortalOperator*>& OPs, vector<Tree*>&  tree_set,
                                         int compare, int size, void* func, vector<real_t> input_v,
                                         double tresh): threshold(tresh), treeSet(tree_set),
                                         ttree(*tree_set[0]), stree(*tree_set[1]), func_compare(compare) {

      dim = tree_set[0]->data[0].size();
      multi_level = OPs.size();
      // threshold = 0.1;

      for(int i = 0 ; i < OPs.size(); i++)
        OP.push_back(*OPs[i]);

      prune_approx_compute();
      kernel_function = func;

      for(size_t i = 0; i < multi_level; i++)
        NodeSet.push_back(&tree_set[i]->root());

      for(int i = 0; i < size+1; i++)
        temp_compare.push_back(temp_c);

      for (int kk = 0; kk < input_v.size(); kk++)
        input_vec.push_back(input_v[kk]);

      input_vec.push_back(0);
      input_vec_size = input_v.size();

    };

    /* constructor with a Euclidean distance as  kernel function and a vector of trees for treeSet*/
    template <typename Tree , typename NBodyExpression>
    prune_generator<Tree, NBodyExpression>::prune_generator(vector<PortalOperator> OPs, vector<real_t> input_v, NBodyExpression& N, vector<Tree*>&  tree_set, double tresh):
                  Nexpr(N), threshold(tresh), treeSet(tree_set), ttree(*tree_set[0]), stree(*tree_set[1]), OP(OPs) {

      dim = tree_set[0]->data[0].size();
      multi_level = OPs.size();
      // threshold = 0.1;
      int size = tree_set[0]->root().size();

      for(int i = 0 ; i < OPs.size(); i++)
        OP.push_back(OPs[i]);

      prune_approx_compute();

      for(size_t i = 0; i < multi_level; i++) {
        NodeSet.push_back(&tree_set[i]->root());
      }
      for(int i = 0; i < size+1; i++) {
        temp_compare.push_back(temp_c);
      }

      for (int kk = 0; kk < input_v.size(); kk++)
        input_vec.push_back(input_v[kk]);

      input_vec.push_back(0);
      input_vec_size = input_v.size();

    };



     /* constructor with a general kernel function  */
     template <typename Tree , typename NBodyExpression>
     prune_generator<Tree, NBodyExpression>::prune_generator(vector<PortalOperator> OPs, NBodyExpression& N, bool Eucl, vector<Tree*>&  tree_set, double tresh):
                        Nexpr(N), threshold(tresh), treeSet(tree_set),ttree(*tree_set[0]), stree(*tree_set[1]), OP(OPs) {

            dim = tree_set[0]->data[0].size();
            multi_level = OPs.size();
            // threshold = 0.1;
            int size = tree_set[0]->root().size();

            for(int i = 0 ; i < OPs.size(); i++)
              OP.push_back(OPs[i]);

            prune_approx_compute();

            for(size_t i = 0; i < multi_level; i++) {
              NodeSet.push_back(&tree_set[i]->root());
            }
            for(int i = 0; i < size+1; i++) {
              temp_compare.push_back(temp_c);
            }
        };


     /* constructor with a general kernel function  */
     template <typename Tree , typename NBodyExpression>
     prune_generator<Tree, NBodyExpression>::prune_generator(vector<PortalOperator> OPs, NBodyExpression& N, vector<Tree*>&  tree_set,void* func, Tree& t, Tree& s, vector<Node> node_set, double tresh):
                    Nexpr(N), threshold(tresh), treeSet(tree_set), ttree(t), stree(s), OP(OPs), NodeSet(node_set) {

        dim = tree_set[0]->data[0].size();
        multi_level = OPs.size();
        // threshold = 0.1;
        int size = tree_set[0]->root().size();

        for(int i = 0 ; i < OPs.size(); i++)
          OP.push_back(OPs[i]);

        prune_approx_compute();
        kernel_function = func;

        for(size_t i = 0; i < multi_level; i++) {
          NodeSet.push_back(&tree_set[i]->root());
        }
        for(int i = 0; i < size+1; i++) {
          temp_compare.push_back(temp_c);
        }
     };


    /* Sets the value for prune_approx
     * prune_approx keeps track of the N-body problems category
     * prune_approx --> 0 : approximation ,
     * prune_approx --> 1 : single prune,
     * prune_approx --> 2 : Nested prune
    */
    template <typename Tree , typename NBodyExpression>
    void
    prune_generator<Tree, NBodyExpression>::prune_approx_compute() {
       prune_approx  = 0;
       for (size_t i=0; i< OP.size(); i++) {
         if (OP[i].isComparative()) {
           prune_approx++;
           if (prune_approx == 1){
             temp_c = OP[i].getInitFilterVar();
             first_prune_operator = OP[i];
           }
         }

        /* initialize the Vec for the point sets , this point set has been used in nested prune tests*/
        Vec point_(dim);
        for (size_t j = 0; j < dim ; j++)
           point_[i] = 0;
        point_set.push_back(point_);
       }
       prune_approx += func_compare;
    }


    template <typename Tree , typename NBodyExpression>
    real_t
    prune_generator<Tree, NBodyExpression>::temp_calculator(Node& t ) {
       if (OP[0].isComparative())
          return temp_compare[0];

       real_t temp_val = temp_compare[t.begin()];
       for (int i =  t.begin(); i < t.end(); i++)
         // if (temp_val > temp_compare[i])
         if (first_prune_operator.reverse(temp_val, temp_compare[i]))
           temp_val = temp_compare[i];
       return temp_val;
    }


    template <typename Tree , typename NBodyExpression>
    real_t
    prune_generator<Tree, NBodyExpression>::distance_calculator(PortalOperator Operator, Node& s, Node& t ) {

      switch(Operator.getOperator()) {
        case PortalOperator::OP::MAX:
        case PortalOperator::OP::ARGMAX:
          return s.max_distance(t);
        case PortalOperator::OP::MIN:
        case PortalOperator::OP::ARGMIN:
          return s.min_distance(t);
      }
      return 0;
    }

    /* Sets the temperorary values asigned by the */
    template <typename Tree , typename NBodyExpression>
    void inline
    prune_generator<Tree, NBodyExpression>::setTemp(int a, real_t b) {
      temp_compare[a] = b;
    }

    template <typename Tree , typename NBodyExpression>
    void inline
    prune_generator<Tree, NBodyExpression>::set_temp_node(Node& t, real_t b) {
        for (int i = t.begin(); i < t.end(); i++) {
              temp_compare[i] = b;
        }
    }


    template <typename Tree, typename NBodyExpression>
    bool
    prune_generator<Tree, NBodyExpression>::prune_subtree(Node& t, int k) {

      bool prune = true;
      real_t diff, max_value, min_value, center_value;
      Vec s_point;
      /* Changes the format of source point to Vec */
      for (size_t i = 0; i < stree.data[k].size(); i++)
           s_point[i]= stree.data[k][i];

      /* Approximation Nbody problems */
      if ( prune_approx == 0) {
            if(Euclidean_kernel) {
                max_value = t.max_distance(stree.data[k]);
                min_value = t.min_distance(stree.data[k]);
                center_value = (max_value + min_value)/2.0;
            }
            else {
                Vec t_min_point = t.min_point(stree.data[k]);
                Vec t_max_point = t.max_point(stree.data[k]);
                Vec t_center_point = t.center();

                center_value   = Nexpr.point_calc(t_center_point, s_point);
                min_value      = Nexpr.point_calc(s_point, t_min_point);
                max_value      = Nexpr.point_calc(s_point, t_max_point);

            }
            diff = max_value - min_value;
            prune = (fabs(diff) < (threshold * center_value));
            return prune;
      }
      /* Single prune Nbody problems */
      else if (prune_approx == 1) {
          if(Euclidean_kernel) {
              double distance_min = t.min_distance(stree.data[k]);
              double distance_max = t.max_distance(stree.data[k]);
              if (!func_compare){
                  real_t temp_val = temp_calculator(t);
                  prune = first_prune_operator.reverse(distance_min, temp_val) &&
                          first_prune_operator.reverse(distance_max, temp_val);
              }
              else{
                  Vec t_min_point = t.min_point(stree.data[k]);
                  Vec t_max_point = t.max_point(stree.data[k]);
                  Vec s_min_point = stree.data[k].min_point(t);
                  Vec s_max_point = stree.data[k].max_point(t);
                  prune = !(Nexpr.point_calc(s_min_point, t_min_point)) &&
                          !(Nexpr.point_calc(s_max_point, t_max_point));
             }
          }
          else {
              real_t min_boundary_point;
              real_t max_boundary_point;
              Vec t_min_point = t.min_point(stree.data[k]);
              Vec t_max_point = t.max_point(stree.data[k]);

              if (!func_compare){
                  real_t temp_val = temp_calculator(t);
                  min_boundary_point = Nexpr.point_calc(s_point, t_min_point);
                  max_boundary_point = Nexpr.point_calc(s_point, t_max_point);
                  prune = first_prune_operator.reverse(min_boundary_point, temp_val) &&
                          first_prune_operator.reverse(max_boundary_point, temp_val);
              }
              else {
                 prune = Nexpr.point_calc(s_point, t_min_point) &&
                         Nexpr.point_calc(s_point, t_max_point);
              }
          }
       }
       return prune;


    }

    template <typename Tree, typename NBodyExpression>
    bool
    prune_generator<Tree, NBodyExpression>::prune_subtree(Node& t, Node& s) {


          bool prune = true;
          real_t diff, max_value, min_value, center_value;

          /* Approximation Nbody problems */
          if (prune_approx == 0) {
              if(Euclidean_kernel) {

                 max_value = t.max_distance(s);
                 min_value = t.min_distance(s);;
                 center_value = (max_value + min_value)/2.0;

              }
              else{
                  Vec t_min_point = t.min_point(s);
                  Vec t_max_point = t.max_point(s);
                  Vec t_center_point = t.center();

                  Vec s_min_point = s.min_point(t);
                  Vec s_max_point = s.max_point(t);
                  Vec s_center_point = s.center();

                  center_value   = Nexpr.point_calc(t_center_point, s_center_point);
                  min_value      = Nexpr.point_calc(s_min_point, t_min_point);
                  max_value      = Nexpr.point_calc(s_max_point, t_max_point);

              }
              diff = max_value - min_value;
              prune = (fabs(diff) < (threshold * center_value));

              /* This version considers the density of the Nodes too */
              // prune = (fabs(diff) * t.size() * s.size()) < (threshold * center_value );

              return prune;
         }
         /* Single prune Nbody problems */
         else if (prune_approx == 1) {
             if(Euclidean_kernel) {
                 double distance_min = t.min_distance(s);
                 double distance_max = t.max_distance(s);
                 if (!func_compare){
                     real_t temp_val = temp_calculator(t);
                     prune = first_prune_operator.reverse(distance_min, temp_val) &&
                             first_prune_operator.reverse(distance_max, temp_val);
                 }
                 else{
                     Vec t_min_point = t.min_point(s);
                     Vec t_max_point = t.max_point(s);
                     Vec s_min_point = s.min_point(t);
                     Vec s_max_point = s.max_point(t);
                     prune = !(Nexpr.point_calc(s_min_point, t_min_point)) &&
                             !(Nexpr.point_calc(s_max_point, t_max_point));
                 }
             }
             else {
                 real_t min_boundary_point;
                 real_t max_boundary_point;
                 Vec t_min_point = t.min_point(s);
                 Vec t_max_point = t.max_point(s);
                 Vec s_min_point = s.min_point(t);
                 Vec s_max_point = s.max_point(t);

                 if (!func_compare){
                     real_t temp_val = temp_calculator(t);
                     min_boundary_point = Nexpr.point_calc(s_min_point, t_min_point);
                     max_boundary_point = Nexpr.point_calc(s_max_point, t_max_point);
                     prune = first_prune_operator.reverse(min_boundary_point, temp_val) &&
                             first_prune_operator.reverse(max_boundary_point, temp_val);
                 }
                 else {
                    prune = !(Nexpr.point_calc(s_min_point, t_min_point)) &&
                            !(Nexpr.point_calc(s_max_point, t_max_point));
                 }
             }
             return prune;
          }
          /* Nested prune Nbody problems */
          else if (prune_approx > 1) {

             if(Euclidean_kernel) {
                 /* cheeper prune check but will prune less of data */
                 real_t temp_val = temp_calculator(t);
                 real_t distance = distance_calculator(first_prune_operator,s,t);
                 prune = first_prune_operator.reverse(distance, temp_val);

             }
             else {
                   /* if we don't have the NodeSet it does not make sense to call the nested prune */
                   NodeSet[0] = &t;
                   NodeSet[1] = &s;
                   if(NodeSet.size())
                     prune = nested_prune(0, point_set);
                }
                return prune;
          }

    }

    template <typename Tree, typename NBodyExpression>
    bool
    prune_generator<Tree, NBodyExpression>::prune_subtree(vector<Node*> NodeVec) {

      nested_prune(0, point_set);
      return prune_result;
    }

    /* Nested prune for the dual tree (it should work for more than two tree too)*/
    template <typename Tree, typename NBodyExpression>
    bool
    prune_generator<Tree, NBodyExpression>::nested_prune(int level, vector<Vec> temp) {

          SquaredEuclideanMetric<Vec, Vec> metric;
          /* stop if we already know that this set of nodes are not prunable instead of iterating through */
          if (!prune_result)
            return 0;
          bool prune = true;
          real_t result;

          /* base case for the recursion which happens at the last level with two remaining nodes */
          if (level == multi_level-2) {
                Vec lastL_min_point = NodeSet[level]->min_point(*NodeSet[level+1]);
                Vec lastL_max_point = NodeSet[level]->max_point(*NodeSet[level+1]);
                Vec lastL_center = NodeSet[level]->center();

                Vec lastL2_min_point = NodeSet[level+1]->min_point(*NodeSet[level]);
                Vec lastL2_max_point = NodeSet[level+1]->max_point(*NodeSet[level]);
                Vec lastL2_center = NodeSet[level+1]->center();

                real_t min_boundary_point;
                real_t max_boundary_point;
                real_t temp_compare = temp_calculator(*NodeSet[level]);

                if(Euclidean_kernel) {
                    vector<double> boundary = NodeSet[level]->boundary_distances(*NodeSet[level+1]);

                    for (size_t i = 0; i < boundary.size(); i++) {
                      /* the return value that is used in recursion could be specidied with the last opertator in that level*/
                      prune = OP[level].reverse(temp_compare, boundary[i]) && prune;
                      if (!prune)
                        prune_result = false;
                    }
                }
                else {
                    for(size_t i = 0; i < dim; i++) {
                      temp[level+1][i] = lastL_min_point[i];
                      temp[level  ][i] = lastL2_min_point[i];
                     }
                     min_boundary_point = Nexpr.point_calc(temp[0], temp[1]);

                    for(size_t i = 0; i < dim; i++) {
                      temp[level+1][i] = lastL_max_point[i];
                      temp[level  ][i] = lastL2_max_point[i];
                     }
                    max_boundary_point = Nexpr.point_calc(temp[0], temp[1]);
                    prune = OP[level].reverse( min_boundary_point, temp_compare) &&
                                   OP[level].reverse(max_boundary_point, temp_compare);
                    if (!prune)
                      prune_result = false;
                }
                return prune;
          }
          /* recursively iterate through the border points  and call the nested prune in inner layers*/
          else if(level < multi_level-2) {

                vector<Vec> boundary_points = NodeSet[level]->boundary_points(*NodeSet[level+1]);
                real_t temp_compare = temp_calculator(*NodeSet[level]);
                for (size_t i = 0; i < boundary_points.size(); i++) {
                  for(size_t j = 0; j < dim; j++)
                    temp[level][j] = boundary_points[i][j];
                  bool nest_result = nested_prune(level+1, temp);
                  real_t temp_val = Nexpr.point_calc(temp[level], temp[level-1]);
                  prune = prune && (OP[level].reverse(temp_compare, temp_val) && nest_result) ;
                  if (!prune)
                    prune_result = false;
                }
            return prune;
          }

    }


    template <typename Tree, typename NBodyExpression>
    real_t
    prune_generator<Tree, NBodyExpression>::centroid_case(Node& t, Point& s) {
        if (prune_approx) {
          Vec t_center_point = t.center();
          real_t center_value;
          Vec s_point;
          /* Changes the format of source point to Vec */
          for (size_t i = 0; i < s.size(); i++)
               s_point[i]= s[i];

          if (Euclidean_kernel) {
              SquaredEuclideanMetric<Vec, Point> metric;
              center_value  = metric.compute_distance(t_center_point, s);
              /* Considers the mass of the Node too */
              // center_value = center_value * t.size();
          }
          else {
              center_value   = Nexpr.point_calc(t_center_point, s_point);
          }
          return center_value;
        }
        return 0;
     }


    template <typename Tree, typename NBodyExpression>
    real_t
    prune_generator<Tree, NBodyExpression>::centroid_case(Node& t, Node& s) {
      if (prune_approx) {
          Vec t_center_point = t.center();
          Vec s_center_point = s.center();
          real_t center_value;

          if (Euclidean_kernel) {
              SquaredEuclideanMetric<Vec, Vec> metric;
              center_value  = metric.compute_distance(s_center_point, t_center_point);
              /* Considers the mass of the Node too */
              // center_value = center_value * t.size() * s.size();
          }
          else {
              center_value   = Nexpr.point_calc(t_center_point, s_center_point);
          }
          return center_value;
      }
      return 0;

    }

    template <typename Tree, typename NBodyExpression>
    real_t
    prune_generator<Tree, NBodyExpression>::centroid_case(vector<Node*> NodeVec) {
          if (prune_approx) {
              Vec t_center_point;
              vector<Vec> centers;
              for (int i = 0; i < NodeVec.size(); i++) {
                  t_center_point = NodeVec[i].center();
                  centers.push_back(t_center_point);
              }

              real_t center_value   = kernel_function_point(centers);
              return center_value;
          }
          return 0;
    }


    template <typename Tree, typename NBodyExpression>
    vector<int>
    prune_generator<Tree, NBodyExpression>::visit (Node& s, int t) {

        vector<int> visit_order;
        std::vector<std::pair<real_t, int> > dist_index;

        if (prune_approx == 0) {
            for (int i = 0; i < s.num_child(); i++)
              visit_order.push_back(i);

        }
        else{
            for (int i = 0; i < s.num_child(); i++) {
                real_t dist = stree.node_data[s.child+i].min_distance(ttree.data[t]);
                dist_index.push_back(std::make_pair(dist, i) );
            }
            std::sort(dist_index.begin(), dist_index.end());
            for (int i = 0; i < dist_index.size(); i++)
                visit_order.push_back(dist_index[i].second);
        }
        return visit_order;
    }

    template <typename Tree, typename NBodyExpression>
    vector<int>
    prune_generator<Tree, NBodyExpression>::visit (Node& s, Node& t) {
          vector<int> visit_order;
          std::vector<std::pair<real_t, int> > dist_index;

          if (prune_approx == 0) {
              for (int i = 0; i < s.num_child(); i++)
                visit_order.push_back(i);

          }
          else{
              for (int i = 0; i < s.num_child(); i++) {
                  real_t dist = s.min_distance(t);
                  dist_index.push_back(std::make_pair(dist, i) );
              }
              std::sort(dist_index.begin(), dist_index.end());
              for (int i = 0; i < dist_index.size(); i++)
                  visit_order.push_back(dist_index[i].second);
          }
          return visit_order;

    }


    template <typename Tree, typename NBodyExpression>
    vector<int>
    prune_generator<Tree, NBodyExpression>::visit (vector<Node*> t) {

      vector<int> visit_order;
      std::vector<std::pair<real_t, int> > dist_index;

      if (prune_approx == 0) {
          for (int i = 0; i < t[0]->num_child(); i++)
            visit_order.push_back(i);

      }
      else{
          for (int i = 0; i < t[0]->num_child(); i++) {
              real_t dist = t[0]->min_distance(t[1]);
              dist_index.push_back(std::make_pair(dist, i) );
          }
          std::sort(dist_index.begin(), dist_index.end());
          for (int i = 0; i < dist_index.size(); i++)
              visit_order.push_back(dist_index[i].second);
      }
      return visit_order;

    }
