#ifndef  _PRUNE_GENERATOR_H_
#define  _PRUNE_GENERATOR_H_


#include <math.h>
#include <cfloat>
#include "PortalFunction.h"
#include "PortalOperator.h"
#include "fixed_vector.h"
#include <NBodyLower.h>
#include "Metric.h"

using namespace Nbody;
using namespace std;

template <typename Tree, typename NBodyExpression>
class prune_generator {

  public:
    typedef typename Tree::NodeTree Node;

    /* Defines the constructor with an Euclidean distance as  kernel function and a vector of trees for treeSet input*/
    prune_generator(vector<PortalOperator*>& OPs, vector<Tree*>&  tree_set, int size, double tresh);
    prune_generator(vector<PortalOperator> OPs,  NBodyExpression& N, vector<Tree*>&  tree_set);

    /* Defines the constructor with an function pointer named func as  kernel function and a vector of trees for treeSet input*/
    prune_generator(vector<PortalOperator*>& OPs, vector<Tree*>&  tree_set, int compare,
                    int size, void* func, vector<real_t> input_v, double tresh);

     /* constructor with a Euclidean distance as  kernel function and a vector of trees for treeSet*/
     prune_generator(vector<PortalOperator> OPs, vector<real_t> input_v, NBodyExpression& N, vector<Tree*>&  tree_set, double tresh);

     /* constructor with a general kernel function  */
     prune_generator(vector<PortalOperator> OPs, NBodyExpression& N, bool Eucl, vector<Tree*>&  tree_set, double tresh);


     /* constructor with a general kernel function  */
     prune_generator(vector<PortalOperator> OPs, NBodyExpression& N, vector<Tree*>&  tree_set,void* func,
                     Tree& t, Tree& s, vector<Node> node_set, double tresh);


     /* Generates prune condition for Singel-tree, Dual-tree and Multi-tree */
     bool prune_subtree(Node&, int);
     bool prune_subtree(Node&, Node&);
     bool prune_subtree(vector<Node*> NodeVec);

     /* Calculates which branch to visit first */
     vector<int>  visit (Node& s, int t);
     vector<int> visit (Node& s, Node& t);
     vector<int> visit (vector<Node*> t);

     /* Calculates the centroid computation */
     real_t centroid_case(Node& t, Point& s);
     real_t centroid_case(Node& t, Node& s);
     real_t centroid_case(vector<Node*> NodeVec);

     /* Sets the temperorary values calculate duringt the basecase computations */
     void setTemp(int a, real_t b);
     void set_temp_node(Node& t, real_t b);

  private:

    /* Computes if the inputed problem is a prune or approximation problem*/
    void prune_approx_compute();

    /* Computes the prune value for multiple prunning opportunities*/
    bool nested_prune(int level, vector<Vec> temp);

    /* Calculates the distance needed for operator O */
    real_t distance_calculator(PortalOperator O, Node& s, Node& t);

    /* Calculates the temeroray values for pruning */
    real_t temp_calculator(Node& t);


    /* Contains the operators as the first one is most outter operator */
    vector<PortalOperator> OP;

    /* Presents the first operator that provides the pruning opportunity */
    PortalOperator first_prune_operator;

    /* Temperorary values for pruning purpose */
    vector<real_t> temp_compare;

    /* The default value for temp_compare which will be calculated using operators */
    real_t temp_c;

    /* Vector of Nodees that we need to decide to prune them or not! */
    vector<Node*> NodeSet;

    /* Inputed vector for the kernel function of problem*/
    vector<real_t> input_vec;
    vector<Vec> input_vec_points;

    /* Reserves the size of input_vec*/
    int input_vec_size = 0;

    /* Thresold defined by user for approximation problems */
    real_t threshold;

    /* Multi_level represents the level of the problem which is equal to number of trees */
    int  multi_level;

    /* Counts the number of prunning opportunities */
    int prune_approx;

    /* Represents vector of points in order to apply kernel function for nested prune when we have a general function */
    vector<Vec> point_set;

    /* the result of prune is saved in this value, specially important in Nested_prune */
    bool prune_result = true;

   /* check if the kernel is not Euclidean */
   bool Euclidean_kernel = true;

   /* Trees that include the data for prune/appriximation */
   Tree& stree;
   Tree& ttree;
   std::vector<Tree*> treeSet;

   /* Pointer to the kernel function of the problem */
   bool (*kernel_function) (vector<real_t>);
   bool (*kernel_function_point) (vector<Vec>);

   /* Checks if the kernel function is a comparison function */
   int func_compare = 0;

   /* Dimension of data points */
   size_t dim;

   /* For calculating the kernel computation */
   NBodyExpression& Nexpr;

};


/* template instantiation*/
#include "prune_generator.cpp"

#endif
